var mysql = require('mysql');

this.connection = {};

this.createConnection = function(options) {
	connection = mysql.createConnection(options);
}

this.connect = mysql.connect;
this.end = mysql.end;

this.query = function() {
	var queryString = arguments[0];
	var queryParams = [];
	var callback = undefined;
	for (var a=0; a<arguments.length; a++) {
		if (typeof arguments[a] == "array") {
			queryParams = arguments[a];
		} else if (typeof arguments[a] == "function") {
			callback = arguments[a];
		}
	}
	
	connection.query(queryString, queryParams, callback);
	
}