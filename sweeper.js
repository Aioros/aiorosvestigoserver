/*
	Script: sweeper
*/

var cls = require("./lib/class"),
	NetworkMessages = require("./message"),
	Items = require("./item"), Item = Items.Item, ItemFactory = Items.ItemFactory,
	fs = require("fs");

/*
	Class: Sweeper
	Holds sweeper information
	
	Variables:
		id						-	The sweeper id.
		name					-	Do you really care?
		level					-	Sweeping level?
		location				-	The current location.
		x						-	X position in the current location.
		z						-	Z position in the current location.
		kind					-	The kind (as in <Constants.Entities>).
		outfits					-	Sweepers kinda look all the same.
		gridBlockSize			-	The size of the sweeping grid block.
		grid					-	A 2D array of grid blocks.
		gridInterval			-	The interval for grid update.
		restInterval			-	The interval for resting.
		itemToSweep				-	The item to be swept. Swopen. Swepped.
		lastDirection			-	The last walking direction chosen.
		target					-	Target position.
		inventory				-	The sweeper keeps the things in his pockets. Ew.
		capacity				-	The inventory capacity.
		full					-	True when the inventory is full.
		trashcans				-	Array of trash cans in the location.
		itemFactory				-	The <ItemFactory>.
		behaviour				-	Available interactions.
*/

module.exports = Sweeper = cls.Class.extend({

	/*
		Constructor: init
		Parameters:
			id			-	id
			name		-	name
			level		-	level
			position	-	position
			location	-	location
			stop it		-	stop it
			i'm dumb	-	i'm d-hey
	*/
	init: function(id, name, level, position, location) {
		var self = this;
		
		this.id = id;
		this.name = name;
		this.level = level;
		this.location = location;
		
        this.kind = Constants.Entities.SPY;
        this.x = position.x;
        this.z = position.z;
		
		this.outfits = {};
		// Default outfit
		this.outfits[1] = {index: 0, animations: {walking: "baseWalking", interacting: "baseInteracting"}};
		
		this.inventory = {};
		this.trashcans = [];
		this.capacity = 5;
		this.full = false;
		
		this.itemFactory = new ItemFactory(this.location.server.db);
		
		this.behaviour = {};
		
		// Retrieve bot outfits
		self.location.server.db.query("SELECT i.id, o.name, o.textures " +
									"FROM object o INNER JOIN object_instance i ON i.object_id = o.id " +
									"WHERE i.spy_id = ? AND o.outfit = 1 AND i.state = 1", [self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving outfits");
				throw err;
			} else {
				for (var t in rows) {
					var textures = JSON.parse(rows[t].textures);
					self.outfits[rows[t].id] = textures.outfit;
				}
			}
		});
		
		// Retrieve inventory
		self.location.server.db.query("SELECT i.id " +
									"FROM object_instance i " +
									"WHERE i.spy_id = ? ", [self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving inventory");
				throw err;
			} else {
				for (var i in rows) {
					self.itemFactory.buildFromId(rows[i].id, function(item) {
						self.inventory[item.id] = item;
						if (Object.keys(self.inventory).length == rows.length) {
							if (Object.keys(self.inventory).length >= self.capacity)
								self.full = true;
							self.behaviour = self.getBehaviour();
							var behaviourMsg = new NetworkMessages.Behaviour(0, self.id, self.behaviour);
							self.broadcast(behaviourMsg);
						}
					});
				}
			}
		});
		
		// Retrieve trash cans in the location
		self.location.server.db.query("SELECT i.id, i.posX, i.posZ " +
									"FROM object o INNER JOIN object_instance i ON i.object_id = o.id " +
									"WHERE o.trashcan = 1 AND i.location_id = ?", [self.location.id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving trash cans");
				throw err;
			} else {
				for (var t in rows) {
					self.trashcans.push({id: rows[t].id, pos:{x: rows[t].posX, z: rows[t].posZ}});
				}
			}
		});
		
		this.gridBlockSize = 2;
		
		this.grid = [];
		this.createGrid();
		this.gridInterval = setInterval(function() {
			self.updateGrid();
		}, 10 * 1000);
		this.restInterval = setInterval(function() {
			self.pauseMovement();
			self.movementTimeout = setTimeout(function() {
				self.startMovement();
			}, 5 * 1000);
		}, 10 * 1000);
		
		this.itemToSweep = -1;
		this.lastDirection = -1;
		this.target = {x: this.x, z: this.z};
		
		this.startMovement();
	
	},
	
	/*
		Function: createGrid
		Creates a grid over the location
	*/
	createGrid: function() {
		var self = this;
		
		var lengthBlocks = Math.ceil(this.location.map.length / this.gridBlockSize);
		var widthBlocks = Math.ceil(this.location.map.width / this.gridBlockSize);
		for (var i=0; i<lengthBlocks; i++) {
			this.grid[i] = [];
			for (var j=0; j<widthBlocks; j++) {
				this.grid[i][j] = {lastTime: 0, dirt: 0, items: []};
			}
		}
		this.updateGrid();
	},
	
	/*
		Function: updateGrid
	*/
	updateGrid: function() {
		var lengthBlocks = Math.ceil(this.location.map.length / this.gridBlockSize);
		var widthBlocks = Math.ceil(this.location.map.width / this.gridBlockSize);
		for (var i=0; i<lengthBlocks; i++) {
			for (var j=0; j<widthBlocks; j++) {
				this.grid[i][j].dirt = 0;
				this.grid[i][j].items = [];
			}
		}
		for (var i in this.location.items) {
			if (this.location.items[i].flags.pickable) {
				var pos = this.location.items[i].pos;
				var block = this.blockFromPosition(pos);
				var firstL = Math.max(0, block.x - 2);
				var lastL = Math.min(lengthBlocks-1, block.x + 2);
				var firstW = Math.max(0, block.z - 2);
				var lastW = Math.min(widthBlocks-1, block.z + 2);
				for (var l=firstL; l<lastL; l++) {
					for (var w=firstW; w<lastW; w++) {
						var distL = Math.abs(l - block.x);
						var distW = Math.abs(w - block.z);
						this.grid[l][w].dirt += 3 - Math.max(distL, distW);
					}
				}
				this.grid[block.x][block.z].items.push(i);
			}
		}
	},
	
	/*
		Function: getInventoryIds
		
		Returns
			An array containing the ids of all the inventory items.
	*/
	getInventoryIds: function() {
		var invIds = [];
		for (var i in this.inventory) {
			invIds.push(this.inventory[i].id);
		}
		return invIds;
	},
	
	/*
		Function: saveInventory
		Stores current inventory in DB.
		
		Parameters:
			callback	-	A callback function.
	*/
	saveInventory: function(callback) {
		var self = this;
		
		// Get list of inventory item ids
		var idsString = self.getInventoryIds().join();
		
		if (idsString.length > 0) {
		
			// Remove items that are not in the list
			self.location.server.db.query("UPDATE object_instance i INNER JOIN object o ON o.id = i.object_id " +
									"SET i.spy_id = NULL " +
									"WHERE i.spy_id = ? " +
									"AND i.id NOT IN (" + idsString + ")", [self.id], function(err, rows, fields) {
				if (err) {
					console.log("Error saving inventory");
					throw err;
				} else {
					// Add objects that are in the list
					self.location.server.db.query("UPDATE object_instance i INNER JOIN object o ON o.id = i.object_id " +
											"SET i.spy_id = ?, i.location_id = NULL, i.posX = NULL, i.posY = NULL, i.posZ = NULL " +
											"WHERE i.id IN (" + idsString + ")", [self.id], function(err, rows, fields) {
						if (err) {
							console.log("Error saving inventory");
							throw err;
						} else {
							console.log("Inventory saved");
							callback();
						}
					});
				}
			});
			
		} else {
			// The inventory is empty, so remove everything
			self.location.server.db.query("UPDATE object_instance i INNER JOIN object o ON o.id = i.object_id " +
									"SET i.spy_id = NULL " +
									"WHERE i.spy_id = ? ", [self.id], function(err, rows, fields) {
				if (err) {
					console.log("Error saving inventory");
					throw err;
				} else {
					console.log("Inventory saved");
					callback();
				}
			});
		}
	},
	
	/*
		Function: removeFromInventory
		Parameters:
			id	-	The item id.
		
		Returns:
			The removed item.
	*/
	removeFromInventory: function(id) {
		if (this.inventory[id] != undefined) {
			var item = this.inventory[id];
			delete this.inventory[id];
			return item;
		}
		return null;
	},
	
	/*
		Function: blockFromPosition
	*/
	blockFromPosition: function(pos) {
		return {x: Math.floor(pos.x / this.gridBlockSize), z: Math.floor(pos.z / this.gridBlockSize)};
	},
	
	/*
		Function: positionFromBlock
	*/
	positionFromBlock: function(block) {
		return {x: block.x * this.gridBlockSize + this.gridBlockSize / 2, z: block.z * this.gridBlockSize + this.gridBlockSize / 2}
	},
	
	/*
		Function: destroy
	*/
	destroy: function() {
		// Unused, but maybe one day, who knows.
	},
	
	/*
		Function: despawn
		Returns:
			A <NetworkMessages.Despawn> for this player.
	*/
	despawn: function() {
		return new NetworkMessages.Despawn(this);
	},
	
	/*
		Function: setPosition
		Parameters:
			x	-	X coordinate.
			z	-	Z coordinate.
	*/
	setPosition: function(x, z) {
        this.x = x;
        this.z = z;
    },
	
	/*
		Function: onMove
		Called each time a Move message is received.
		
		Parameters:
			x	-	X coordinate of the target.
			z	-	Z coordinate of the target.
	*/
	onMove: function(x, z) {
		var self = this;
		
		// Check if the sweeper has changed his group
		var hasChangedGroups = self.location.handleEntityGroupMembership(self);
		if (hasChangedGroups) {
			// Save sweeper position to DB
			self.savePosition();
			// Tell everybody in the previous groups to despawn the sweeper
			self.location.pushToPreviousGroups(self, new NetworkMessages.Despawn(self));
			// Tell everybody in the new groups to spawn the sweeper
			self.location.pushToAdjacentGroups(self.group, new NetworkMessages.Spawn(self), self.id);
		}
	},
	
	/*
		Function: savePosition
		Stores current position in DB.
	*/
	savePosition: function() {
		var self = this;
		self.location.server.db.query("UPDATE spy SET location_id = ?, posX = ?, posZ = ? WHERE id = ?", [self.location.id, self.x, self.z, self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error saving position");
				throw err;
			} else {
				//console.log("Position saved");
			}
		});
	},
	
	/*
		Function: startMovement
		Sets up the periodic moving cycle management.
	*/
	startMovement: function() {
		var self = this;
		
		self.movementInterval = setInterval(function() {
			var cur = self.blockFromPosition({x: self.x, z: self.z});
			self.grid[cur.x][cur.z].lastTime = Date.now();
			
			if (!self.full && self.itemToSweep == -1 && self.grid[cur.x][cur.z].items.length > 0) {
				self.itemToSweep = self.grid[cur.x][cur.z].items[0];
				self.target = self.location.items[self.itemToSweep].pos;
			}
			if (!self.full && self.itemToSweep == -1 && self.grid[cur.x][cur.z].items.length == 0) {
				var lengthBlocks = Math.ceil(self.location.map.length / self.gridBlockSize);
				var widthBlocks = Math.ceil(self.location.map.width / self.gridBlockSize);
				var adjacentBlocks = [	{x: Math.max(0, cur.x - 1), z: Math.min(cur.z + 1, widthBlocks-1)},
										{x: cur.x, z: Math.min(cur.z + 1, widthBlocks-1)},
										{x: Math.min(cur.x + 1, lengthBlocks-1), z: Math.min(cur.z + 1, widthBlocks-1)},
										{x: Math.max(0, cur.x - 1), z: cur.z},
										{x: Math.min(cur.x + 1, lengthBlocks-1), z: cur.z},
										{x: Math.max(0, cur.x - 1), z: Math.max(0, cur.z - 1)},
										{x: cur.x, z: Math.max(0, cur.z - 1)},
										{x: Math.min(cur.x + 1, lengthBlocks-1), z: Math.max(0, cur.z - 1)}	];
				var maxDirt = 0;
				var dirtiest = -1;
				for (var i=0; i<adjacentBlocks.length; i++) {
					if (self.grid[adjacentBlocks[i].x][adjacentBlocks[i].z] != undefined && self.grid[adjacentBlocks[i].x][adjacentBlocks[i].z].dirt > maxDirt) {
						maxDirt = self.grid[adjacentBlocks[i].x][adjacentBlocks[i].z].dirt;
						dirtiest = i;
					}
				}
				if (dirtiest == -1) {
					var oldestTime = Date.now();
					for (var i=0; i<adjacentBlocks.length; i++) {
						if (self.grid[adjacentBlocks[i].x][adjacentBlocks[i].z] != undefined && self.grid[adjacentBlocks[i].x][adjacentBlocks[i].z].lastTime < oldestTime) {
							oldestTime = self.grid[adjacentBlocks[i].x][adjacentBlocks[i].z].lastTime;
							dirtiest = i;
						}
					}
				}
				while (adjacentBlocks[dirtiest].x == cur.x && adjacentBlocks[dirtiest].z == cur.z) {
					dirtiest = Math.floor(Math.random() * adjacentBlocks.length);
				}
				self.target = self.positionFromBlock(adjacentBlocks[dirtiest]);
				self.lastDirection = dirtiest;
			}
			if (self.full) {
				var minDistance = 1000;
				self.trashcan = null;
				for (var t in self.trashcans) {
					var trashcanPos = self.trashcans[t].pos;
					var distance = Math.sqrt(Math.pow(self.x - trashcanPos.x, 2) + Math.pow(self.z - trashcanPos.z, 2));
					if (distance < minDistance) {
						minDistance = distance;
						self.trashcan = self.trashcans[t];
					}
				}
				if (self.trashcan != null) {
					self.target = self.trashcan.pos;
				}
			}
			
			var distance = Math.sqrt(Math.pow(self.x - self.target.x, 2) + Math.pow(self.z - self.target.z, 2));
			if (distance > 0.2) {
				// Compute new position along the line towards the target
				var deltaX = self.target.x - self.x;
				var deltaZ = self.target.z - self.z;
				var signX = (deltaX == 0 ? 0 : deltaX / Math.abs(deltaX));
				var signZ = (deltaZ == 0 ? 0 : deltaZ / Math.abs(deltaZ));
				var m = deltaZ / deltaX;
				var d = Math.min(0.5, distance);
				var dx = signX * d / Math.sqrt(1 + m*m);
				var dz = signZ * d / Math.sqrt(1 + 1/m/m);
				self.x += dx;
				self.z += dz;
				var move = new NetworkMessages.Move(self.id, {x: self.x, y: 0, z: self.z});
				self.broadcast(move);
				self.onMove(self.x, self.z);
			} else {
				if (self.itemToSweep > 0) {
					// We reached the item
					// Check if it's still there (someone might have taken it)
					if (self.location.items[self.itemToSweep] != undefined) {
						self.pauseMovement();
						self.movementTimeout = setTimeout(function() {
							self.sweepItem(self.itemToSweep, function() {
								if (Object.keys(self.inventory).length >= self.capacity)
									self.full = true;
								self.behaviour = self.getBehaviour();
								var behaviourMsg = new NetworkMessages.Behaviour(0, self.id, self.behaviour);
								self.broadcast(behaviourMsg);
								self.updateGrid();
								self.itemToSweep = -1;
								self.startMovement();
							});
						}, 3 * 1000);
					} else {
						self.updateGrid();
						self.itemToSweep = -1;
					}
				} else if (self.full) {
					// We reached the trash can
					self.pauseMovement();
					self.movementTimeout = setTimeout(function() {
						self.trashAll(self.trashcan.id, function() {
							self.full = false;
							self.behaviour = self.getBehaviour();
							var behaviourMsg = new NetworkMessages.Behaviour(0, self.id, self.behaviour);
							self.broadcast(behaviourMsg);
							self.trashcan = null;
							self.startMovement();
						});
					}, 3 * 1000);
				}
			}
			
		}, 10000 / self.location.ups);
			
	},
	
	pauseMovement: function() {
		clearTimeout(this.movementTimeout);
		clearInterval(this.movementInterval);
	},
	
	/*
		Function: sweepItem
		Picks up an item from the ground.
		
		Parameters:
			itemId		-	Item id.
			callback	-	A callback function.
	*/
	sweepItem: function(itemId, callback) {
		var self = this;
		
		self.location.server.db.query("UPDATE object_instance " +
									"SET location_id = NULL, posX = NULL, posY = NULL, posZ = NULL, spy_id = ? " +
									"WHERE id = ?", [self.id, itemId], function(err, rows, fields) {
			if (err) {
				console.log("Error sweeping item");
				throw err;
			} else {
				self.location.pushBroadcast(new NetworkMessages.Destroy(itemId, Constants.Entities.OBJECT));
				self.inventory[itemId] = self.location.items[itemId];
				delete self.location.items[itemId];
				if (callback)
					callback();
			}
		});
	},
	
	/*
		Function: trashAll
		Puts all the items in a trash can
		
		Parameters:
			trashcan	-	Trash can id.
			callback	-	A callback function.
	*/
	trashAll: function(trashcan, callback) {
		var self = this;
		
		self.location.server.db.query("UPDATE object_instance " +
									"SET spy_id = NULL, container_id = ? " +
									"WHERE spy_id = ?", [trashcan, self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error trashing item");
				throw err;
			} else {
				self.itemFactory.buildFromId(trashcan, function(trashcan) {
					var behaviourMsg = new NetworkMessages.Behaviour(trashcan.id, 0, trashcan.behaviour);
					self.location.pushBroadcast(behaviourMsg);
				});
				self.inventory = {};
				if (callback)
					callback();
			}
		});
	},
	
	/*
		Function: getBehaviour
		
		Returns:
			The sweeper's current behaviour.
	*/
	getBehaviour: function(callback) {
		var self = this;
		
		var behaviour = {};
		if (Object.keys(self.inventory).length > 0) {
			behaviour.state = {id: 0, name: "Default"};
			behaviour.name = "Default";
			behaviour.methods = {};
			behaviour.methods[Constants.Methods.ROB] = {name: "Rob", isSubject: false, isObject: true, auto: true, icon: "rob"};
		}
		return behaviour;
	},
	
	/*
		Function: broadcast
		Enqueues message for the adjacent groups.
		
		Parameters:
			message		-	The <NetworkMessage> to be sent.
			ignoreId	-	Do not notify this guy.
	*/
	broadcast: function(message, ignoreId) {
		if (ignoreId === undefined)
			ignoreId = null;
		this.location.pushToAdjacentGroups(this.group, message, ignoreId);
	}
	
});