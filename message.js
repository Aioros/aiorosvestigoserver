/*
	Script: message
*/

var cls = require("./lib/class"),
    _ = require("underscore"),
	Constants = require("./constants");

function isBitSet(value, index) {
	return (value & (1 << index)) != 0;
}

function log2(x) {
	return Math.log(x) / Math.LN2;
}

/*
	Class: NetworkMessage
	Describes a network message and manages encoding/decoding.
	
	Variables:
		message	-	A string description of the message ("hello", "interaction", etc.).
*/
var NetworkMessage = cls.Class.extend({
	/*
		Constructor: init
		Parameters:
			message	-	A string description of the message.
	*/
	init: function(message) {
        this.message = message;
    }
});

/*
	Class: NetworkMessages
	Manages identification and creation of a particular <NetworkMessage> from a byte array.
*/
	
var NetworkMessages = {};
module.exports = NetworkMessages;

/*
	Function: decode
	Parameters:
		msg	-	An encoded message (byte array)
	
	Returns:
		An instance of the correct sub-class of <NetworkMessage>.
*/
NetworkMessages.decode = function(msg) {
	var type = parseInt(msg[0]);
	for (var t in Constants.Messages) {
		if (Constants.Messages[t].value == type)
			return (new NetworkMessages[Constants.Messages[t].messageClass]()).decode(msg);
	}
	return null;
}

/*
	Class: NetworkMessages.Hello
*/
NetworkMessages.Hello = NetworkMessage.extend({
	
	/*
		Constructor: init
	*/
	init: function() {
		this._super("hello");
	},
	
	encode: function() {
		
		var helloPayloadSize = 1;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(helloPayloadSize, 0);
		var helloType = new Buffer(1);
		helloType[0] = Constants.Messages.HELLO.value;
		var msg = Buffer.concat([header, helloType]);
		return msg;
		
	},
	
	decode: function(msg) {
		return new NetworkMessages.Hello();
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Welcome
*/
NetworkMessages.Welcome = NetworkMessage.extend({
	
	/*
		Constructor: init
	*/
	init: function() {
		this._super("welcome");
	},
	
	encode: function() {
	
		var welcomePayloadSize = 1;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(welcomePayloadSize, 0);
		var welcomeType = new Buffer(1);
		welcomeType[0] = Constants.Messages.WELCOME.value;
		var msg = Buffer.concat([header, welcomeType]);
		
		return msg;	
	},
	
	decode: function(msg) {
		return new NetworkMessages.Welcome();
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Login
*/
NetworkMessages.Login = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id			-	Player id on successful login, 0 otherwise.
			name		-	Player name.
			password	-	Player password.
	*/
	init: function(id, name, password) {
		this._super("login");
		this.id = id;
		this.name = name;
		this.password = password;
	},
	
	encode: function() {
		
		var nameBuffer = new Buffer(this.name);
		var termNameBuffer = Buffer.concat([nameBuffer, new Buffer("\0")]);
		var pwBuffer = new Buffer(this.password);
		var termPwBuffer = Buffer.concat([pwBuffer, new Buffer("\0")]);
		
		var loginPayloadSize = 5 + termNameBuffer.length + termPwBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(loginPayloadSize, 0);
		var loginType = new Buffer(1);
		loginType[0] = Constants.Messages.LOGIN.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, loginType, idBuffer, termNameBuffer, termPwBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		var m = 5;
		while (msg[m] != 0) {
			m++;
		}
		var name = msg.toString("utf8", 5, m);
		
		var n = m+1;
		while (msg[n] != 0) {
			n++;
		}
		var password = msg.toString("utf8", m+1, n);
		
		return new NetworkMessages.Login(id, name, password);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Cities
*/
NetworkMessages.Cities = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			data	-	Available cities' data.
	*/
	init: function(data) {
		this._super("cities");
		this.data = data;
	},
	
	encode: function() {
		
		var dataJSON = JSON.stringify(this.data);
		var dataBuffer = new Buffer(dataJSON);
		var termDataBuffer = Buffer.concat([dataBuffer, new Buffer("\0")]);
		
		var citiesPayloadSize = 1 + termDataBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(citiesPayloadSize, 0);
		var citiesType = new Buffer(1);
		citiesType[0] = Constants.Messages.CITIES.value;
		
		var msg = Buffer.concat([header, citiesType, termDataBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var i = 1;
		while (msg[i] != 0) {
			i++;
		}
		var data = JSON.parse(msg.toString("utf8", 1, i));
		
		return new NetworkMessages.Cities(data);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.City
*/
NetworkMessages.City = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id	-	Id of the selected city.
	*/
	init: function(id) {
		this._super("city");
		this.id = id;
	},
	
	encode: function() {
		
		var cityPayloadSize = 5;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(cityPayloadSize, 0);
		var cityType = new Buffer(1);
		cityType[0] = Constants.Messages.CITY.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var msg = Buffer.concat([header, cityType, idBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		return new NetworkMessages.City(id);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.LocationInfo
*/
NetworkMessages.LocationInfo = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			name		-	Location name.
			coords		-	Location coordinates.
			weather		-	Location weather (right now, 1 for rain, 0 otherwise).
			indoor		-	Location type (0 for outdoors, 1 for indoors).
			population	-	Location population.
	*/
	init: function(name, coords, weather, indoor, population) {
		this._super("locationinfo");
		this.name = name;
		this.coords = coords;
		this.weather = weather;
		this.indoor = indoor;
		this.population = population;
	},
	
	encode: function() {
	
		var nameBuffer = new Buffer(this.name);
		var termNameBuffer = Buffer.concat([nameBuffer, new Buffer("\0")]);
		
		var locationPayloadSize = 13 + termNameBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(locationPayloadSize, 0);
		var locationType = new Buffer(1);
		locationType[0] = Constants.Messages.LOCATIONINFO.value;
		var lat = new Buffer(4);
		lat.writeFloatLE(this.coords.lat, 0);
		var lng = new Buffer(4);
		lng.writeFloatLE(this.coords.lng, 0);
		var w = new Buffer(1);
		w[0] = this.weather;
		var i = new Buffer(1);
		i[0] = this.indoor;
		var pop = new Buffer(2);
		pop.writeUInt16LE(this.population, 0);
		var msg = Buffer.concat([header, locationType, termNameBuffer, lat, lng, w, i, pop]);

		return msg;	
	},
	
	decode: function(msg) {
		var m = 1;
		while (msg[m] != 0) {
			m++;
		}
		var name = msg.toString("utf8", 1, m);
		var coords = {};
		coords.lat = msg.readFloatLE(m+1);
		coords.lng = msg.readFloatLE(m+5);
		var weather = msg[m+9];
		var indoor = msg[m+10];
		var population = msg.readUInt16LE(m+11);
		return new NetworkMessages.LocationInfo(name, coords, weather, population);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.PlayerInfo
*/
NetworkMessages.PlayerInfo = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id		-	Player id.
			level	-	Player level.
			xp		-	Player XP.
			money	-	It's a gas.
			home	-	Home again. (Did you just - I did.)
	*/
	init: function(id, level, xp, money, home) {
		this._super("playerinfo");
		this.id = id;
		this.level = level;
		this.xp = xp;
		this.money = money;
		this.home = home;
	},
	
	encode: function() {
	
		var playerPayloadSize = 21;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(playerPayloadSize, 0);
		var playerType = new Buffer(1);
		playerType[0] = Constants.Messages.PLAYERINFO.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var levelBuffer = new Buffer(4);
		levelBuffer.writeUInt32LE(this.level, 0);
		var xpBuffer = new Buffer(4);
		xpBuffer.writeUInt32LE(this.xp, 0);
		var moneyBuffer = new Buffer(4);
		moneyBuffer.writeUInt32LE(this.money, 0);
		var homeBuffer = new Buffer(4);
		homeBuffer.writeUInt32LE(this.home, 0);
		var msg = Buffer.concat([header, playerType, idBuffer, levelBuffer, xpBuffer, moneyBuffer, homeBuffer]);

		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var level = msg.readUInt32LE(5);
		var xp = msg.readUInt32LE(9);
		var money = msg.readUInt32LE(13);
		var home = msg.readUInt32LE(17);
		return new NetworkMessages.PlayerInfo(id, level, xp, money, home);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Notification
*/
NetworkMessages.Notification = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			text	-	Notification text.
	*/
	init: function(text) {
		this._super("notification");
		this.text = text;
	},
	
	encode: function() {
		
		var textBuffer = new Buffer(this.text);
		var termTextBuffer = Buffer.concat([textBuffer, new Buffer("\0")]);
		
		var notifyPayloadSize = 1 + termTextBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(notifyPayloadSize, 0);
		var notifyType = new Buffer(1);
		notifyType[0] = Constants.Messages.NOTIFICATION.value;
		
		var msg = Buffer.concat([header, notifyType, termTextBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var m = 1;
		while (msg[m] != 0) {
			m++;
		}
		var text = msg.toString("utf8", 1, m);
		
		return new NetworkMessages.Notification(text);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Create
*/
NetworkMessages.Create = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id			-	Object id.
			flags		-	Boolean flags (active, outfit, document, hint, token, bot)
			name		-	Object name.
			kind		-	Object kind (<Constants.Entities>).
			coords		-	Object coordinates.
			scale		-	Object scale.
			rotation	-	Object rotation angle.
			textures	-	Object describing all the textures used by the object to be created.
			behaviour	-	Object behaviour (state and available interactions).
			parts		-	Object parts.
	*/
	init: function(id, flags, name, kind, coords, scale, rotation, textures, behaviour, parts) {
		this._super("create");
		this.id = id;
		this.flags = flags;
		this.name = name;
		this.kind = kind;
		this.coords = coords;
		this.scale = scale;
		this.rotation = rotation;
		this.textures = textures;
		if (behaviour == undefined)
			this.behaviour = {};
		else
			this.behaviour = behaviour;
		if (parts == undefined)
			this.parts = [];
		else
			this.parts = parts;
	},
	
	encode: function() {
		
		var nameBuffer = new Buffer(this.name);
		var termNameBuffer = Buffer.concat([nameBuffer, new Buffer("\0")]);
		
		var texturesJSON = JSON.stringify(this.textures);
		var texturesBuffer = new Buffer(texturesJSON);
		var termTexturesBuffer = Buffer.concat([texturesBuffer, new Buffer("\0")]);
		
		var behaviourJSON = JSON.stringify(this.behaviour);
		var behaviourBuffer = new Buffer(behaviourJSON);
		var termBehaviourBuffer = Buffer.concat([behaviourBuffer, new Buffer("\0")]);
		
		var partsJSON = JSON.stringify(this.parts);
		var partsBuffer = new Buffer(partsJSON);
		var termPartsBuffer = Buffer.concat([partsBuffer, new Buffer("\0")]);
		
		var createPayloadSize = 35 + termNameBuffer.length + termTexturesBuffer.length + termBehaviourBuffer.length + termPartsBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(createPayloadSize, 0);
		var createType = new Buffer(1);
		createType[0] = Constants.Messages.CREATE.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
				
		var createFlags = new Buffer(1);
		createFlags[0] = ((this.flags.active ? 1 : 0) * Constants.CreateFlags.ACTIVE) |
						((this.flags.outfit ? 1 : 0) * Constants.CreateFlags.OUTFIT) |
						((this.flags.document ? 1 : 0) * Constants.CreateFlags.DOCUMENT) |
						((this.flags.hint ? 1 : 0) * Constants.CreateFlags.HINT) |
						((this.flags.token ? 1 : 0) * Constants.CreateFlags.TOKEN) |
						((this.flags.bot ? 1 : 0) * Constants.CreateFlags.BOT);
		
		var kindBuffer = new Buffer(4);
		kindBuffer.writeUInt32LE(this.kind, 0);
		var cX = new Buffer(4);
		cX.writeFloatLE(this.coords.x, 0);
		var cY = new Buffer(4);
		cY.writeFloatLE(this.coords.y, 0);
		var cZ = new Buffer(4);
		cZ.writeFloatLE(this.coords.z, 0);
		var sX = new Buffer(4);
		sX.writeFloatLE(this.scale.x, 0);
		var sY = new Buffer(4);
		sY.writeFloatLE(this.scale.y, 0);
		var sZ = new Buffer(4);
		sZ.writeFloatLE(this.scale.z, 0);
		var rotBuffer = new Buffer(1);
		rotBuffer[0] = this.rotation;
		var msg = Buffer.concat([header, createType, idBuffer, createFlags, termNameBuffer, kindBuffer, cX, cY, cZ, sX, sY, sZ, rotBuffer, termTexturesBuffer, termBehaviourBuffer, termPartsBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		var flagsByte = msg[5];
		var flags = {};
		flags.active = isBitSet(flagsByte, log2(Constants.CreateFlags.ACTIVE));
		flags.outfit = isBitSet(flagsByte, log2(Constants.CreateFlags.OUTFIT));
		flags.document = isBitSet(flagsByte, log2(Constants.CreateFlags.DOCUMENT));
		flags.hint = isBitSet(flagsByte, log2(Constants.CreateFlags.HINT));
		flags.token = isBitSet(flagsByte, log2(Constants.CreateFlags.TOKEN));
		flags.bot = isBitSet(flagsByte, log2(Constants.CreateFlags.BOT));
		
		var m = 6;
		while (msg[m] != 0) {
			m++;
		}
		var name = msg.toString("utf8", 6, m);
		var kind = msg.readUInt32LE(m+1);
		var coords;
		coords.x = msg.readFloatLE(m+5);
		coords.y = msg.readFloatLE(m+9);
		coords.z = msg.readFloatLE(m+13);
		var scale;
		scale.x = msg.readFloatLE(m+17);
		scale.y = msg.readFloatLE(m+21);
		scale.z = msg.readFloatLE(m+25);
		var rotation = msg[m+29];
		
		var n = m+30;
		while (msg[n] != 0) {
			n++;
		}
		var textures = JSON.parse(msg.toStrng("utf8", m+30, n));
		
		var o = n+1;
		while (msg[o] != 0) {
			o++;
		}
		var behaviour = JSON.parse(msg.toString("utf8", n+1, o));
		
		var p = o+1;
		while (msg[p] != 0) {
			p++;
		}
		var parts = JSON.parse(msg.toString("utf8", o+1, p));
		
		return new NetworkMessages.Create(id, flags, name, kind, coords, scale, rotation, textures, behaviour, parts);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Spawn
*/
NetworkMessages.Spawn = NetworkMessage.extend({
	/*
		Constructor: init
		Parameters:
			entity	-	The entity to be spawned.
	*/
    init: function(entity) {
        this.entity = entity;
    },
    encode: function() {
		var self = this;
		var behaviour = this.entity.behaviour;
		var flags = {active: true, outfit: false, bot: false};
		if (self.entity instanceof Bot) {
			flags.bot = true;
			behaviour = {};
		}
		var create = new NetworkMessages.Create(this.entity.id, flags, this.entity.name, Constants.Entities.SPY, {x: this.entity.x, y: 0.3, z: this.entity.z}, {x: 1, y: 1, z: 1}, 0, this.entity.outfits, behaviour);
		return create.encode();
    },
	send: function(socket) {
		socket.write(this.encode());
	}
});

/*
	Class: NetworkMessages.Destroy
*/
NetworkMessages.Destroy = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id		-	Object id.
			kind	-	Object kind (<Constants.Entities>).
	*/
	init: function(id, kind) {
		this._super("destroy");
		this.id = id;
		this.kind = kind;
	},
	
	encode: function() {
		
		var destroyPayloadSize = 9;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(destroyPayloadSize, 0);
		var destroyType = new Buffer(1);
		destroyType[0] = Constants.Messages.DESTROY.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var kindBuffer = new Buffer(4);
		kindBuffer.writeUInt32LE(this.kind, 0);
		
		var msg = Buffer.concat([header, destroyType, idBuffer, kindBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var kind = msg.readUInt32LE(5);
		
		return new NetworkMessages.Destroy(id, kind);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Despawn
*/
NetworkMessages.Despawn = NetworkMessage.extend({
	/*
		Constructor: init
		Parameters:
			entity	-	The entity to be despawned.
	*/
    init: function(entity) {
        this.entity = entity;
    },
    encode: function() {
        var destroy = new NetworkMessages.Destroy(this.entity.id, Constants.Entities.SPY);
		return destroy.encode();
    },
	send: function(socket) {
		socket.write(this.encode());
	}
});

/*
	Class: NetworkMessages.Move
*/
NetworkMessages.Move = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id		-	The character's id.
			coords	-	The target coordinates.
	*/
	init: function(id, coords) {
		this._super("move");
		this.id = id;
		this.coords = coords;
	},
	
	encode: function() {
	
		var movePayloadSize = 17;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(movePayloadSize, 0);
		var moveType = new Buffer(1);
		moveType[0] = Constants.Messages.MOVE.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var mX = new Buffer(4);
		mX.writeFloatLE(this.coords.x, 0);
		var mY = new Buffer(4);
		mY.writeFloatLE(this.coords.y, 0);
		var mZ = new Buffer(4);
		mZ.writeFloatLE(this.coords.z, 0);
		var msg = Buffer.concat([header, moveType, idBuffer, mX, mY, mZ]);

		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var coords = {};
		coords.x = msg.readFloatLE(5);
		coords.y = msg.readFloatLE(9);
		coords.z = msg.readFloatLE(13);
		return new NetworkMessages.Move(id, coords);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Texture
*/
NetworkMessages.Texture = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			textureId	-	The requested texture's id.
			textureData	-	The encoded texture image data.
	*/
	init: function(textureId, textureData) {
		this._super("texture");
		this.textureId = textureId;
		this.textureData = textureData;
	},
	
	encode: function() {
		
		var textureIdBuffer = new Buffer(this.textureId);
		var termTexIdBuffer = Buffer.concat([textureIdBuffer, new Buffer("\0")]);
		
		var texturePayloadSize = 1 + termTexIdBuffer.length + this.textureData.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(texturePayloadSize, 0);
		var textureType = new Buffer(1);
		textureType[0] = Constants.Messages.TEXTURE.value;
				
		var msg = Buffer.concat([header, textureType, termTexIdBuffer, this.textureData]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var m = 1;
		while (msg[m] != 0) {
			m++;
		}
		var textureId = msg.toString("utf8", 1, m);
		if (msg.length > m+1)
			var textureData = msg.slice(m+1);
		else
			var textureData = new Buffer(0);
		
		return new NetworkMessages.Texture(textureId, textureData);
	},
	
	send: function(socket) {
		var mess = this.encode();
		console.log("Sending texture message: " + mess.length + " bytes");
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.List
*/
NetworkMessages.List = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			ids	-	Array of the ids of the relevant entities.
	*/
	init: function(ids) {
		this._super("list");
		this.ids = ids;
	},
	
	encode: function() {
	
		var listPayloadSize = 1 + this.ids.length * 4;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(listPayloadSize, 0);
		var listType = new Buffer(1);
		listType[0] = Constants.Messages.LIST.value;
		var msg = Buffer.concat([header, listType]);
		for (var i=0; i<this.ids.length; i++) {
			var id = new Buffer(4);
			id.writeUInt32LE(this.ids[i], 0);
			msg = Buffer.concat([msg, id]);
		}

		return msg;	
	},
	
	decode: function(msg) {
		var len = (msg.length - 1) / 4;
		var ids = [];
		for (var i=0; i<len; i++) {
			ids[i] = msg.readUInt32LE(i*4 + 1);
		}
		
		return new NetworkMessages.List(ids);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Chat
*/
NetworkMessages.Chat = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id		-	Speaker's id.
			text	-	Chat text.
	*/
	init: function(id, text) {
		this._super("chat");
		this.id = id;
		this.text = text;
	},
	
	encode: function() {
		
		var textBuffer = new Buffer(this.text);
		var termTextBuffer = Buffer.concat([textBuffer, new Buffer("\0")]);
		
		var chatPayloadSize = 5 + termTextBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(chatPayloadSize, 0);
		var chatType = new Buffer(1);
		chatType[0] = Constants.Messages.CHAT.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, chatType, idBuffer, termTextBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		var m = 5;
		while (msg[m] != 0) {
			m++;
		}
		var text = msg.toString("utf8", 5, m);
		
		return new NetworkMessages.Chat(id, text);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Inventory
*/
NetworkMessages.Inventory = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			ids	-	Array of the ids of the inventory objects.
	*/
	init: function(ids) {
		this._super("inventory");
		this.ids = ids;
	},
	
	encode: function() {
	
		var invPayloadSize = 1 + this.ids.length * 4;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(invPayloadSize, 0);
		var invType = new Buffer(1);
		invType[0] = Constants.Messages.INVENTORY.value;
		var msg = Buffer.concat([header, invType]);
		for (var i=0; i<this.ids.length; i++) {
			var id = new Buffer(4);
			id.writeUInt32LE(this.ids[i], 0);
			msg = Buffer.concat([msg, id]);
		}

		return msg;	
	},
	
	decode: function(msg) {
		var len = (msg.length - 1) / 4;
		var ids = [];
		for (var i=0; i<len; i++) {
			ids[i] = msg.readUInt32LE(i*4 + 1);
		}
		
		return new NetworkMessages.Inventory(ids);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Collect
*/
NetworkMessages.Collect = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id	-	Id of the object to be collected.
	*/
	init: function(id) {
		this._super("collect");
		this.id = id;
	},
	
	encode: function() {
		
		var collectPayloadSize = 5;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(collectPayloadSize, 0);
		var collectType = new Buffer(1);
		collectType[0] = Constants.Messages.COLLECT.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var msg = Buffer.concat([header, collectType, idBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		return new NetworkMessages.Collect(id);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Drop
*/
NetworkMessages.Drop = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id		-	Id of the object to be dropped.
			coords	-	Where to drop it.
	*/
	init: function(id, coords) {
		this._super("drop");
		this.id = id;
		this.coords = coords;
	},
	
	encode: function() {
	
		var dropPayloadSize = 17;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(dropPayloadSize, 0);
		var dropType = new Buffer(1);
		dropType[0] = Constants.Messages.DROP.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var dX = new Buffer(4);
		dX.writeFloatLE(this.coords.x, 0);
		var dY = new Buffer(4);
		dY.writeFloatLE(this.coords.y, 0);
		var dZ = new Buffer(4);
		dZ.writeFloatLE(this.coords.z, 0);
		var msg = Buffer.concat([header, dropType, idBuffer, dX, dY, dZ]);

		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var coords = {};
		coords.x = msg.readFloatLE(5);
		coords.y = msg.readFloatLE(9);
		coords.z = msg.readFloatLE(13);
		return new NetworkMessages.Drop(id, coords);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Interaction
*/
NetworkMessages.Interaction = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id		-	Interaction id.
			subject	-	Interaction subject id.
			object	-	Interaction object id.
	*/
	init: function(id, subject, object) {
		this._super("interaction");
		this.id = id;
		this.subject = subject;
		this.object = object;
	},
	
	encode: function() {
	
		var interactPayloadSize = 13;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(interactPayloadSize, 0);
		var interactType = new Buffer(1);
		interactType[0] = Constants.Messages.INTERACTION.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var subjBuffer = new Buffer(4);
		subjBuffer.writeUInt32LE(this.subject, 0);
		var objBuffer = new Buffer(4);
		objBuffer.writeUInt32LE(this.object, 0);
		var msg = Buffer.concat([header, interactType, idBuffer, subjBuffer, objBuffer]);

		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var subject = msg.readUInt32LE(5);
		var object = msg.readUInt32LE(9);
		return new NetworkMessages.Interaction(id, subject, object);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Dialogue
*/
NetworkMessages.Dialogue = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id		-	Dialogue id.
			bot		-	Bot id.
			over	-	True if the dialogue text is over.
			text	-	Dialogue text.
	*/
	init: function(id, bot, over, text) {
		this._super("dialogue");
		this.id = id;
		this.bot = bot;
		this.over = over;
		this.text = text;
	},
	
	encode: function() {
		
		var textBuffer = new Buffer(this.text);
		var termTextBuffer = Buffer.concat([textBuffer, new Buffer("\0")]);
		
		var dialoguePayloadSize = 10 + termTextBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(dialoguePayloadSize, 0);
		var dialogueType = new Buffer(1);
		dialogueType[0] = Constants.Messages.DIALOGUE.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var botBuffer = new Buffer(4);
		botBuffer.writeUInt32LE(this.bot, 0);
		var overBuffer = new Buffer(1);
		overBuffer[0] = (!this.over ? 0 : 1);
		
		var msg = Buffer.concat([header, dialogueType, idBuffer, botBuffer, overBuffer, termTextBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var bot = msg.readUInt32LE(5);
		var over = msg[9];
		var m = 10;
		while (msg[m] != 0) {
			m++;
		}
		var text = msg.toString("utf8", 10, m);
		
		return new NetworkMessages.Dialogue(id, bot, over, text);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Behaviour
*/
NetworkMessages.Behaviour = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			objectId	-	Object id.
			spyId		-	Spy id.
			behaviour	-	Object behaviour.
	*/
	init: function(objectId, spyId, behaviour) {
		this._super("behaviour");
		this.objectId = objectId;
		this.spyId = spyId;
		this.behaviour = behaviour;
	},
	
	encode: function() {
		
		var behaviourBuffer = new Buffer(JSON.stringify(this.behaviour));
		var termBehaviourBuffer = Buffer.concat([behaviourBuffer, new Buffer("\0")]);
		
		var behaviourPayloadSize = 9 + termBehaviourBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(behaviourPayloadSize, 0);
		var behaviourType = new Buffer(1);
		behaviourType[0] = Constants.Messages.BEHAVIOUR.value;
		var objectBuffer = new Buffer(4);
		objectBuffer.writeUInt32LE(this.objectId, 0);
		var spyBuffer = new Buffer(4);
		spyBuffer.writeUInt32LE(this.spyId, 0);
		
		var msg = Buffer.concat([header, behaviourType, objectBuffer, spyBuffer, termBehaviourBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var objectId = msg.readUInt32LE(1);
		var spyId = msg.readUInt32LE(5);
		
		var m = 9;
		while (msg[m] != 0) {
			m++;
		}
		var behaviour = JSON.parse(msg.toString("utf8", 9, m));
		
		return new NetworkMessages.Behaviour(objectId, spyId, behaviour);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Textures
*/
NetworkMessages.Textures = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id			-	Object id.
			kind		-	Object kind (<Constants.Entities>).
			textures	-	Object textures.
	*/
	init: function(id, kind, textures) {
		this._super("textures");
		this.id = id;
		this.kind = kind;
		this.textures = textures;
	},
	
	encode: function() {
		
		var texturesBuffer = new Buffer(JSON.stringify(this.textures));
		var termTexturesBuffer = Buffer.concat([texturesBuffer, new Buffer("\0")]);
		
		var texturesPayloadSize = 9 + termTexturesBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(texturesPayloadSize, 0);
		var texturesType = new Buffer(1);
		texturesType[0] = Constants.Messages.TEXTURES.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var kindBuffer = new Buffer(4);
		kindBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, texturesType, idBuffer, kindBuffer, termTexturesBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var kind = msg.readUInt32LE(5);
		
		var m = 9;
		while (msg[m] != 0) {
			m++;
		}
		var textures = JSON.parse(msg.toString("utf8", 9, m));
		
		return new NetworkMessages.Textures(id, kind, textures);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Insert
*/
NetworkMessages.Insert = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id			-	Content object id.
			container	-	Container object id.
	*/
	init: function(id, container) {
		this._super("insert");
		this.id = id;
		this.container = container;
	},
	
	encode: function() {
	
		var insertPayloadSize = 9;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(insertPayloadSize, 0);
		var insertType = new Buffer(1);
		insertType[0] = Constants.Messages.INSERT.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var containerBuffer = new Buffer(4);
		containerBuffer.writeUInt32LE(this.container, 0);
		var msg = Buffer.concat([header, insertType, idBuffer, containerBuffer]);

		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var container = msg.readUInt32LE(5);
		return new NetworkMessages.Insert(id, container);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Loot
*/
NetworkMessages.Loot = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id	-	Container object id.
	*/
	init: function(id) {
		this._super("loot");
		this.id = id;
	},
	
	encode: function() {
		
		var lootPayloadSize = 5;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(lootPayloadSize, 0);
		var lootType = new Buffer(1);
		lootType[0] = Constants.Messages.LOOT.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, lootType, idBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		return new NetworkMessages.Loot(id);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Equip
*/
NetworkMessages.Equip = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id	-	Outfit id.
	*/
	init: function(id) {
		this._super("equip");
		this.id = id;
	},
	
	encode: function() {
		
		var equipPayloadSize = 5;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(equipPayloadSize, 0);
		var equipType = new Buffer(1);
		equipType[0] = Constants.Messages.EQUIP.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, equipType, idBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		return new NetworkMessages.Equip(id);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Unequip
*/
NetworkMessages.Unequip = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id	-	Outfit id.
	*/
	init: function(id) {
		this._super("unequip");
		this.id = id;
	},
	
	encode: function() {
		
		var unequipPayloadSize = 5;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(unequipPayloadSize, 0);
		var unequipType = new Buffer(1);
		unequipType[0] = Constants.Messages.UNEQUIP.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, unequipType, idBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		return new NetworkMessages.Unequip(id);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Teleport
*/
NetworkMessages.Teleport = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id			-	The character's id.
			location	-	The target location's id.
			coords		-	The target coordinates.
	*/
	init: function(id, location, coords) {
		this._super("teleport");
		this.id = id;
		this.location = location;
		this.coords = coords;
	},
	
	encode: function() {
	
		var teleportPayloadSize = 21;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(teleportPayloadSize, 0);
		var teleportType = new Buffer(1);
		teleportType[0] = Constants.Messages.TELEPORT.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var locationBuffer = new Buffer(4);
		locationBuffer.writeUInt32LE(this.location, 0);
		var tX = new Buffer(4);
		tX.writeFloatLE(this.coords.x, 0);
		var tY = new Buffer(4);
		tY.writeFloatLE(this.coords.y, 0);
		var tZ = new Buffer(4);
		tZ.writeFloatLE(this.coords.z, 0);
		var msg = Buffer.concat([header, teleportType, idBuffer, locationBuffer, tX, tY, tZ]);

		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var location = msg.readUInt32LE(5);
		var coords = {};
		coords.x = msg.readFloatLE(9);
		coords.y = msg.readFloatLE(13);
		coords.z = msg.readFloatLE(17);
		return new NetworkMessages.Teleport(id, location, coords);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Read
*/
NetworkMessages.Examine = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id		-	Document id.
			text	-	Document text.
	*/
	init: function(id, text) {
		this._super("examine");
		this.id = id;
		this.text = text;
	},
	
	encode: function() {
		
		var textBuffer = new Buffer(this.text);
		var termTextBuffer = Buffer.concat([textBuffer, new Buffer("\0")]);
		
		var examinePayloadSize = 5 + termTextBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(examinePayloadSize, 0);
		var examineType = new Buffer(1);
		examineType[0] = Constants.Messages.READ.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, examineType, idBuffer, termTextBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		var m = 5;
		while (msg[m] != 0) {
			m++;
		}
		var text = msg.toString("utf8", 5, m);
		
		return new NetworkMessages.Examine(id, text);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Hint
*/
NetworkMessages.Hint = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id			-	Hint id.
			question	-	Hint question.
			answer		-	Hint answer.
	*/
	init: function(id, question, answer) {
		this._super("hint");
		this.id = id;
		this.question = question;
		this.answer = answer ? answer : "";
	},
	
	encode: function() {
		
		var qBuffer = new Buffer(this.question);
		var termQBuffer = Buffer.concat([qBuffer, new Buffer("\0")]);
		var aBuffer = new Buffer(this.answer);
		var termABuffer = Buffer.concat([aBuffer, new Buffer("\0")]);
		
		var hintPayloadSize = 5 + termQBuffer.length + termABuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(hintPayloadSize, 0);
		var hintType = new Buffer(1);
		hintType[0] = Constants.Messages.HINT.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, hintType, idBuffer, termQBuffer, termABuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		var m = 5;
		while (msg[m] != 0) {
			m++;
		}
		var question = msg.toString("utf8", 5, m);
		
		var n = m+1;
		while (msg[n] != 0) {
			n++;
		}
		var answer = msg.toString("utf8", m+1, n);
		
		return new NetworkMessages.Hint(id, question, answer);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Token
*/
NetworkMessages.Token = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id	-	Token id.
	*/
	init: function(id) {
		this._super("token");
		this.id = id;
	},
	
	encode: function() {
		
		var tokenPayloadSize = 5;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(tokenPayloadSize, 0);
		var tokenType = new Buffer(1);
		tokenType[0] = Constants.Messages.TOKEN.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, tokenType, idBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		return new NetworkMessages.Token(id);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Win
*/
NetworkMessages.Win = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			reward	-	Reward data.
	*/
	init: function(reward) {
		this._super("win");
		if (reward) {
			this.xp = reward.xp;
			this.text = reward.message;
		}
	},
	
	encode: function() {
		
		var textBuffer = new Buffer(this.text);
		var termTextBuffer = Buffer.concat([textBuffer, new Buffer("\0")]);
		
		var winPayloadSize = 5 + termTextBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(winPayloadSize, 0);
		var winType = new Buffer(1);
		winType[0] = Constants.Messages.WIN.value;
		var xpBuffer = new Buffer(4);
		xpBuffer.writeUInt32LE(this.xp, 0);
		
		var msg = Buffer.concat([header, winType, xpBuffer, termTextBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var xp = msg.readUInt32LE(1);
		
		var m = 5;
		while (msg[m] != 0) {
			m++;
		}
		var text = msg.toString("utf8", 5, m);
		
		return new NetworkMessages.Win({xp: xp, message: text});
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Rob
*/
NetworkMessages.Rob = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id	-	Victim id.
	*/
	init: function(id) {
		this._super("rob");
		this.id = id;
	},
	
	encode: function() {
		
		var robPayloadSize = 5;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(robPayloadSize, 0);
		var robType = new Buffer(1);
		robType[0] = Constants.Messages.ROB.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, robType, idBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		return new NetworkMessages.Rob(id);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Give
*/
NetworkMessages.Give = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			recipient	-	Recipient player id.
			object		-	Object id.
	*/
	init: function(recipient, object) {
		this._super("give");
		this.recipient = recipient;
		this.object = object;
	},
	
	encode: function() {
	
		var givePayloadSize = 9;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(givePayloadSize, 0);
		var giveType = new Buffer(1);
		giveType[0] = Constants.Messages.GIVE.value;
		var recipientBuffer = new Buffer(4);
		recipientBuffer.writeUInt32LE(this.recipient, 0);
		var objectBuffer = new Buffer(4);
		objectBuffer.writeUInt32LE(this.object, 0);
		var msg = Buffer.concat([header, giveType, recipientBuffer, objectBuffer]);

		return msg;	
	},
	
	decode: function(msg) {
		var recipient = msg.readUInt32LE(1);
		var object = msg.readUInt32LE(5);
		return new NetworkMessages.Give(recipient, object);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.MoneyTransfer
*/
NetworkMessages.MoneyTransfer = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			recipientId		-	Recipient id.
			recipientKind	-	Recipient kind (<Constants.Entities>).
			amount			-	Money amount.
	*/
	init: function(recipientId, recipientKind, amount) {
		this._super("moneytransfer");
		this.recipientId = recipientId;
		this.recipientKind = recipientKind;
		this.amount = amount;
	},
	
	encode: function() {
	
		var moneyPayloadSize = 10;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(moneyPayloadSize, 0);
		var moneyType = new Buffer(1);
		moneyType[0] = Constants.Messages.MONEYTRANSFER.value;
		var recIdBuffer = new Buffer(4);
		recIdBuffer.writeUInt32LE(this.recipientId, 0);
		var recKindBuffer = new Buffer(1);
		recKindBuffer[0] = this.recipientKind;
		var amountBuffer = new Buffer(4);
		amountBuffer.writeUInt32LE(this.recipientKind, 0);
		var msg = Buffer.concat([header, moneyType, recIdBuffer, recKindBuffer, amount]);

		return msg;	
	},
	
	decode: function(msg) {
		var recipientId = msg.readUInt32LE(1);
		var recipientKind = msg[5];
		var amount = msg.readUInt32LE(6);
		return new NetworkMessages.MoneyTransfer(recipientId, recipientKind, amount);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Sell
*/
NetworkMessages.Sell = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			item		-	Item id.
			seller		-	Seller id.
			buyer		-	Buyer id.
			price		-	Selling price.
			buyers		-	Potential buyers.
	*/
	init: function(item, seller, buyer, price, buyers) {
		this._super("sell");
		this.item = item;
		this.seller = seller;
		this.buyer = buyer;
		this.price = price;
		this.buyers = buyers;
	},
	
	encode: function() {
		
		var buyersBuffer = new Buffer(JSON.stringify(this.buyers));
		var termBuyersBuffer = Buffer.concat([buyersBuffer, new Buffer("\0")]);
		
		var sellPayloadSize = 17 + termBuyersBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(sellPayloadSize, 0);
		var sellType = new Buffer(1);
		sellType[0] = Constants.Messages.SELL.value;
		var itemBuffer = new Buffer(4);
		itemBuffer.writeUInt32LE(this.item, 0);
		var sellerBuffer = new Buffer(4);
		sellerBuffer.writeUInt32LE(this.seller, 0);
		var buyerBuffer = new Buffer(4);
		buyerBuffer.writeUInt32LE(this.buyer, 0);
		var priceBuffer = new Buffer(4);
		priceBuffer.writeUInt32LE(this.price, 0);
		
		var msg = Buffer.concat([header, sellType, itemBuffer, sellerBuffer, buyerBuffer, priceBuffer, termBuyersBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var item = msg.readUInt32LE(1);
		var seller = msg.readUInt32LE(5);
		var buyer = msg.readUInt32LE(9);
		var price = msg.readUInt32LE(13);
		
		var m = 17;
		while (msg[m] != 0) {
			m++;
		}
		var buyers = JSON.parse(msg.toString("utf8", 17, m));
		
		return new NetworkMessages.Sell(item, seller, buyer, price, buyers);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Animation
*/
NetworkMessages.Animation = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id		-	Spy id.
			name	-	Animation name.
	*/
	init: function(id, name) {
		this._super("animation");
		this.id = id;
		this.name = name;
	},
	
	encode: function() {
		
		var nameBuffer = new Buffer(this.name);
		var termNameBuffer = Buffer.concat([nameBuffer, new Buffer("\0")]);
		
		var animationPayloadSize = 5 + termNameBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(animationPayloadSize, 0);
		var animationType = new Buffer(1);
		animationType[0] = Constants.Messages.ANIMATION.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		
		var msg = Buffer.concat([header, animationType, idBuffer, termNameBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		var m = 5;
		while (msg[m] != 0) {
			m++;
		}
		var name = msg.toString("utf8", 5, m);
		
		return new NetworkMessages.Animation(id, name);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Catalog
*/
NetworkMessages.Catalog = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			data	-	Available products' data.
	*/
	init: function(data) {
		this._super("catalog");
		this.data = data;
	},
	
	encode: function() {
		
		var dataJSON = JSON.stringify(this.data);
		var dataBuffer = new Buffer(dataJSON);
		var termDataBuffer = Buffer.concat([dataBuffer, new Buffer("\0")]);
		
		var catalogPayloadSize = 1 + termDataBuffer.length;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(catalogPayloadSize, 0);
		var catalogType = new Buffer(1);
		catalogType[0] = Constants.Messages.CATALOG.value;
		
		var msg = Buffer.concat([header, catalogType, termDataBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var i = 1;
		while (msg[i] != 0) {
			i++;
		}
		var data = JSON.parse(msg.toString("utf8", 1, i));
		
		return new NetworkMessages.Catalog(data);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});

/*
	Class: NetworkMessages.Buy
*/
NetworkMessages.Buy = NetworkMessage.extend({
	
	/*
		Constructor: init
		Parameters:
			id	-	Id of the selected object.
	*/
	init: function(id) {
		this._super("buy");
		this.id = id;
	},
	
	encode: function() {
		
		var buyPayloadSize = 5;
		var header = new Buffer(Constants.NET_HEADER_SIZE);
		header.writeUInt32LE(buyPayloadSize, 0);
		var buyType = new Buffer(1);
		buyType[0] = Constants.Messages.BUY.value;
		var idBuffer = new Buffer(4);
		idBuffer.writeUInt32LE(this.id, 0);
		var msg = Buffer.concat([header, buyType, idBuffer]);
		
		return msg;	
	},
	
	decode: function(msg) {
		var id = msg.readUInt32LE(1);
		
		return new NetworkMessages.Buy(id);
	},
	
	send: function(socket) {
		socket.write(this.encode());
	}
	
});