/*
	Script: server
	Main server script. Receives connections and dispatches them to the cities.
*/

var net = require("net"),
	fs = require("fs"),
	_ = require("underscore"),
	crypto = require('crypto'),
	Location = require("./location");
	Player = require("./player"),
	Db = require("mysql");

/*
	Function: start
	Starts the TCP server and defines the behaviour when a connection is received.
	
	Parameters:
		config	-	Configuration object.
*/
function start(config) {
	
	config.host = process.env.HOST ? process.env.HOST : config.host;
	config.dbOpts = process.env.HOST ? config.dbOpts : config.dbOptsOld2;
	
	var server = net.createServer();
	server.listen(config.port, config.host);	
	server.on("connection", function(socket) {
		
		console.log('CONNECTED: ' + socket.remoteAddress +':'+ socket.remotePort);
		// Disable Nagle algorithm (output buffering)
		socket.setNoDelay(true);
		
		var player = new Player(socket, server);
		
	});
	
	server.on("error", function(err) {
		console.log("ERROR: " + err.code);
	});
	
	server.db = Db.createConnection(config.dbOpts);
	server.db.connect(function() {
		console.log("Connected to DB");
	});
	
	// Reconnect to DB in case of errors
	server.onDbError = function(err) {
		if (!err.fatal) {
			return;
		}
		if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
			throw err;
		}
		console.log('Re-connecting lost connection');
		server.db = Db.createConnection(config.dbOpts);
		server.db.connect(function() {
			console.log("Connected to DB");
		});
		server.db.on("error", server.onDbError);
	};
	
	server.db.on("error", server.onDbError);
	
	server.locations = [];
	
	// Create locations from DB
	server.db.query("SELECT id, name, lat, lng, weather, indoor, max_players, descriptor, store_query, copy_of FROM location", function(err, rows, fields) {
		if (err) {
			console.log("Error saving position");
			throw err;
		} else {
			for (var l in rows) {
				// Initialise the location
				var location = new Location(rows[l].id, rows[l].name, rows[l].max_players, rows[l].store_query, rows[l].copy_of, server);
				// Fill the location with map information
				location.run(__dirname + "/maps/" + rows[l].descriptor + ".json");
				// Add the location to the array
				server.locations.push(location);
			}
		}
	});
	
	/*
		Function: addLocation
		Adds a newly created location from the DB.
		
		Parameters:
			id			-	The location id.
			callback	-	A callback function.
	*/
	server.addLocation = function(id, callback) {
		server.db.query("SELECT id, name, lat, lng, weather, indoor, max_players, descriptor, store_query, copy_of FROM location WHERE id = ?", [id], function(err, rows, fields) {
			if (err) {
				console.log("Error adding location");
				throw err;
			} else {
				// Initialise the location
				var location = new Location(rows[0].id, rows[0].name, rows[0].max_players, rows[0].store_query, rows[0].copy_of, server);
				console.log("ADDED LOCATION", location);
				// Fill the location with map information
				location.run(__dirname + "/maps/" + rows[0].descriptor + ".json", function() {
					// Add the location to the array
					server.locations.push(location);
					callback(location);
				});
			}
		});
	},
	
	/*
		Function: removeLocation
		Removes a location from the server.
		
		Parameters:
			location	-	The location.
			callback	-	A callback function.
	*/
	server.removeLocation = function(location, callback) {
		location.stop();
		server.db.query("DELETE FROM location WHERE id = ?", [location.id], function(err, rows, fields) {
			if (err) {
				console.log("Error removing location");
				throw err;
			} else {
				server.locations.splice(server.locations.indexOf(location), 1);
				if (callback)
					callback();
			}
		});
	},
	
	/*
		Function: getLocationDistribution
		Parameters:
			locations	-	The array of locations.
			
		Returns:
			An array of location populations.
	*/
	server.getLocationDistribution = function(locations) {
		var distribution = [];
		
		_.each(locations, function(location) {
			distribution.push(location.playerCount);
		});
		return distribution;
	};
	
	/*
		Function: getCities	
		Returns:
			An object containing information about all the available cities and passes it to a callback function.
	*/
	server.getCities = function() {
		var citiesData = [];
		for (var c in server.locations) {
			var location = server.locations[c];
			// Only outdoor locations are cities
			if (location.info.indoor == 0) {
				citiesData.push({id: location.id,
								name: location.name,
								coords: [location.info.lat, location.info.lng],
								population: location.playerCount,
								maxPlayers: location.maxPlayers});
			}
		}
		return citiesData;
	};
	
	/*
		Function: getLocationById
		Parameters:
			id	-	Location id.
		
		Returns:
			The location having the requested id.
	*/
	server.getLocationById = function(id) {
		for (var l in server.locations) {
			if (server.locations[l].id == id)
				return server.locations[l];
		}
		return null;
	};
	
	/*
		Function: createHint
		Instantiates a hint and passes its id to a callback function.
		
		Parameters:
			hintData	- The hint descriptor object.
			player		- The player who found the hint.
			callback	- A callback function.
	*/
	server.createHint = function(hintData, player, callback) {
		// Choose a random hint object type
		server.db.query("SELECT id FROM object WHERE hint = 1", function(err, rows, fields) {
			if (err) {
				console.log("Error creating hint");
				throw err;
			} else {
				var rand = Math.floor(Math.random() * rows.length);
				var object_id = rows[rand].id;
				// A fantastic algorithm will create a real hint right here. Someday.
				// TEMP: this is a temporary and simple management of the hint/secret location mechanism. Baby steps.
				// Find all documents in the cluster
				var cluster = hintData.cluster;
				server.db.query("SELECT location_id " +
								"FROM object_instance " +
								"WHERE doc_cluster_element_id IN (SELECT id " +
																	"FROM doc_cluster_element " +
																	"WHERE doc_cluster_id = ?) " +
								"AND spy_id IS NULL " + 			// This is temporary too
								"AND location_id IS NOT NULL " +	// So is this
								"AND copy_of IS NULL", [cluster], function(err, rows, fields) {
					if (err) {
						console.log("Error computing document cluster");
						throw err;
					} else {
						// Choose a random one
						var rand = Math.floor(Math.random() * rows.length);
						hintData.location = rows[rand].location_id;
						hintData.question = "What is the capital of Honduras?";
						hintData.answer = "Tegucigalpa";
						server.db.query("INSERT INTO object_instance (object_id, rotation, spy_id, state, properties) " +
										"VALUES (?, 0, ?, 0, ?)", [object_id, player.id, JSON.stringify(hintData)], function(err, result) {
							if (err) {
								console.log("Error creating hint");
								throw err;
							} else {
								if (callback)
									callback(result.insertId);
							}
						});
					}
				});
			}
		});
	};
	
	/*
		Function: createToken
		Instantiates a mission token and passes its id to a callback function.
		
		Parameters:
			location	- The secret location id.
			player		- The player who found the hint.
			callback	- A callback function.
	*/
	server.createToken = function(location, player, callback) {
		// Choose a random token object type
		server.db.query("SELECT id FROM object WHERE token = 1", function(err, rows, fields) {
			if (err) {
				console.log("Error creating token");
				throw err;
			} else {
				var rand = Math.floor(Math.random() * rows.length);
				var object_id = rows[rand].id;
				var properties = {location: location};
				server.db.query("INSERT INTO object_instance (object_id, rotation, spy_id, state, properties) " +
								"VALUES (?, 0, ?, 0, ?)", [object_id, player.id, JSON.stringify(properties)], function(err, result) {
					if (err) {
						console.log("Error creating token");
						throw err;
					} else {
						if (callback)
							callback(result.insertId);
					}
				});
			}
		});
	};
	
	/*
		Function: createMoneyItem
		Instantiates a money container in the DB.
		
		Parameters:
			amount		- The money amount.
			location	- The location id.
			coords		- The item coordinates.
			callback	- A callback function.
	*/
	server.createMoneyItem = function(amount, location, coords, callback) {
		server.db.query("INSERT INTO object_instance (object_id, location_id, posX, posY, posZ, rotation, state, properties) " +
						"VALUES (10, ?, ?, ?, ?, 0, 0, ?)", [location, coords.x, coords.y, coords.z, '{"money":' + amount + '}'], function(err, result) {
			if (err) {
				console.log("Error creating money item");
				throw err;
			} else {
				if (callback)
					callback(result.insertId);
			}
		});
	};
	
	/*
		Function: createItem
		Instantiates an item or an items bundle in the DB.
		
		Parameters:
			object_id	- The object id.
			location	- The location id.
			coords		- The item coordinates.
			spy			- The spy id.
			callback	- A callback function.
	*/
	server.createItem = function(object_id, location, coords, spy, callback) {
		var self = this;
		if (coords == null)
			coords = {x: null, y: null, z: null};
			
		// Check if it's a single item or a bundle
		server.db.query("SELECT o.properties, bo.object_id, bo.variable " +
						"FROM object o LEFT JOIN bundle_object bo ON bo.bundle_id = o.id " +
						"WHERE o.id = ? " +
						"ORDER BY bo.index", [object_id], function(err, rows, fields) {
			if (err) {
				console.log("Error buying item");
				throw err;
			} else {
				var properties = {};
				if (rows[0].properties != null && rows[0].properties.length > 0) {
					properties = JSON.parse(rows[0].properties);
				}
				var onBundle = properties.onBundle;
				var isBundle = (onBundle != undefined);
				var query = "";
				var params = [];
				if (!isBundle) {
					query = "INSERT INTO object_instance (object_id, location_id, posX, posY, posZ, rotation, spy_id, state) " +
							"VALUES (?, ?, ?, ?, ?, 0, ?, 0)";
					params = [object_id, location, coords.x, coords.y, coords.z, spy];
				} else {
					var components = [];
					var variables = [];
					for (var r in rows) {
						components.push({id: rows[r].object_id, variable: rows[r].variable});
						variables.push(rows[r].variable);
					}
					var onBundleFunction = new Function(variables.join(), onBundle);
					query = "INSERT INTO object_instance (object_id, location_id, posX, posY, posZ, rotation, spy_id, state) " +
							"VALUES ";
					for (var c in components) {
						query += "(" + components[c].id + ", ?, ?, ?, ?, 0, ?, 0),";
						params.push(location, coords.x, coords.y, coords.z, spy);
					}
					query = query.slice(0, -1);
				}
				server.db.query(query, params, function(err, result) {
					if (err) {
						console.log("Error creating item");
						throw err;
					} else {
						var inserted = [];
						for (var i=0; i<result.affectedRows; i++) {
							inserted.push(result.insertId + i);
						}
						if (callback)
							callback(inserted, onBundleFunction, components);
					}
				});
			}
		});
	};
	
	/*
		Function: duplicateItem
		Creates a copy of an object in the DB.
		
		Parameters:
			item		-	The item id.
			player		-	The player id.
			callback	-	A callback function.
	*/
	server.duplicateItem = function(item, player, callback) {
		server.db.query("INSERT INTO object_instance (object_id, location_id, posX, posY, posZ, rotation, container_id, spy_id, state, properties) " +
							"SELECT object_id, NULL, posX, posY, posZ, rotation, NULL, ?, state, properties " +
							"FROM object_instance " +
							"WHERE id = ?", [player, item], function(err, result) {
			if (err) {
				console.log("Error copying item");
				throw err;
			} else {
				if (callback)
					callback(result.insertId)
			}
		});
	};
	
	console.log('Server listening on ' + config.host +':'+ config.port);
}

/*
	Function: getConfigFile
	Parses a JSON file and applies a callback function
	
	Parameters:
		path		-	The path of JSON config file
		callback	-	The callback function.
*/
function getConfigFile(path, callback) {
    fs.readFile(path, 'utf8', function(err, json_string) {
        if (err) {
            console.error("Could not open config file:", err.path);
            callback(null);
        } else {
            callback(JSON.parse(json_string));
        }
    });
}

// This is the actual starting point of the script

// A custom configuration file can be provided too
var defaultConfigPath = __dirname + '/config.json',
    customConfigPath = '';

// It can also be provided from the shell
process.argv.forEach(function (val, index, array) {
    if (index === 2) {
        customConfigPath = val;
    }
});

// Parse the default config file, then parse the custom config file, then start.
getConfigFile(defaultConfigPath, function(defaultConfig) {
    getConfigFile(customConfigPath, function(localConfig) {
        if (localConfig) {
            start(localConfig);
        } else if(defaultConfig) {
            start(defaultConfig);
        } else {
            console.error("Server cannot start without any configuration file.");
            process.exit(1);
        }
    });
});