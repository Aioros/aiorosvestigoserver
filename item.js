/*
	Script: item
*/

var cls = require("./lib/class"),
	_ = require("underscore"),
	fs = require('fs'),
	NetworkMessages = require("./message");

var Items = {};
module.exports = Items;

/*
	Class: Item
	Describes a game item.
	
	Variables:
		id						-	Item's instance id.
		object_id				-	Item's object type id.
		flags					-	Boolean flags (pickable, container, outfit, door, document, hint, token)
		name					-	Item name.
		pos						-	Item position.
		scale					-	Item size.
		rotation				-	Item rotation.
		price					-	Item price.
		textures				-	Object describing all the textures used by the item.
		baseProperties			-	Object defining the properties of the item's object type.
		properties				-	Object defining the properties of the instance.
		doc_cluster_element_id	-	Document cluster element id.
		copy_of					-	Id of the original object (this is a particular instance).
		descriptor				-	Path of the JSON descriptor file.
		state					-	Current state id.
		behaviour				-	Object defining state and available interactions.
		parts					-	Object parts.
*/
Items.Item = cls.Class.extend({
	/*
		Constructor: init
	*/
	init: function(id, object_id, flags, name, pos, scale, rotation, price, textures, baseProperties, properties, doc_cluster_element_id, copy_of, descriptor, state, behaviour, parts) {
		this.id = id;
		this.object_id = object_id;
		this.flags = flags;
		this.flags.active = true;
		this.flags.bot = false;
		this.name = name;
		this.pos = pos;
		this.scale = scale;
		this.rotation = rotation;
		this.price = price;
		this.textures = textures;
		this.baseProperties = baseProperties;
		this.properties = properties;
		this.doc_cluster_element_id = doc_cluster_element_id;
		this.copy_of = copy_of;
		this.descriptor = descriptor;
		this.state = state;
		this.behaviour = behaviour;
		this.parts = parts;
		this.contents = [];
	},
	
	/*
		Function: spawn
		Parameters:
			active	-	False if the client must create the object without actually activating it in the game (used for inventory items).
		
		Returns:
			A valid <NetworkMessages.Create> for this item.
	*/
	spawn: function(active) {
		if (active == undefined)
			active = true;
		this.flags.active = active;
		var kind = Constants.Entities.OBJECT;
		if (this.flags.door) {
			kind = Constants.Entities.DOOR;
		}
		return new NetworkMessages.Create(this.id, this.flags, this.name, kind, this.pos, this.scale, this.rotation, this.textures, this.behaviour, this.parts);
	}
});

/*
	Class: ItemFactory
	Manages the creation of an <Item>.
*/
Items.ItemFactory = cls.Class.extend({
	/*
		Constructor: init
		Parameters:
			db	-	The MySQL database object.
	*/
	init: function(db) {
		this.db = db;
	},
	
	/*
		Function: buildFromRow
		Creates an <Item> from a DB row, computes its behaviour and passes it to a callback function.
		
		Parameters:
			row			-	The DB row object.
			callback	-	The callback function.
	*/
	buildFromRow: function(row, callback) {
		if (typeof row.textures == "string")
			row.textures = {world: JSON.parse(row.textures).world, icon: JSON.parse(row.textures).icon};
		if (row.properties == null || row.properties == "")
			row.properties = "{}";
		if (typeof row.properties == "string")
			row.properties = JSON.parse(row.properties);
		if (row.baseProperties == null || row.baseProperties == "")
			row.baseProperties = "{}";
		if (typeof row.baseProperties == "string")
			row.baseProperties = JSON.parse(row.baseProperties);
		if (row.parts == null || row.parts == "")
			row.parts = "[]";
		if (typeof row.parts == "string")
			row.parts = JSON.parse(row.parts);
		if (row.posX == null) row.posX = 0;
		if (row.posY == null) row.posY = 0;
		if (row.posZ == null) row.posZ = 0;
		var pos = {x: row.posX, y: row.posY, z: row.posZ};
		var scale = {x: row.scaleX, y: row.scaleY, z: row.scaleZ};
		var item = new Items.Item(row.id, row.object_id,
							{pickable: row.pickable, container: row.container, outfit: row.outfit, document: row.document, hint: row.hint, token: row.token, door: row.door},
							row.name, pos, scale, row.rotation, row.price, row.textures, row.baseProperties, row.properties, row.doc_cluster_element_id, row.copy_of, row.descriptor, row.state, {}, row.parts);
		this.buildBehaviour(item, callback);
	},
	
	/*
		Function: buildBehaviour
		Calculates the behaviour (available interactions) of an <Item> and passes it to a callback function.
		
		Parameters:
			item		-	The <Item> object.
			callback	-	The callback function.
	*/
	buildBehaviour: function(item, callback) {
		var self = this;

		// Reminder: an interactive object must have a JSON descriptor file. In the simplest case, it will only define the default state.
		if (item.descriptor != null && item.descriptor.length > 0) {
			// Parse the descriptor
			fs.readFile(__dirname + "/objects/" + item.descriptor + ".json", function(err, file) {
				var json = JSON.parse(file.toString());
				// Reminder: item.state is the id of the item's current state (0, 1, or whatever)
				var state = json.states[item.state];
				var modules = json.modules;
				state.id = item.state;
				item.behaviour.state = state;
				item.behaviour.name = state.name;
				item.behaviour.methods = {};

				// Ok, what is happening here. If the item is a container, we have to check its volume and its contents to decide if it is empty, full, or whatever;
				// so we query the DB. If it is not a container, though, we don't need to. We could just use an IF, but the asynchronous query() would make this either
				// a total mess or a confusing series of function declarations, weeping and gnashing of teeth.
				// So this is a fake query with a fake parameters array that will return exactly 0 rows. If the item is a container, they are replaced.
				var contentQuery = "SELECT 0, 0 FROM DUAL WHERE 1=2";
				var params = [];
				if (item.flags.container) {
					contentQuery = "SELECT i.id, o.scaleX*o.scaleY*o.scaleZ volume FROM object_instance i INNER JOIN object o ON o.id = i.object_id WHERE container_id = ?";
					params = [item.id];
				}
				self.db.query(contentQuery, params, function(err, contentRows, fields) {
					if (err) {
						console.log("Error retrieving content from DB");
						throw err;
					} else {
						item.contents = [];
						var fillVolume = 0;
						// Compute the total occupied volume
						for (c in contentRows) {
							item.contents.push(contentRows[c].id);
							fillVolume += contentRows[c].volume;
						}
						// SPECIAL GAME METHODS
						if (item.flags.pickable) {
							item.behaviour.methods[Constants.Methods.PICK] = {name: "Pick", isSubject: true, isObject: false, auto: true, icon: "pick"};
						}
						// Reminder: outfits currently have two states: 1 if somebody's wearing it, 0 otherwise
						if (item.flags.outfit) {
							if (item.state == 0) {
								item.behaviour.methods[Constants.Methods.EQUIP] = {name: "Equip", isSubject: true, isObject: false, auto: true, icon: "equip"};
							} else {
								item.behaviour.methods[Constants.Methods.UNEQUIP] = {name: "Unequip", isSubject: true, isObject: false, auto: true, icon: "unequip"};
							}
						}
						if (item.flags.document) {
							item.behaviour.methods[Constants.Methods.SELL] = {name: "Buyers", isSubject: true, isObject: false, auto: true, icon: "sell"};
						}
						if ((item.flags.document && item.properties.text) || item.flags.hint) {
							item.behaviour.methods[Constants.Methods.EXAMINE] = {name: "Examine", isSubject: true, isObject: false, auto: true, icon: "examine"};
						}
						if (item.flags.token) {
							item.behaviour.methods[Constants.Methods.TOKEN] = {name: "Token", isSubject: true, isObject: false, auto: true, icon: "go"};
						}
						
						// Item instance properties
						var objProps = item.properties;
						// If it's not been parsed already, parse it
						if (typeof item.properties == "string") {
							if (item.properties.length > 0)
								objProps = JSON.parse(item.properties);
							else
								objProps = {};
						}
						// Item base object properties
						var baseProps = item.baseProperties;
						// If it's not been parsed already, parse it
						if (typeof item.baseProperties == "string") {
							if (item.baseProperties.length > 0)
								baseProps = JSON.parse(item.baseProperties);
							else
								baseProps = {};
						}
						// Merge base and instance properties
						var properties = _.defaults(objProps, baseProps);
						// If it's an open container with some space left, add insert method
						if (item.flags.container && properties.open && properties.capacity - fillVolume > 0.05) {
							item.behaviour.methods[Constants.Methods.INSERT] = {name: "Insert", isSubject: true, isObject: false, auto: false, icon: "insert"};
						}
						// If it's an open container with something inside, add loot method
						if (item.flags.container && properties.open && item.contents.length > 0) {
							item.behaviour.methods[Constants.Methods.LOOT] = {name: "Loot", isSubject: true, isObject: false, auto: true, icon: "loot"};
						}
						
						// Retrieve all possible interactions for this object type
						// Reminder: interactions have preconditions, to be matched with state and properties of the objects involved
						var moduleIds = (modules != undefined ? Object.keys(modules).join() : "-1");
						//self.db.query("SELECT id, name, auto, 'subject' as side, -1 as module, attempt_precondition, subject_id, object_id, icon " +
						self.db.query("SELECT id, name, auto, 'subject' as side, -1 as module, attempt_precondition, icon " +
										"FROM interaction " +
										"WHERE subject_id = ? OR subject_id IS NULL " +
										"UNION " +
										//"SELECT id, name, auto, 'object' as side, -1 as module, attempt_precondition, subject_id, object_id, icon " +
										"SELECT id, name, auto, 'object' as side, -1 as module, attempt_precondition, icon " +
										"FROM interaction " +
										"WHERE auto = 0 AND (object_id = ? OR object_id IS NULL) " +
										// Behaviour models
										"UNION " +
										//"SELECT bmi.id, bmi.name, bmi.auto, 'subject' as side, bm.id as module, bmi.attempt_precondition, ?, bmi.object_id, bmi.icon " +
										"SELECT bmi.id, bmi.name, bmi.auto, 'subject' as side, bm.id as module, bmi.attempt_precondition, bmi.icon " +
										"FROM behaviour_module bm INNER JOIN behaviour_module_interaction bmi ON bm.id = bmi.behaviour_module_id " +
										"WHERE bm.id IN (?) " +
										"UNION " +
										//"SELECT bmi.id, bmi.name, bmi.auto, 'object' as side, bm.id as module, bmi.attempt_precondition, QUALCOSA, QUALCOSA, bmi.icon " +
										"SELECT bmi.id, bmi.name, bmi.auto, 'object' as side, bm.id as module, bmi.attempt_precondition, bmi.icon " +
										"FROM behaviour_module bm INNER JOIN behaviour_module_interaction bmi ON bm.id = bmi.behaviour_module_id " +
										"WHERE bmi.object_id = ? ",
										[item.object_id, item.object_id, moduleIds, item.object_id], function(err, rows, fields) {
							if (err) {
								console.log("Error retrieving behaviour from DB");
								throw err;
							} else {
								for (var pre in rows) {
									// Check whether the item is subject or object of this interaction
									var isSubject = (rows[pre].side == "subject");
									var isObject = (rows[pre].side == "object");
									var auto = rows[pre].auto ? true : false;
									
									var attemptPrecondition = rows[pre].attempt_precondition;
									if (rows[pre].module > 0) {
										var params = modules[rows[pre].module];
										for (var p in params) {
											attemptPrecondition = attemptPrecondition.replace(new RegExp("\\$"+p+"\\b", "g"), params[p]);
										}
									}
									var checkPrecondition = new Function("item", attemptPrecondition);
									var applicable = checkPrecondition(item);
									
									if (rows[pre].object_id == null) {
										auto = true;
									}
									if (applicable) {
										// Add the method to the behaviour
										item.behaviour.methods[rows[pre].id] = {name: rows[pre].name, isSubject: isSubject, isObject: isObject, auto: auto, icon: rows[pre].icon};
									}
								}
								if (callback)
									callback(item);
							}
						});
						
					}
				});
			});
		} else {
			// No descriptor, empty behaviour
			if (callback)
				callback(item);
		}
	},
	
	/*
		Function: buildBehaviour
		Calculates the behaviour (available interactions) of an <Item> and passes it to a callback function.
		
		Parameters:
			item		-	The <Item> object.
			callback	-	The callback function.
	*/
	buildBehaviourNew: function(item, callback) {
		var self = this;

		// Reminder: an interactive object must have a JSON descriptor file. In the simplest case, it will only define the default state.
		//if (item.descriptor != null && item.descriptor.length > 0) {
			/*// Parse the descriptor
			fs.readFile(__dirname + "/objects/" + item.descriptor + ".json", function(err, file) {
				var json = JSON.parse(file.toString());
				// Reminder: item.state is the id of the item's current state (0, 1, or whatever)
				var state = json.states[item.state];
				var modules = json.modules;
				state.id = item.state;
				item.behaviour.state = state;
				item.behaviour.name = state.name;
				item.behaviour.methods = {};*/
			
			self.db.query("SELECT os.id, os.object_id, os.value, os.name " +
							"FROM object_state os " +
							"WHERE object_id = ?", [item.object_id], function(err, stateRows, fields) {
				var state;
				for (s in stateRows) {
					if (stateRows[s].value == item.state) {
						state = {id: stateRows[s].value, name: stateRows[s].name};
					}
				}
				item.behaviour.state = state;
				item.behaviour.name = state.name;
				
				// Ok, what is happening here. If the item is a container, we have to check its volume and its contents to decide if it is empty, full, or whatever;
				// so we query the DB. If it is not a container, though, we don't need to. We could just use an IF, but the asynchronous query() would make this either
				// a total mess or a confusing series of function declarations, weeping and gnashing of teeth.
				// So this is a fake query with a fake parameters array that will return exactly 0 rows. If the item is a container, they are replaced.
				var contentQuery = "SELECT 0, 0 FROM DUAL WHERE 1=2";
				var params = [];
				if (item.flags.container) {
					contentQuery = "SELECT i.id, o.scaleX*o.scaleY*o.scaleZ volume FROM object_instance i INNER JOIN object o ON o.id = i.object_id WHERE container_id = ?";
					params = [item.id];
				}
				self.db.query(contentQuery, params, function(err, contentRows, fields) {
					if (err) {
						console.log("Error retrieving content from DB");
						throw err;
					} else {
						item.contents = [];
						var fillVolume = 0;
						// Compute the total occupied volume
						for (c in contentRows) {
							item.contents.push(contentRows[c].id);
							fillVolume += contentRows[c].volume;
						}
						// SPECIAL GAME METHODS
						if (item.flags.pickable) {
							item.behaviour.methods[Constants.Methods.PICK] = {name: "Pick", isSubject: true, isObject: false, auto: true, icon: "pick"};
						}
						// Reminder: outfits currently have two states: 1 if somebody's wearing it, 0 otherwise
						if (item.flags.outfit) {
							if (item.state == 0) {
								item.behaviour.methods[Constants.Methods.EQUIP] = {name: "Equip", isSubject: true, isObject: false, auto: true, icon: "equip"};
							} else {
								item.behaviour.methods[Constants.Methods.UNEQUIP] = {name: "Unequip", isSubject: true, isObject: false, auto: true, icon: "unequip"};
							}
						}
						if (item.flags.document) {
							item.behaviour.methods[Constants.Methods.SELL] = {name: "Buyers", isSubject: true, isObject: false, auto: true, icon: "sell"};
						}
						if ((item.flags.document && item.properties.text) || item.flags.hint) {
							item.behaviour.methods[Constants.Methods.EXAMINE] = {name: "Examine", isSubject: true, isObject: false, auto: true, icon: "examine"};
						}
						if (item.flags.token) {
							item.behaviour.methods[Constants.Methods.TOKEN] = {name: "Token", isSubject: true, isObject: false, auto: true, icon: "go"};
						}
						
						// Item instance properties
						var objProps = item.properties;
						// If it's not been parsed already, parse it
						if (typeof item.properties == "string") {
							if (item.properties.length > 0)
								objProps = JSON.parse(item.properties);
							else
								objProps = {};
						}
						// Item base object properties
						var baseProps = item.baseProperties;
						// If it's not been parsed already, parse it
						if (typeof item.baseProperties == "string") {
							if (item.baseProperties.length > 0)
								baseProps = JSON.parse(item.baseProperties);
							else
								baseProps = {};
						}
						// Merge base and instance properties
						var properties = _.defaults(objProps, baseProps);
						// If it's an open container with some space left, add insert method
						if (item.flags.container && properties.open && properties.capacity - fillVolume > 0.05) {
							item.behaviour.methods[Constants.Methods.INSERT] = {name: "Insert", isSubject: true, isObject: false, auto: false, icon: "insert"};
						}
						// If it's an open container with something inside, add loot method
						if (item.flags.container && properties.open && item.contents.length > 0) {
							item.behaviour.methods[Constants.Methods.LOOT] = {name: "Loot", isSubject: true, isObject: false, auto: true, icon: "loot"};
						}
						
						
						
						self.db.query("SELECT module.id module_id, module.subject_id, module.object_id, module.bhv_module_id, module.parameters, " +
										"bmi.id, bmi.name, bmi.auto, bmi.attempt_precondition, bmi.success_precondition, bmi.postcondition, bmi.icon " +
										"FROM " +
											"(SELECT obm.id, obm.subject_id, obm.object_id, obm.bhv_module_id, " +
													"GROUP_CONCAT(os.value ORDER BY obmp.position ASC) parameters " +
											"FROM obj_bhv_module obm " +
											"INNER JOIN obj_bhv_mod_parameters obmp ON obmp.obj_bhv_module_id = obm.id " +
											"INNER JOIN object_state os ON obmp.object_state_id = os.id " +
											"WHERE obm.subject_id = ? OR obm.object_id = ? " +
											"GROUP BY obm.id) module " +
									"INNER JOIN bhv_mod_interaction bmi ON modules.bhv_module_id = bmi.bhv_module_id " +
									"ORDER BY modules.id ASC" +
									
									"UNION " +
									//"SELECT id, name, auto, 'subject' as side, -1 as module, attempt_precondition, icon " +
									"SELECT -1 as module_id, subject_id, object_id, -1 as bhv_module_id, '' as parameters, " +
										"" +
									"FROM interaction " +
									"WHERE subject_id = ? OR subject_id IS NULL " +
									"UNION " +
									//"SELECT id, name, auto, 'object' as side, -1 as module, attempt_precondition, subject_id, object_id, icon " +
									"SELECT id, name, auto, 'object' as side, -1 as module, attempt_precondition, icon " +
									"FROM interaction " +
									"WHERE auto = 0 AND (object_id = ? OR object_id IS NULL) "
									
									
									
									
									, [item.object_id, item.object_id], function(err, rows, fields) {
							
							
							
						});
						
						
						
						
						// Retrieve all possible interactions for this object type
						// Reminder: interactions have preconditions, to be matched with state and properties of the objects involved
						var moduleIds = (modules != undefined ? Object.keys(modules).join() : "-1");
						//self.db.query("SELECT id, name, auto, 'subject' as side, -1 as module, attempt_precondition, subject_id, object_id, icon " +
						self.db.query("SELECT id, name, auto, 'subject' as side, -1 as module, attempt_precondition, icon " +
										"FROM interaction " +
										"WHERE subject_id = ? OR subject_id IS NULL " +
										"UNION " +
										//"SELECT id, name, auto, 'object' as side, -1 as module, attempt_precondition, subject_id, object_id, icon " +
										"SELECT id, name, auto, 'object' as side, -1 as module, attempt_precondition, icon " +
										"FROM interaction " +
										"WHERE auto = 0 AND (object_id = ? OR object_id IS NULL) " +
										// Behaviour models
										"UNION " +
										//"SELECT bmi.id, bmi.name, bmi.auto, 'subject' as side, bm.id as module, bmi.attempt_precondition, ?, bmi.object_id, bmi.icon " +
										"SELECT bmi.id, bmi.name, bmi.auto, 'subject' as side, bm.id as module, bmi.attempt_precondition, bmi.icon " +
										"FROM behaviour_module bm INNER JOIN behaviour_module_interaction bmi ON bm.id = bmi.behaviour_module_id " +
										"WHERE bm.id IN (?) " +
										"UNION " +
										//"SELECT bmi.id, bmi.name, bmi.auto, 'object' as side, bm.id as module, bmi.attempt_precondition, QUALCOSA, QUALCOSA, bmi.icon " +
										"SELECT bmi.id, bmi.name, bmi.auto, 'object' as side, bm.id as module, bmi.attempt_precondition, bmi.icon " +
										"FROM behaviour_module bm INNER JOIN behaviour_module_interaction bmi ON bm.id = bmi.behaviour_module_id " +
										"WHERE bmi.object_id = ? ",
										[item.object_id, item.object_id, moduleIds, item.object_id], function(err, rows, fields) {
							if (err) {
								console.log("Error retrieving behaviour from DB");
								throw err;
							} else {
								for (var pre in rows) {
									// Check whether the item is subject or object of this interaction
									var isSubject = (rows[pre].side == "subject");
									var isObject = (rows[pre].side == "object");
									var auto = rows[pre].auto ? true : false;
									
									var attemptPrecondition = rows[pre].attempt_precondition;
									if (rows[pre].module > 0) {
										var params = modules[rows[pre].module];
										for (var p in params) {
											attemptPrecondition = attemptPrecondition.replace(new RegExp("\\$"+p+"\\b", "g"), params[p]);
										}
									}
									var checkPrecondition = new Function("item", attemptPrecondition);
									var applicable = checkPrecondition(item);
									
									if (rows[pre].object_id == null) {
										auto = true;
									}
									if (applicable) {
										// Add the method to the behaviour
										item.behaviour.methods[rows[pre].id] = {name: rows[pre].name, isSubject: isSubject, isObject: isObject, auto: auto, icon: rows[pre].icon};
									}
								}
								if (callback)
									callback(item);
							}
						});
						
					}
				});
			});
		/*} else {
			// No descriptor, empty behaviour
			if (callback)
				callback(item);
		}*/
	},
	
	/*
		Function: buildFromId
		Retrieves item data from the DB and calls buildFromRow().
		
		Parameters:
			id			-	The item's id.
			callback	-	The callback function.
	*/
	buildFromId: function(id, callback) {
		var self = this;
		
		self.db.query("SELECT i.id, i.posX, i.posY, i.posZ, i.rotation, o.id object_id, o.pickable, o.container, o.outfit, o.document, o.hint, o.token, o.door, " +
									"o.name, o.scaleX, o.scaleY, o.scaleZ, o.price, o.textures, o.descriptor, o.properties baseProperties, o.parts, i.state, i.properties, " +
									"i.doc_cluster_element_id, i.copy_of " +
						"FROM object o INNER JOIN object_instance i ON o.id = i.object_id " +
						"WHERE i.id = ?", [id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving item data");
				throw err;
			} else {
				self.buildFromRow(rows[0], callback);
			}
		});
	}
});