/*
	Script: map
*/

var cls = require('./lib/class'),
	fs = require('fs'),
	_ = require('underscore'),
	Constants = require("./constants");

/*
	Class: Map
	Manages city map information.
*/
module.exports = Map = cls.Class.extend({
	/*
		Constructor: init
		Reads the map JSON descriptor and calls <initMap>.
		
		Parameters:
			filepath	-	Path of the JSON map file.
	*/
	init: function(filepath) {
		var self = this;
		
		this.isLoaded = false;
		
		fs.exists(filepath, function(exists) {
			if (!exists) {
				console.log(filepath + " doesn't exist.");
				return;
			}
			
			fs.readFile(filepath, function(err, file) {
				var json = JSON.parse(file.toString());
				self.initMap(json);
			});
		});
	},
	
	/*
		Function: initMap
		Sets up map's dimensions, objects and groups size.
		
		Parameters:
			map	-	Parsed map file.
	*/
	initMap: function(map) {
		this.width = map.width;
		this.length = map.length;
		
		this.objects = map.objects;
		this.lamps = map.lamps;
		this.walls = map.walls;
		
		this.ending = map.ending;
		
		this.isLoaded = true;
		
		// zone groups
		this.zoneWidth = Constants.CITY_ZONE_WIDTH;
		this.zoneLength = Constants.CITY_ZONE_LENGTH;
		this.groupWidth = Math.floor(this.width / this.zoneWidth);
		this.groupLength = Math.floor(this.length / this.zoneLength);
		
		this.initConnectedGroups(map.doors);
		
		if (this.ready_func) {
			this.ready_func();
		}
	},

	ready: function(f) {
		this.ready_func = f;
	},

	/*
		Function: isOutOfBounds
		Parameters:
			x	-	X coordinate.
			z	-	Z coordinate.
			
		Returns:
			True if the input coordinates are inside the map, false otherwise.
	*/
	isOutOfBounds: function(x, z) {
		return x <= 0 || x >= this.length || z <= 0 || z >= this.width;
	},

	/*
		Function: isColliding
		Parameters:
			x	-	X coordinate.
			z	-	Z coordinate.
			
		Returns:
			True if the input coordinates are occupied by an object, false otherwise.
	*/
	isColliding: function(x, z) {
		if (this.isOutOfBounds(x, z)) {
			return false;
		}
		
		for (var i=0; i<this.objects.length; i++) {
			var obj = this.objects[i];
			if (x > obj.pos.x-obj.scale.x/2 && x < obj.pos.x + obj.scale.x/2
					&& z > obj.pos.z-obj.scale.z/2 && z < obj.pos.z + obj.scale.z/2) {
				return true;
			}
		}
		
		return false;
	},
	
	/*
		Function: groupIdToGroupPosition
		Parameters:
			id	-	Group id.
			
		Returns:
			The group position as a pair of integers (instead of "x-z").
	*/
	groupIdToGroupPosition: function(id) {
		var posArray = id.split('-');
		
		return pos(parseInt(posArray[0]), parseInt(posArray[1]));
	},
	
	/*
		Function: forEachGroup
		Parameters:
			callback	-	The function to execute on each group.
	*/
	forEachGroup: function(callback) {
		var width = this.groupWidth,
		length = this.groupLength;
		
		for (var x = 0; x < length; x += 1) {
			for (var z = 0; z < width; z += 1) {
				callback(x+'-'+z);
			}
		}
	},
	
	/*
		Function: getGroupIdFromPosition
		Parameters:
			x	-	X coordinate.
			z	-	Z coordinate.
			
		Returns:
			The group id (in "x-z" form) to which the position belongs.
	*/
	getGroupIdFromPosition: function(x, z) {
		var l = this.zoneLength,
		w = this.zoneWidth,
		gx = Math.floor(x / l),
		gz = Math.floor(z / w);

		return gx+'-'+gz;
	},
	
	/*
		Function: getAdjacentGroupPositions
		Parameters:
			id	-	Group id (x-z).
			
		Returns:
			The ids (x-z) of all adjacent groups, including the input one (e.g. 3-5 will give 2-4, 3-5, 4-4, 2-5, 3-5, 4-5, 2-6, 3-6, 4-6).
	*/
	getAdjacentGroupPositions: function(id) {
		var self = this,
		position = this.groupIdToGroupPosition(id),
		x = position.x,
		z = position.z,
		// surrounding groups
		list = [pos(x-1, z-1), pos(x, z-1), pos(x+1, z-1),
		pos(x-1, z),   pos(x, z),   pos(x+1, z),
		pos(x-1, z+1), pos(x, z+1), pos(x+1, z+1)];
		
		// groups connected via doors
		_.each(this.connectedGroups[id], function(position) {
			// don't add a connected group if it's already part of the surrounding ones.
			if (!_.any(list, function(groupPos) { return equalPositions(groupPos, position); })) {
				list.push(position);
			}
		});
		
		return _.reject(list, function(pos) { 
			return pos.x < 0 || pos.z < 0 || pos.x >= self.groupLength || pos.z >= self.groupWidth;
		});
	},
	
	/*
		Function: forEachAdjacentGroup
		Parameters:
			groupId		-	The group id.
			callback	-	The function to execute on each adjacent group.
	*/
	forEachAdjacentGroup: function(groupId, callback) {
		if (groupId) {
			_.each(this.getAdjacentGroupPositions(groupId), function(pos) {
				callback(pos.x+'-'+pos.z);
			});
		}
	},
	
	/*
		Function: initConnectedGroups
		Uses the map doors to define connected groups.
		
		Parameters:
			doors	-	Array of doors.
	*/
	initConnectedGroups: function(doors) {
		var self = this;

		this.connectedGroups = {};
		_.each(doors, function(door) {
			var groupId = self.getGroupIdFromPosition(door.x, door.z),
			connectedGroupId = self.getGroupIdFromPosition(door.tx, door.tz),
			connectedPosition = self.groupIdToGroupPosition(connectedGroupId);
			
			if (groupId in self.connectedGroups) {
				self.connectedGroups[groupId].push(connectedPosition);
			} else {
				self.connectedGroups[groupId] = [connectedPosition];
			}
		});
	},
	
	/*
		Function: getRandomStartingPosition		
		Returns:
			A pair of random valid coordinates in the map.
	*/
	getRandomStartingPosition: function() {
		var pos = {};
		var valid = false;
		while (!valid) {
			pos.x = Math.floor(Math.random() * (this.length + 1));
			pos.z = Math.floor(Math.random() * (this.width + 1));
			valid = !this.isColliding(pos.x, pos.z);
		}
		return pos;
	}
});

var pos = function(x, z) {
	return { x: x, z: z };
};

var equalPositions = function(pos1, pos2) {
	return pos1.x === pos2.x && pos2.z === pos2.z;
};
