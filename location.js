/*
	Script: location
*/

var Map = require("./map"),
	_ = require("underscore"),
	NetworkMessages = require("./message"),
	Items = require("./item"), Item = Items.Item, ItemFactory = Items.ItemFactory,
	Bot = require("./bot"),
	Sweeper = require("./sweeper"),
	fs = require('fs');

/*
	Class: Location
	Holds location information
	
	Variables:
		name			-	Location name.
		id				-	Location id.
		maxPlayers		-	Maximum number of players.
		store_query		-	WHERE condition to identify products that are on sale here.
		copy_of			-	Id of the original location, if this is a specific instance.
		server			-	Server object.
		ups				-	Updates per second.
		info			-	Object containing location information.
		playerCount		-	Number of players connected to the location.
		entities		-	Array of entities (characters) connected to the location (associative by id).
		players			-	Array of players connected to the location.
		items			-	Array of items in the location (associative by id).
		transactions	-	Array of sale negotiations.
		portkeys		-	Array of portkeys in the location (associative by id).
		groups			-	Object describing the location groups.
		outgoingQueues	-	Array of network message queues for each connected player (associative by player id).
		connections		-	Array of sockets for each connected player (associative by player id).
		itemFactory		-	<ItemFactory> object, used to create an <Item>.
		processInterval	-	The interval for message queues processing.
		infoInterval	-	The interval for location info broadcasting.
*/

/*
	Constructor: Location
	Parameters:
		id			-	Location id.
		name		-	Location name.
		maxPlayers	-	Maximum number of something somewhere, can't remember.
		store_query	-	WHERE condition to identify products that are on sale here.
		copy_of		-	Id of the original location, if this is a specific instance.
		server		-	The server object.
*/
function Location(id, name, maxPlayers, store_query, copy_of, server) {
	var self = this;
	
	this.name = name;
	this.id = id;

	this.maxPlayers = maxPlayers;
	this.store_query = store_query;
	this.copy_of = copy_of;
	
	this.server = server;
	
	// Updates per second
	this.ups = 50;
	
	this.info = {};
	
	this.playerCount = 0;
	
	this.entities = {};
	this.players = {};
	this.items = {};
	this.transactions = {};
	this.portkeys = {};
	
	this.groups = {};
	this.outgoingQueues = {};
	
	this.connections = {};
	
	this.itemFactory = new ItemFactory(this.server.db);
	
	this.processInterval = null;
	this.infoInterval = null;
	
	this.map = null;	
}

Location.prototype = {
	
	/*
		Function: run
		Starts the location
		
		Parameters:
			mapFilePath	-	The path of the JSON map file.
			callback	-	A callback function.
	*/
	run: function(mapFilePath, callback) {
        var self = this;
		
		// Gather map information (see map.js)
        this.map = new Map(mapFilePath);

        this.map.ready(function() {
			// Reminder: the location is divided in smaller zones, to avoid telling everything to everybody in the whole city
            self.initZoneGroups();
            
			// Retrieve all the portkeys in the location
			self.server.db.query("SELECT p.id, p.location_id, p.posX, p.posY, p.posZ, p.sizeX, p.sizeY, p.sizeZ, " +
										"p.dest_location_id, p.dest_posX, p.dest_posY, p.dest_posZ, p.textures "+
								"FROM portkey p " +
								"WHERE p.location_id = ?", [self.id], function(err, rows, fields) {
				if (err) {
					console.log("Error retrieving portkeys");
					throw err;
				} else {
					for (var p in rows) {
						var portkey = rows[p];
						var textures;
						if (p.textures != "")
							textures = JSON.parse(portkey.textures);
						else
							textures = {};
						self.portkeys[portkey.id] = {id: portkey.id, pos: {x: portkey.posX, y: portkey.posY, z: portkey.posZ}, scale: {x: portkey.sizeX, y: portkey.sizeY, z: portkey.sizeZ}, destination: {location: portkey.dest_location_id, pos: {x: portkey.dest_posX, y: portkey.dest_posY, z: portkey.dest_posZ}}, textures: textures};
					}
					console.log("Added " + rows.length + " portkeys to location " + self.name);
				}
			});
			
			// Retrieve all the bots in the location
			self.server.db.query("SELECT s.id, s.name, s.level, s.posX, s.posZ " +
								"FROM spy s " +
								"WHERE s.location_id = ? AND s.bot = 1", [self.id], function(err, rows, fields) {
				if (err) {
					console.log("Error retrieving bots");
					throw err;
				} else {
					for (var i in rows) {
						var bot = new Bot(rows[i].id, rows[i].name, rows[i].level, {x: rows[i].posX, z: rows[i].posZ}, self);
						self.addEntity(bot);
					}
					console.log("Added " + rows.length + " bots to location " + self.name);
				}
			});
			
			// Retrieve all the sweepers in the location
			self.server.db.query("SELECT s.id, s.name, s.level, s.posX, s.posZ " +
								"FROM spy s " +
								"WHERE s.location_id = ? AND s.sweeper = 1", [self.id], function(err, rows, fields) {
				if (err) {
					console.log("Error retrieving sweepers");
					throw err;
				} else {
					for (var i in rows) {
						var sweeper = new Sweeper(rows[i].id, rows[i].name, rows[i].level, {x: rows[i].posX, z: rows[i].posZ}, self);
						self.addEntity(sweeper);
					}
					console.log("Added " + rows.length + " sweepers to location " + self.name);
				}
			});
			
			// Retrieve all the items that are laying around in the location
			self.server.db.query("SELECT i.id, i.posX, i.posY, i.posZ, i.rotation, o.id object_id, o.pickable, o.container, o.outfit, o.document, o.hint, o.token, o.door, " +
									"o.name, o.scaleX, o.scaleY, o.scaleZ, o.price, o.textures, o.descriptor, o.properties baseProperties, o.parts, " +
									"i.state, i.properties, i.doc_cluster_element_id, i.copy_of " +
								"FROM object o INNER JOIN object_instance i ON o.id = i.object_id " +
								"WHERE i.location_id = ? AND i.container_id IS NULL AND i.spy_id IS NULL", [self.id], function(err, rows, fields) {
				if (err) {
					console.log("Error retrieving items");
					throw err;
				} else {
					var doneItems = [];
					var onComplete = function() {
						console.log("Added " + rows.length + " items to location " + self.name);
						if (callback)
							callback();
					};
					if (rows.length == 0) {
						onComplete();
					} else {
						for (var i in rows) {
							self.itemFactory.buildFromRow(rows[i], function(item) {
								self.items[item.id] = item;
								doneItems.push(item.id);
								if (doneItems.length == rows.length) {
									onComplete();
								}
							});
						}
					}
				}
			});
			
        });
        
		// Process groups and queues every 20 ms
        self.processInterval = setInterval(function() {
            self.processGroups();
            self.processQueues();
        }, 1000 / this.ups);
		
		// Retrieve location information (see below), then send it to the players)
		self.getLocationInfo(function(info) {
			self.info = info;
			// Temporary random weather (waiting for cron job)
			info.weather = Math.floor(Math.random() * 2);
			self.pushBroadcast(new NetworkMessages.LocationInfo(self.name, {lat: info.lat, lng: info.lng}, info.weather, info.indoor, self.playerCount));
		});
		// Do it again every ten minutes
		self.infoInterval = setInterval(function() {
			self.getLocationInfo(function(info) {
				self.info = info;
				self.pushBroadcast(new NetworkMessages.LocationInfo(self.name, {lat: info.lat, lng: info.lng}, info.weather, info.indoor, self.playerCount));
			});
		}, 10*60*1000);
        
        console.log("Location " + this.name + " created (capacity: " + this.maxPlayers + " players).");
    },
	
	/*
		Function: stop
		Stops the location
	*/
	stop: function() {
		clearInterval(this.processInterval);
		clearInterval(this.infoInterval);
		console.log("Location " + this.name + " stopped");
	},
	
	/*
		Function: markForDeletion
		Marks a completed location for deletion. The location will be removed when empty.
	*/
	markForDeletion: function() {
		this.markedForDeletion = true;
	},
	
	/*
		Function: isValidPosition
		Checks if a position is valid and collision-free. Almost useless, since the player can only move to valid positions in the client. Just for safety, though.
		
		Parameters:
			x	-	X coordinate.
			z	-	Zorro.
	*/
	isValidPosition: function(x, z) {
        if (this.map && _.isNumber(x) && _.isNumber(z) && !this.map.isOutOfBounds(x, z) && !this.map.isColliding(x, z)) {
            return true;
        }
        return false;
    },
	
	/*
		Function: onPlayerEnter
		Called by the player object after login and basic stuff.
		
		Parameters:
			player	-	The player object.
	*/
	onPlayerEnter: function(player) {
		var self = this;
		
		console.log(player.name + " has joined " + self.name);
		// Send location description to the player (see below)
		self.sendTo(player);
		// Send a list of near characters to the player (see below)
		self.pushRelevantEntityListTo(player);
	},
	
	/*
		Function: setPlayerCount
		Sets the current number of players in the location.
		
		Parameters:
			count	-	The desired number of players.
	*/
	setPlayerCount: function(count) {
        this.playerCount = count;
    },
    
	/*
		Function: incrementPlayerCount
		Increments the player count.
	*/
    incrementPlayerCount: function() {
        this.setPlayerCount(this.playerCount + 1);
    },
    
	/*
		Function: decrementPlayerCount
		Decrements the player count.
	*/
    decrementPlayerCount: function() {
        if (this.playerCount > 0) {
            this.setPlayerCount(this.playerCount - 1);
        }
    },
	
	/*
		Function: pushRelevantEntityListTo
		Sends a list containing the ids of all characters in the same area.
		
		Parameters:
			player	-	The player object.
	*/
	pushRelevantEntityListTo: function(player) {
        var entities;
        if (player && (player.group in this.groups)) {
			// Get the keys (ids as string) from the array describing all the characters in the player's group
            entities = _.keys(this.groups[player.group].entities);
			// Exclude the player themselves
            entities = _.reject(entities, function(id) { return id == player.id; });
			// Convert to integers
            entities = _.map(entities, function(id) { return parseInt(id); });
            if (entities) {
				// Send message
                this.pushToPlayer(player, new NetworkMessages.List(entities));
            }
        }
    },
	
	/*
		Function: pushToPlayer
		Enqueues a message for a specific player.
		
		Parameters:
			player	-	I think it's clear by now.
			message	-	The network message.
	*/
    pushToPlayer: function(player, message) {
        if (player && player.id in this.outgoingQueues) {
			// If message is still an object, encode it
			if (message.encode) {
				message = message.encode();
			}
			// Enqueue
            this.outgoingQueues[player.id].push(message);
        } else {
			if (!player)
				console.log("!player");
			if (!(player.id in this.outgoingQueues))
				console.log("!(player.id in this.outgoingQueues)");
            console.log("pushToPlayer: player was undefined");
        }
    },
    
	/*
		Function: pushToGroup
		Enqueues a message for all players in a group.
		
		Parameters:
			groupId			-	The group id (x-z).
			message			-	The network message.
			ignoredPlayer	-	Don't send the message to this id.
	*/
    pushToGroup: function(groupId, message, ignoredPlayer) {
        var self = this,
            group = this.groups[groupId];
        
        if (group) {
            _.each(group.players, function(playerId) {
                if (playerId != ignoredPlayer) {
                    self.pushToPlayer(self.getEntityById(playerId), message);
                }
            });
        } else {
            log.error("groupId: "+groupId+" is not a valid group");
        }
    },
    
	/*
		Function: pushToAdjacentGroups
		Enqueues a message for all players in a group or in adjacent groups.
		
		Parameters:
			groupId			-	The group id (x-z).
			message			-	The network message.
			ignoredPlayer	-	I hate this guy.
	*/
    pushToAdjacentGroups: function(groupId, message, ignoredPlayer) {
        var self = this;
        self.map.forEachAdjacentGroup(groupId, function(id) {
            self.pushToGroup(id, message, ignoredPlayer);
        });
    },
    
	/*
		Function: pushToPreviousGroups
		Enqueues a message for all players in groups recently left (a despawn, usually).
		
		Parameters:
			player	-	Which part of "player" is not clear to you.
			message	-	Will this ever end.
	*/
    pushToPreviousGroups: function(player, message) {
        var self = this;
        // Push this message to all groups which are not going to be updated anymore,
        // since the player left them.
        _.each(player.recentlyLeftGroups, function(id) {
            self.pushToGroup(id, message);
        });
        player.recentlyLeftGroups = [];
    },
    
	/*
		Function: pushBroadcast
		Enqueues a message for everybody in the location.
		
		Parameters:
			message			-	The network message.
			ignoredPlayer	-	Guys? Where did you all go?
	*/
    pushBroadcast: function(message, ignoredPlayer) {
		if (message.encode) {
			message = message.encode();
		}
        for (var id in this.outgoingQueues) {
            if (id != ignoredPlayer) {
                this.outgoingQueues[id].push(message);
            }
        }
    },
    
	/*
		Function: processQueues
		Sends the enqueued messages.
	*/
    processQueues: function() {
        var self = this,
            connection,
			message;

        for (var id in this.outgoingQueues) {
            if (this.outgoingQueues[id].length > 0) {
				// Get the socket for this player id (see below)
                connection = this.getConnection(id);
				// Write each message on the socket;
				messages = this.outgoingQueues[id];
				for (var m in messages) {
					connection.write(messages[m]);
				}
				this.outgoingQueues[id] = [];
            }
        }
    },
	
	/*
		Function: addConnection
		Adds a socket to the connections array, using the player id as key.
		
		Parameters:
			id		-	Player id.
			socket	-	The socket associated to the player.
	*/
	addConnection: function(id, socket) {
		this.connections[id] = socket;
	},
	
	/*
		Function: getConnection
		Gets the socket from the player id.
		
		Parameters:
			id	-	The player id.
		
		Returns:
			The socket associated to the player.
	*/
	getConnection: function(id) {
		return this.connections[id];
	},
	
	/*
		Function: addItem
		Adds an item from DB.
		
		Parameters:
			item		-	The item to be added (id or full object).
			callback	-	A callback function.
	*/
	addItem: function(item, callback) {
		if (typeof item == "object") {
			this.items[item.id] = item;
			if (callback)
				callback(this.items[item.id]);
		} else {
			self.itemFactory.buildFromId(item, function(item) {
				self.items[item.id] = item;
				if (callback)
					callback(this.items[item.id]);
			});
		}
	},
	
	/*
		Function: addEntity
		Adds a generic character to the location.
		
		Parameters:
			entity	-	The character to be added.
	*/
	addEntity: function(entity) {
		this.entities[entity.id] = entity;
		this.handleEntityGroupMembership(entity);
	},
    
	/*
		Function: removeEntity
		Removes a generic character from the location.
		
		Parameters:
			entity	-	The character to be removed.
	*/
	removeEntity: function(entity) {
		if (entity.id in this.entities) {
			delete this.entities[entity.id];
		}
		entity.destroy();
		this.removeFromGroups(entity);
		console.log("Removed entity " + entity.id);
	},
    
	/*
		Function: addPlayer
		Adds a player to the location.
		
		Parameters:
			player	-	The. Player. Object.
	*/
	addPlayer: function(player) {
		this.incrementPlayerCount();
		this.addEntity(player);
		this.players[player.id] = player;
		this.outgoingQueues[player.id] = [];
		this.addConnection(player.id, player.socket);
		console.log("Added player : " + player.name + " (" + player.id + ")");
	},
    
	/*
		Function: removePlayer
		Removes a player from the location.
		
		Parameters:
			player	-	I can't do this anymore.
	*/
    removePlayer: function(player) {
        player.broadcast(player.despawn());
        this.removeEntity(player);
        delete this.players[player.id];
        delete this.outgoingQueues[player.id];
		for (var id in this.transactions) {
			var parties = id.split("-");
			if (player.id == parties[0] || player.id == parties[1])
				delete this.transactions[id];
		}
		this.decrementPlayerCount();
		
		if (this.markedForDeletion && this.playerCount == 0) {
			this.server.removeLocation(this);
		}
    },
	
	/*
		Function: getEntityById
		Gets an entity from its id.
	
		Parameters:
			id	-	The entity id.
	
		Returns:
			The entity having the requested id.
	*/
	getEntityById: function(id) {
		if (id in this.entities) {
			return this.entities[id];
		} else {
			console.log("Unknown entity : " + id);
		}
	},
	
	/*
		Function: initZoneGroups
		Initialise the location <groups> object.
	*/
	initZoneGroups: function() {
		var self = this;
		
		this.map.forEachGroup(function(id) {
			self.groups[id] = { entities: {},
								players: [],
								incoming: []};
		});
		this.zoneGroupsReady = true;
	},
	
	/*
		Function: removeFromGroups
		Removes an entity from involved groups.
		
		Parameters:
			entity	-	The entity to be removed.
		
		Returns:
			An array of groups from which the entity was removed.
	*/
	removeFromGroups: function(entity) {
		var self = this,
		oldGroups = [];
		
		if (entity && entity.group) {
			
			var group = this.groups[entity.group];
			if (entity instanceof Player) {
				group.players = _.reject(group.players, function(id) { return id === entity.id; });
			}
			
			this.map.forEachAdjacentGroup(entity.group, function(id) {
				if (entity.id in self.groups[id].entities) {
					delete self.groups[id].entities[entity.id];
					oldGroups.push(id);
				}
			});
			entity.group = null;
		}
		return oldGroups;
	},
	
	/*
		Function: addAsIncomingToGroup
		Registers an entity as "incoming" into several groups, meaning that it just entered them.
		All players inside these groups will receive a Spawn message when Location.processGroups is called.
		
		Parameters:
			entity	-	The entity to be added.
			groupId	-	The group to which the entity will be added.
	*/
	addAsIncomingToGroup: function(entity, groupId) {
		var self = this;
		
		if (entity && groupId) {
			this.map.forEachAdjacentGroup(groupId, function(id) {
				var group = self.groups[id];
				
				if (group) {
					if(!_.include(group.entities, entity.id)) {
						group.incoming.push(entity);
					}
				}
			});
		}
	},
	
	/*
		Function: addToGroup
		Adds an entity to a group.
		
		Parameters:
			entity	-	The entity to be added.
			groupId	-	The group to which the entity will be added.
		
		Returns:
			An array of groups in which the entity was added.
	*/
	addToGroup: function(entity, groupId) {
		var self = this,
		newGroups = [];
		
		if (entity && groupId && (groupId in this.groups)) {
			this.map.forEachAdjacentGroup(groupId, function(id) {
				self.groups[id].entities[entity.id] = entity;
				newGroups.push(id);
			});
			entity.group = groupId;
			
			if (entity instanceof Player) {
				this.groups[groupId].players.push(entity.id);
			}
		}
		return newGroups;
	},
	
	/*
		Function: handleEntityGroupMembership
		Checks if an entity has changed group, and treats it accordingly.
		
		Parameters:
			entity	-	The entity to be checked.
		
		Returns:
			True if the entity has changed groups, false otherwise.
	*/
	handleEntityGroupMembership: function(entity) {
		var hasChangedGroups = false;
		if (entity) {
			var groupId = this.map.getGroupIdFromPosition(entity.x, entity.z);
			if (!entity.group || (entity.group && entity.group !== groupId)) {
				hasChangedGroups = true;
				this.addAsIncomingToGroup(entity, groupId);
				var oldGroups = this.removeFromGroups(entity);
				var newGroups = this.addToGroup(entity, groupId);
				
				if (_.size(oldGroups) > 0) {
					entity.recentlyLeftGroups = _.difference(oldGroups, newGroups);
					//console.log("group diff: " + entity.recentlyLeftGroups);
				}
			}
		}
		return hasChangedGroups;
	},
	
	/*
		Function: processGroups
		Sends spawn messages based on incoming entities in groups.
	*/
	processGroups: function() {
		var self = this;
		if (this.zoneGroupsReady) {
			this.map.forEachGroup(function(id) {
				var spawns = [];
				if (self.groups[id].incoming.length > 0) {
					spawns = _.each(self.groups[id].incoming, function(entity) {
						if (entity instanceof Player || entity instanceof Sweeper) {
							self.pushToGroup(id, new NetworkMessages.Spawn(entity), entity.id);
						} else if (entity instanceof Bot) {
							// Spawn the bot with empty behaviour
							self.pushToGroup(id, new NetworkMessages.Spawn(entity), entity.id);
							// Rebuild bot's behaviour with respect to each player and push a Behaviour message accordingly
							_.each(self.groups[id].players, function(playerId) {
								entity.updateState(null, self.getEntityById(playerId), function(state, behaviour) {
									var behaviourMessage = new NetworkMessages.Behaviour(0, entity.id, behaviour);
									behaviourMessage.send(self.getEntityById(playerId).socket);
								});
							});
						} else {
							self.pushToGroup(id, new NetworkMessages.Spawn(entity));
						}
					});
					self.groups[id].incoming = [];
				}
			});
		}
	},
	
	/*
		Function: getLocationInfo
		Retrieves location information from DB and passes it to a callback function.
		
		Parameters:
			callback	-	The callback function.
	*/
	getLocationInfo: function(callback) {
		this.server.db.query("SELECT * FROM location WHERE id = ?", [this.id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving location info");
				throw err;
			} else {
				callback(rows[0]);
			}
		});
	},
	
	/*
		Function: getCatalog
		Retrieves information about the catalog of this location store and passes it to a callback function.
		
		Parameters:
			callback	-	The callback function.
	*/
	getCatalog: function(callback) {
		if (this.store_query == null) {
			return [];
		} else {
			var query = "SELECT * FROM object";
			if (this.store_query.length > 0)
				query += " WHERE " + this.store_query;
			query += " ORDER BY price";
			this.server.db.query(query, [], function(err, rows, fields) {
				if (err) {
					console.log("Error retrieving location catalog");
					throw err;
				} else {
					var catalog = [];
					for (var i in rows) {
						catalog[i] = {id: rows[i].id, name: rows[i].name, description: rows[i].description, price: rows[i].price, icon: JSON.parse(rows[i].textures).icon};
					}
					callback(catalog);
				}
			});
		}
	},
	
	/*
		Function: sendTo
		Sends location description to a player.
		
		Parameters:
			player	-	The target player.
	*/
	sendTo: function(player) {
		var self = this;
		
		var locationWidth = this.map.width;
		var locationLength = this.map.length;
		
		// Location info
		var info = new NetworkMessages.LocationInfo(self.name, {lat: self.info.lat, lng: self.info.lng}, self.info.weather, self.info.indoor, self.playerCount);
		info.send(player.socket);
		
		// Create floor
		// Reminder: ids are still a bit random here
		var create = new NetworkMessages.Create(0, {active: true}, "Floor", Constants.Entities.FLOOR, {x: locationLength/2, y: -0.25, z: locationWidth/2}, {x: locationLength, y: 1, z: locationWidth}, 0, {world: {0: {SW: "asphalt", SE: "asphalt", NE: "asphalt", NW: "asphalt"}}, icon: ""}, {}, []);
		create.send(player.socket);
		
		// Create map objects (buildings and static stuff)
		for (var o in self.map.objects) {
			var obj = self.map.objects[o];
			var create = new NetworkMessages.Create(o, {active: true}, "", Constants.Entities.CITY_OBJECT, {x: obj.pos.x, y: obj.pos.y, z: obj.pos.z}, {x: obj.scale.x, y: obj.scale.y, z: obj.scale.z}, 0, {world: {0: obj.textures}, icon: ""}, {}, []);
			create.send(player.socket);
		}
		
		// Create portkeys
		for (var p in self.portkeys) {
			var portkey = self.portkeys[p];
			var pBehaviour = {state: {id: 0, name: "Default"}, methods: {}};
			pBehaviour.methods[Constants.Methods.GO] = {name: "Go", isSubject: true, isObject: false, auto: true, icon: "go", teleport: {location: portkey.destination.location, target: portkey.destination.pos}};
			var create = new NetworkMessages.Create(p, {active: true}, "", Constants.Entities.PORTKEY, portkey.pos, portkey.scale, 0, portkey.textures, pBehaviour, []);
			create.send(player.socket);
		}
		
		// Create lamps (yeah, they're special)
		if (self.map.lamps) {
			for (var l in self.map.lamps) {
				var lamp = self.map.lamps[l];
				var lampTextures = {world: {0: {SW: "lamp", SE: "lamp", NE: "lamp", NW: "lamp"}}, icon: ""};
				var create = new NetworkMessages.Create(l, {active: true}, "", Constants.Entities.LAMP, {x: lamp.x, y: lamp.y, z: lamp.z}, {x: 1, y: 1, z: 1}, 0, lampTextures, {}, []);
				create.send(player.socket);
			}
		}
		
		// Create walls
		if (self.map.walls) {
			for (var w in self.map.walls) {
				var wall = self.map.walls[w];
				for (var p=0; p<wall.length - 1; p++) {
					var point1 = [wall[p].pos[0], wall[p].pos[1]];
					var point2 = [wall[p+1].pos[0], wall[p+1].pos[1]];
					var orientation = 0;	// horizontal
					if (point1[0] == point2[0])
						orientation = 1;	// vertical
					if (point1[orientation] > point2[orientation]) {
						var temp = point2;
						point2 = point1;
						point1 = temp;
					}
					if (wall[p].type == "start") {
						point1[orientation] -= Constants.WALLS_DEPTH / 2;
					}
					if (wall[p].type == "door") {
						point1[orientation] += Constants.DOORS_WIDTH / 2;
					}
					if (wall[p+1].type == "end") {
						point2[orientation] += Constants.WALLS_DEPTH / 2;
					}
					if (wall[p+1].type == "door") {
						point2[orientation] -= Constants.DOORS_WIDTH / 2;
					}
					var segmentSizeX = Math.max(Math.abs(point2[0] - point1[0]), Constants.WALLS_DEPTH);
					var segmentSizeZ = Math.max(Math.abs(point2[1] - point1[1]), Constants.WALLS_DEPTH);
					
					var segmentPosY = Constants.WALLS_HEIGHT / 2;
					var segmentSizeY = Constants.WALLS_HEIGHT;
				
					var segmentCenterX = (point2[0] + point1[0]) / 2;
					var segmentCenterZ = (point2[1] + point1[1]) / 2;
					
					if (wall[p+1].type == "door") {
						var topCenterX = segmentCenterX + (1-orientation) * (segmentSizeX / 2 + Constants.DOORS_WIDTH / 2);
						var topCenterY = Constants.DOORS_HEIGHT + (Constants.WALLS_HEIGHT - Constants.DOORS_HEIGHT) / 2;
						var topCenterZ = segmentCenterZ + orientation * (segmentSizeZ / 2 + Constants.DOORS_WIDTH / 2);
						var topSizeX = Math.max((1-orientation) * Constants.DOORS_WIDTH, Constants.WALLS_DEPTH);
						var topSizeY = Constants.WALLS_HEIGHT - Constants.DOORS_HEIGHT;
						var topSizeZ = Math.max(orientation * Constants.DOORS_WIDTH, Constants.WALLS_DEPTH);
						var create = new NetworkMessages.Create(w*10+p, {active: true}, "", Constants.Entities.WALL, {x: topCenterX, y: topCenterY, z: topCenterZ}, {x: topSizeX, y: topSizeY, z: topSizeZ}, 0, {world: {0: {SW: "wall", SE: "wall", NE: "wall", NW: "wall"}}, icon: ""}, {}, []);
						create.send(player.socket);
					}
					
					var create = new NetworkMessages.Create(w*10+p, {active: true}, "", Constants.Entities.WALL, {x: segmentCenterX, y: segmentPosY, z: segmentCenterZ}, {x: segmentSizeX, y: segmentSizeY, z: segmentSizeZ}, 0, {world: {0: {SW: "wall", SE: "wall", NE: "wall", NW: "wall"}}, icon: ""}, {}, []);
					create.send(player.socket);
				}
			}
		}
		
		// Create items
		for (var i in self.items) {
			var item = self.items[i];
			item.spawn(true).send(player.socket);
		}

	}
	
}

module.exports = Location;