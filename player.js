/*
	Script: player
*/

var cls = require("./lib/class"),
	_ = require("underscore"),
	NetworkMessages = require("./message"),
	Items = require("./item"), Item = Items.Item, ItemFactory = Items.ItemFactory,
	Constants = require("./constants"),
	crypto = require('crypto'),
	fs = require('fs');

/*
	Class: Player
	Holds player information and manages game interactions
	
	Variables:
		tcpBuffer			-	A Buffer holding TCP data.
		socket				-	The network socket connecting this player to the server.
		location			-	The location in which this player is.
		id					-	The player id.
		kind				-	The kind (as in <Constants.Entities>).
		x					-	X coordinate of the player's position.
		z					-	Z coordinate of the player's position.
		level				-	Player level
		xp					-	Player experience points.
		money				-	A quantity of units generally accepted as payment for goods and services in the socio-economic context.
		outfits				-	Array describing the outfits currently worn by the player (associative by id).
		inventory			-	Array describing the items (objects, outfits, documents, hints and tokens) currently carried by the player (associative by id).
		behaviour			-	Player interactions.
		hasGreeted			-	Flag defining whether the player has already sent a HELLO message.
		hasEnteredGame		-	Flag defining whether the player has effectively entered the game.
		inventoryInterval	-	Interval between inventory updates.
		disconnectTimeout	-	Timer for disconnection.
*/

module.exports = Player = cls.Class.extend({

	/*
		Constructor: init
		Parameters:
			socket	-	The socket associated to this player.
			server	-	The server object.
	*/
	init: function(socket, server) {
		var self = this;
		
		// Buffer for TCP communication
		this.tcpBuffer = new Buffer(0);
		
		this.socket = socket;
		this.server = server;
		
		// Temporary id before actual login
		this.id = -1;
        this.kind = Constants.Entities.SPY;
        this.x = 0;
        this.z = 0;
		
		this.level = 0;
		this.xp = 0;
		this.money = 0;
		
		this.outfits = {};
		this.inventory = [];
		this.inventory[Constants.Inventory.OBJECT] = {};
		this.inventory[Constants.Inventory.OUTFIT] = {};
		this.inventory[Constants.Inventory.DOCUMENT] = {};
		this.inventory[Constants.Inventory.HINT] = {};
		this.inventory[Constants.Inventory.TOKEN] = {};
		
		this.behaviour = {};
		this.behaviour.state = {id: 0, name: "Default"};
		this.behaviour.name = "Default";
		this.behaviour.methods = {};
		this.behaviour.methods[Constants.Methods.GIVE] = {name: "Give", isSubject: false, isObject: true, auto: false, icon: "give"};
		this.behaviour.methods[Constants.Methods.SELL] = {name: "Sell", isSubject: false, isObject: true, auto: false, icon: "sell"};
		this.behaviour.methods[Constants.Methods.ROB] = {name: "Rob", isSubject: false, isObject: true, auto: true, icon: "rob"};
		
		this.hasGreeted = false;
		this.hasEnteredGame = false;

		this.disconnectTimeout = null;
		this.inventoryInterval = null;
		
		this.itemFactory = new ItemFactory(this.server.db);
		
		// Buffer management
		this.socket.on('data', function(data) {		// When TCP data are received
			// Add them to the buffer
			self.tcpBuffer = Buffer.concat([self.tcpBuffer, data]);
			if (self.tcpBuffer.length >= Constants.NET_HEADER_SIZE) {	// We have at least the message header
				// Read the payload size (contained in the header)
				var payloadSize = self.tcpBuffer.readUInt32LE(0);
				if (self.tcpBuffer.length >= Constants.NET_HEADER_SIZE + payloadSize) {		// We have the full message
					// Get only the message payload
					var message = self.tcpBuffer.slice(Constants.NET_HEADER_SIZE, Constants.NET_HEADER_SIZE + payloadSize);
					// Remove whole message (header and payload) from the buffer
					self.tcpBuffer = self.tcpBuffer.slice(Constants.NET_HEADER_SIZE + payloadSize);
					// Manage received message
					try {
						self.onMessageReceived(message);
					} catch (err) {
						console.log("ERROR: ", err.stack);
					}
				}
			}
		});
		
		this.socket.on('error', function() {
			clearTimeout(self.disconnectTimeout);
			clearInterval(self.inventoryInterval);
			self.onExit();
		});
		
		this.socket.on('close', function() {
			clearTimeout(self.disconnectTimeout);
			clearInterval(self.inventoryInterval);
			self.onExit();
		});
	
	},
	
	/*
		Function: onMessageReceived
		EVERYTHING.
		
		Parameters:
			message	-	The received <NetworkMessage>.
	*/
	onMessageReceived: function(message) {
		var self = this;
	
		var netMessage = NetworkMessages.decode(message);

		if (!self.hasGreeted && netMessage.message != "hello") { // HELLO must be the first message
			self.socket.end("Invalid handshake message: "+message);
			return;
		}
		if (self.hasGreeted && netMessage.message == "hello") { // HELLO can be sent only once
			self.socket.end("Cannot initiate handshake twice: "+message);
			return;
		}
		
		self.resetTimeout();
		
		// HELLO
		// Reminder: client says hello, server says welcome, then client tries to login
		if (netMessage.message == "hello") {
			self.hasGreeted = true;
		
			console.log("Received: HELLO");
			console.log("Replying: WELCOME");

			var welcome = new NetworkMessages.Welcome();
			welcome.send(self.socket);
			
		}
		
		// LOGIN
		// Reminder: the server sends back a login message, with id=userid when successful, id=0 otherwise
		else if (netMessage.message == "login") {
			
			var loginName = netMessage.name;
			var loginPassword = netMessage.password;
			var dbSalt;
			var dbHash;
			
			self.server.db.query("SELECT spy.id id, spy.name name, pw_salt, pw_hash " +
								"FROM player INNER JOIN spy ON player.id = spy.player_id " +
								"WHERE username = ?", [loginName], function(err, rows, fields) {
				if (err) {
					console.log("Login error");
					throw err;
				} else {
					if (rows.length != 1) {
						// The provided username doesn't exist
						console.log("Missing user");
						// Send the login error message
						var login = new NetworkMessages.Login(0, "", "");
						login.send(self.socket);
					} else {
						dbSalt = rows[0].pw_salt;
						dbHash = rows[0].pw_hash;
						var hash = crypto.createHash("sha256").update(dbSalt + loginPassword).digest('base64');
						if (hash == dbHash) {
							// Success
							var login = new NetworkMessages.Login(rows[0].id, rows[0].name, loginPassword);
							login.send(self.socket);
							// Setup the player
							self.id = rows[0].id;
							self.name = rows[0].name;
							self.onLogin();
						} else {
							console.log("Wrong credentials");
							var login = new NetworkMessages.Login(0, "", "");
							login.send(self.socket);
						}
					}
				}
			});
			
			// Skip the db
			/*var login = new NetworkMessages.Login(1, netMessage.name, netMessage.password);
			login.send(self.socket);
			self.id = 1;
			self.name = netMessage.name;
			self.onLogin();*/
			
		}
		
		// CITY
		else if (netMessage.message == "city") {
			console.log("City selection received: " + netMessage.id);
			if (netMessage.id == 0) {
				// The player is asking for a new location choice
				self.server.db.query("UPDATE spy s SET s.location_id = NULL WHERE s.id = ?", [self.id], function(err, rows, fields) {
					if (err) {
						console.log("Error updating player location");
						throw err;
					} else {
						self.onLeave(true);
					}
				});
			} else {
				// Get the correct location from id
				var location = _.detect(self.server.locations, function(location) {
					return location.id == netMessage.id;
				});
				// Location might be full
				if (location.playerCount < location.maxPlayers) {
					// Ok, get in
					self.location = location;
					var pos = self.location.map.getRandomStartingPosition();
					self.setPosition(pos.x, pos.z);
					self.savePosition();
					self.onInformationUpdated();
				} else {
					// No way
					self.startCitySelection();
				}
			}
		}
		
		// TEXTURE
		else if (netMessage.message == "texture") {
			console.log("Texture request received: " + netMessage.textureId);
			var filename = __dirname + "/textures/" + netMessage.textureId + ".png";
			fs.readFile(filename, function(err, imageData) {
				if (err) {
					console.log("Texture " + netMessage.textureId + " not found");
				} else {
					var texture = new NetworkMessages.Texture(netMessage.textureId, imageData);
					console.log("Sending texture: " + netMessage.textureId);
					texture.send(self.socket);
				}
			});
		}
		
		// MOVE
		else if (netMessage.message == "move") {
			var x = netMessage.coords.x,
				z = netMessage.coords.z;
				
			if (self.location.isValidPosition(x, z)) {
				// Update player position
				self.setPosition(x, z);
				// Notify players nearby
				self.broadcast(new NetworkMessages.Move(self.id, netMessage.coords));
				// Do other stuff (group checking, mainly)
				self.onMove(self.x, self.z);
			}
		}
		
		// LIST
		// Reminder: after login, the server sends a List message containing the ids of nearby entities;
		//			 the client sends back this List message, containing only the ids of entities he still has to create,
		//			 so the server sends the necessary Create messages.
		else if (netMessage.message == "list") {
			console.log("List request received");
			_.each(netMessage.ids, function(id) {
				var entity = self.location.getEntityById(id);
				if (entity instanceof Bot) {
					var create = new NetworkMessages.Spawn(entity);
					create.send(self.socket);
					entity.updateState(null, self, function(state, behaviour) {
						console.log("Sending behaviour for bot " + entity.id + " to player " + self.id);
						var behaviourMessage = new NetworkMessages.Behaviour(0, entity.id, behaviour);
						behaviourMessage.send(self.socket);
					});
				} else {
					var create = new NetworkMessages.Spawn(entity);
					create.send(self.socket);
				}
			});
		}
		
		// CHAT
		else if (netMessage.message == "chat") {
			var speaker = self.location.getEntityById(netMessage.id);
			console.log(speaker.name + " says something");
			// Notify only players in the same zone
			if (speaker instanceof Bot)
				speaker.broadcast(netMessage, self.id);
			else
				speaker.broadcast(netMessage);
		}
		
		// INVENTORY
		// Reminder: just like the List message, the server sends an Inventory message first, containing the ids of the objects in the player's inventory,
		//			 then the client sends this message, containing only the ids of objects he has to create.
		else if (netMessage.message == "inventory") {
			console.log("Inventory request received");
			for (var i in netMessage.ids) {
				var item = self.getItemById(netMessage.ids[i]);
				// The "active" flag is set to false for inventory items
				item.spawn(false).send(self.socket);
				// We also send a Collect message, so the client can put the newly created object in his inventory
				var collect = new NetworkMessages.Collect(netMessage.ids[i], item.name, item.textures.icon);
				collect.send(self.socket);
			}
		}
		
		// COLLECT
		// Reminder: the client sends this message when he wants to collect an item, the server evaluates the request and
		//			 confirms it by sending the message back
		else if (netMessage.message == "collect") {
			console.log("Collect message received");
			
			// Tell other players to animate this player
			self.broadcast(new NetworkMessages.Animation(self.id, "interacting"));
			
			var item = self.location.items[netMessage.id];
			var copy_of = item.copy_of;
			var hintData = item.properties.hintData;
			var money = item.properties.money;
			
			if (!money)				
				self.addToInventory(item);
			
			// Remove the item from the free location items
			delete self.location.items[netMessage.id];
			
			if (!money) {
				var saveCallback = function() {
					// Confirm by sending it back
					netMessage.send(self.socket);
					// Notify other players
					self.location.pushBroadcast(new NetworkMessages.Destroy(netMessage.id, Constants.Entities.OBJECT));
					// Tell everybody to stop animating this player
					self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
				};
				self.saveInventory(self.getItemKind(item), saveCallback);
			} else {
				// Update player money
				self.money += money;
				self.saveMoney(function() {
					self.sendInfo();
				});
				// Notify other players
				self.location.pushBroadcast(new NetworkMessages.Destroy(netMessage.id, Constants.Entities.OBJECT));
				// Tell everybody to stop animating this player
				self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
			}
			
			// If the item has an hint (but it's not a hint itself), evaluate it
			if (hintData && !hint)
				self.onHint(hintData);
			
			// Check if the item is the location target object
			self.checkWin(copy_of);

		}
		
		// DROP
		// Reminder: the client sends a Drop message, the server confirms by sending it back
		else if (netMessage.message == "drop") {
			console.log("Drop message received");
			
			// Tell other players to animate this player
			self.broadcast(new NetworkMessages.Animation(self.id, "interacting"));
			
			// Update object info
			self.server.db.query("UPDATE object_instance i INNER JOIN object o ON o.id = i.object_id " +
						"SET i.location_id = ?, i.posX = ?, i.posY = ?, i.posZ = ?, i.spy_id = NULL, i.state = (CASE WHEN o.outfit = 1 THEN 0 ELSE i.state END) " +
						"WHERE i.id = ?", [self.location.id, netMessage.coords.x, netMessage.coords.y, netMessage.coords.z, netMessage.id], function(err, rows, fields) {
				if (err) {
					console.log("Error updating item data");
					throw err;
				} else {
					var unequip = false;
					
					var kind = self.getItemKind(netMessage.id);
					var item = self.inventory[kind][netMessage.id];
					self.inventory[kind][netMessage.id].pos = {x: netMessage.coords.x, y: netMessage.coords.y, z: netMessage.coords.z};
					if (kind == Constants.Inventory.OUTFIT) {
						// If the player was wearing it, remove it from the self.outfits array
						if (self.inventory[kind][netMessage.id].state == 1) {
							delete self.outfits[netMessage.id];
							unequip = true;
						}
						// Set its state to 0 (not equipped)
						self.inventory[kind][netMessage.id].state = 0;
					}
					self.location.items[netMessage.id] = self.inventory[kind][netMessage.id];
					delete self.inventory[kind][netMessage.id];
					
					// Confirm by sending it back
					netMessage.send(self.socket);
					// Notify other players
					self.location.pushBroadcast(item.spawn(), self.id);
					if (unequip) {
						console.log("Updating player textures: ", {outfit: self.outfits});
						self.broadcast(new NetworkMessages.Textures(self.id, Constants.Entities.SPY, {outfit: self.outfits}), false);
					}
					
					// Tell everybody to stop animating this player
					self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
				}
			});				
		}
		
		// INTERACTION
		else if (netMessage.message == "interaction") {
			console.log("Interaction message received (" + netMessage.id + ": " + netMessage.subject + " - " + netMessage.object + ")");
			
			// Tell other players to animate this player
			self.broadcast(new NetworkMessages.Animation(self.id, "interacting"));			
			
			self.server.db.query("SELECT i.id, i.name, i.subject_id, i.object_id, i.attempt_precondition, i.success_precondition, i.postcondition " +
								"FROM interaction i " +
								"WHERE i.id = ?", [netMessage.id], function(err, rows, fields) {
				if (err) {
					console.log("Error retrieving interaction data from DB");
					throw err;
				} else {
					var applicable = true;
					var interaction = rows[0];
					
					// Find the subject item
					var subject = self.getItemById(netMessage.subject);
					if (subject == undefined) 
						subject = self.location.items[netMessage.subject];

					// Subject properties
					interaction.subjectType = subject.object_id;
					interaction.subjectState = subject.state;
					interaction.subjectProps = subject.properties;
					
					// Find the object item
					var object = self.getItemById(netMessage.object);
					if (object == undefined)
						object = self.location.items[netMessage.object];
					
					// Object properties (if there is an object)
					if (object != undefined) {
						interaction.objectType = object.object_id;
						interaction.objectState = object.state;
						interaction.objectProps = object.properties;
					}
					
					if (interaction.subject_id > 0 && interaction.subjectType != interaction.subject_id) {
						// The subject type doesn't meet the requirements
						// console.log("FALSE1: interaction.subject_id = " + interaction.subject_id + ", interaction.subjectType = " + interaction.subjectType);
						applicable = false;
					}
					if (interaction.object_id > 0 && interaction.objectType != interaction.object_id) {
						// The object type doesn't meet the requirements
						// console.log("FALSE2");
						applicable = false;
					}
					
					var checkAttemptPrecondition = new Function("item", interaction.attempt_precondition);
					applicable = applicable && checkAttemptPrecondition(subject);
					
					var checkSuccessPrecondition = new Function("subject", "object", interaction.success_precondition);
					applicable = applicable && checkSuccessPrecondition(subject, object);
					
					console.log("Interaction applicable: ", applicable);
					if (applicable) {
					
						var executeEffects = new Function("player", "subject", "object", "callback", interaction.postcondition);
						executeEffects(self, subject, object, function() {
							// Tell everybody to stop animating this player
							self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
						});
						
					} else {
						var notification = new NetworkMessages.Notification("Didn't work.");
						notification.send(self.socket);
						// Tell everybody to stop animating this player
						self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
					}
				}
			});
		}
		
		// DIALOGUE
		else if (netMessage.message == "dialogue") {
			console.log("Dialogue message received (" + netMessage.id + ": " + netMessage.bot + ")");
			
			if (netMessage.id == 0) {
				// Starting message, stop the bot's movement
				self.location.getEntityById(netMessage.bot).pauseMovement();
				// Send back to confirm conversation
				netMessage.send(self.socket);
			} else {
				self.server.db.query("SELECT d.id, d.bot_id, d.precondition, d.postcondition, d.dialogue " +
									"FROM bot_dialogues d " +
									"WHERE d.id = ?", [netMessage.id], function(err, rows, fields) {
					if (err) {
						console.log("Error retrieving dialogue data from DB");
						throw err;
					} else {
						var dialogue = rows[0];
						var bot = self.location.getEntityById(dialogue.bot_id);
						
						var executeEffects = new Function("message", "bot", "player", dialogue.postcondition);
						executeEffects(netMessage, bot, self);
					}
				});
			}
		}
		
		// INSERT
		// Reminder: the server confirms by sending the message back
		else if (netMessage.message == "insert") {
			console.log("Insert message received");
			
			// Tell other players to animate this player
			self.broadcast(new NetworkMessages.Animation(self.id, "interacting"));
			
			self.insertItem(netMessage.id, netMessage.container, function() {
				// Confirm by sending it back
				netMessage.send(self.socket);
				// Tell everybody to stop animating this player
				self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
			});
		}
		
		// LOOT
		else if (netMessage.message == "loot") {
			console.log("Loot message received");
			
			// Tell other players to animate this player
			self.broadcast(new NetworkMessages.Animation(self.id, "interacting"));
			
			// Find the container item
			var container = self.getItemById(netMessage.id);
			if (!container)
				container = self.location.items[netMessage.id];
			
			// Define the callback
			var saveCallback = function(container, winItem) {
				// Update the contents' properties
				self.server.db.query("UPDATE object_instance i " +
										"SET i.container_id = NULL " +
										"WHERE i.container_id = ?", [netMessage.id], function(err, rows, fields) {
					if (err) {
						console.log("Error updating item data");
						throw err;
					} else {
						// Save inventory and update container state
						self.saveInventory(function() {
							console.log("Content collected");
							self.updateItemState(container.id, container.state, function() {
								// Tell everybody to stop animating this player
								self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
							});
							if (winItem) {
								// One of the items was the location target, so start the winning procedure
								self.checkWin(winItem.copy_of);
							}
						});
					}
				});
			};
			
			var winItem;
			// Send Create and Collect messages for each content item
			for (var i in container.contents) {
				self.itemFactory.buildFromId(container.contents[i], function(content) {
					var money = content.properties.money;
					if (money) {
						var index = container.contents.indexOf(content.id);
						container.contents.splice(index, 1)[0];
						// Update player money
						self.money += money;
						self.saveMoney(function() {
							self.sendInfo();
						});
					} else {
						var copy_of = content.copy_of;
						self.addToInventory(content);
						var createInactive = content.spawn(false);
						createInactive.send(self.socket);
						var collect = new NetworkMessages.Collect(content.id);
						collect.send(self.socket);
						var index = container.contents.indexOf(content.id);
						container.contents.splice(index, 1)[0];
						// Check if the collected item is the location goal
						if (self.location.map.ending && self.location.map.ending.goal == content.copy_of) {
							winItem = content;
						}
					}
					if (container.contents.length == 0) {
						saveCallback(container, winItem);
					}
				});
			}
		}
		
		// EQUIP
		else if (netMessage.message == "equip") {
			console.log("Equip message received");
			
			// Update the outfit's state in DB
			self.server.db.query("UPDATE object_instance i " +
									"SET i.state = 1 " +
									"WHERE i.id = ?", [netMessage.id], function(err, rows, fields) {
				if (err) {
					console.log("Error equipping outfit");
					throw err;
				} else {
					// Update the outfit's state
					self.updateItemState(netMessage.id, 1);
					// Add the outfit to the self.outfits array
					self.outfits[netMessage.id] = self.inventory[Constants.Inventory.OUTFIT][netMessage.id].textures.outfit;
					// Confirm by sending it back
					netMessage.send(self.socket);
					// Notify other players
					self.broadcast(new NetworkMessages.Textures(self.id, Constants.Entities.SPY, {outfit: self.outfits}));
				}
			});
		}
		
		// UNEQUIP
		else if (netMessage.message == "unequip") {
			console.log("Unequip message received");
			
			// Update the outfit's state in DB
			self.server.db.query("UPDATE object_instance i " +
									"SET i.state = 0 " +
									"WHERE i.id = ?", [netMessage.id], function(err, rows, fields) {
				if (err) {
					console.log("Error unequipping outfit");
					throw err;
				} else {
					// Update the outfit's state
					self.updateItemState(netMessage.id, 0);
					// Remove the outfit from self.outfits
					delete self.outfits[netMessage.id];
					// Confirm by sending it back
					netMessage.send(self.socket);
					// Notify other players
					self.broadcast(new NetworkMessages.Textures(self.id, Constants.Entities.SPY, {outfit: self.outfits}));
				}
			});
		}
		
		// TELEPORT
		else if (netMessage.message == "teleport") {
			console.log("Teleport message received");
			var newLocation = self.server.getLocationById(netMessage.location);
			if (newLocation.isValidPosition(netMessage.coords.x, netMessage.coords.z)) {
				var retLocation = (self.location ? self.location.id : null);
				var retPos = (self.location ? {x: self.x, z: self.z} : {x: null, z: null});
				self.server.db.query("UPDATE spy s " +
									"SET s.location_id = ?, s.posX = ?, s.posZ = ?, s.ret_location_id = ?, ret_posX = ?, ret_posZ = ? " +
									"WHERE s.id = ?", [netMessage.location, netMessage.coords.x, netMessage.coords.z, retLocation, retPos.x, retPos.z, self.id], function(err, rows, fields) {
					if (err) {
						console.log("Error updating position");
						throw err;
					} else {
						self.onLeave();
						self.location = newLocation;
						self.setPosition(netMessage.coords.x, netMessage.coords.z);
						self.onInformationUpdated();
					}
				});
			}
		}
		
		// EXAMINE
		else if (netMessage.message == "examine") {
			console.log("Examine message received");
			var item;
			if (item = self.inventory[Constants.Inventory.DOCUMENT][netMessage.id]) {
				netMessage.text = item.properties.text;
				netMessage.send(self.socket);
			} else if (item = self.inventory[Constants.Inventory.HINT][netMessage.id]) {
				var hint = new NetworkMessages.Hint(item.id, item.properties.question);
				hint.send(self.socket);
			}
		}
		
		// HINT
		// Reminder: the client just tried to solve a hint he was examining
		else if (netMessage.message == "hint") {
			console.log("Hint message received");
			var hint = self.inventory[Constants.Inventory.HINT][netMessage.id];
			if (netMessage.answer.toLowerCase() == hint.properties.answer.toLowerCase()) {
				self.onHintSolved(hint);
			}
		}
		
		// TOKEN
		// Reminder: the client just used a mission token
		else if (netMessage.message == "token") {
			console.log("Token message received");
			var token = self.inventory[Constants.Inventory.TOKEN][netMessage.id];
			self.onToken(token);
		}
		
		// WIN
		// Reminder: the client is confirming its winning notification
		else if (netMessage.message == "win") {
			console.log("Win confirmation message received");
			var teleport = new NetworkMessages.Teleport(self.id, self.returnTo.location.id, self.returnTo.pos);
			teleport.send(self.socket);
		}
		
		// ROB
		else if (netMessage.message == "rob") {
			console.log("Rob message received");
			var victim = self.location.getEntityById(netMessage.id);
			
			var victimItems = victim.getInventoryIds();
			var total = victimItems.length;
			
			var robbery = self.computeRobCount(victim);
			if (!robbery.wallet && robbery.items == 0) {
				// FAIL.
				var notification = new NetworkMessages.Notification("You couldn't steal anything. Embarassing.");
				notification.send(self.socket);
			} else {
			
				var robCallback = function(robbery) {
					var notification = new NetworkMessages.Notification("Wow. You actually got away with it.");
					notification.send(self.socket);
					if (victim.level > 10 && victim instanceof Player) {
						notification = new NetworkMessages.Notification("Somebody just robbed you.");
						notification.send(victim.socket);
					}
				}
			
				if (robbery.items > 0) {
					var robCount = 0;
					while (robCount < Math.min(robbery.items, total)) {
						var robbedId = victimItems[Math.floor(Math.random()*victimItems.length)];
						var robbedKind;
						if (victim instanceof Player)
							robbedKind = victim.getItemKind(robbedId);
						if (robbedKind == Constants.Inventory.OUTFIT) {
							delete victim.outfits[robbedId];
							victim.inventory[Constants.Inventory.OUTFIT][robbedId].state = 0;
							victim.broadcast(new NetworkMessages.Textures(victim.id, Constants.Entities.SPY, {outfit: victim.outfits}), false);
						}
						var robbedItem = victim.removeFromInventory(robbedId);
						if (robbedItem != null) {
							var money = robbedItem.properties.money;
							if (money) {
								// Update player money
								self.money += money;
								self.saveMoney(function() {
									self.sendInfo();
								});
							} else {
								self.addToInventory(robbedItem);
							}
							victimItems.splice(victimItems.indexOf(robbedId), 1);
							robCount++;
						}
					}
					
					self.saveInventory(function() {
						victim.saveInventory(function() {
							self.sendInventory();
							if (victim instanceof Player)
								victim.sendInventory();
							robCallback(robbery);
						});
					});
				}
				
				if (robbery.wallet) {
					self.money = victim.money;
					victim.money = 0;
					self.saveMoney(function() {
						victim.saveMoney(function() {
							self.sendInfo();
							if (victim instanceof Player)
								victim.sendInfo();
							robCallback(robbery);
						});
					});
				}
				
			}
		}
		
		// GIVE
		else if (netMessage.message == "give") {
			console.log("Give message received");
			
			// Tell other players to animate this player
			self.broadcast(new NetworkMessages.Animation(self.id, "interacting"));
			
			var recipient = self.location.getEntityById(netMessage.recipient);
			var itemKind = self.getItemKind(netMessage.object);
			if (itemKind == Constants.Inventory.OUTFIT) {
				delete self.outfits[netMessage.object];
				self.inventory[Constants.Inventory.OUTFIT][netMessage.object].state = 0;
				self.broadcast(new NetworkMessages.Textures(self.id, Constants.Entities.SPY, {outfit: self.outfits}), false);
			}
			recipient.addToInventory(self.removeFromInventory(netMessage.object));
			self.saveInventory(function() {
				self.sendInventory();
				// Tell everybody to stop animating this player
				self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
			});
			recipient.saveInventory(function() {
				recipient.sendInventory();
			});
		}
		
		// MONEY TRANSFER
		else if (netMessage.message == "moneytransfer") {
			console.log("Money transfer message received");
			
			// Tell other players to animate this player
			self.broadcast(new NetworkMessages.Animation(self.id, "interacting"));
			
			if (netMessage.recipientId == 0) {
				// Drop the money
				// Create the item instance in DB
				self.server.createMoneyItem(netMessage.amount, self.location.id, {x: self.x + 0.5, y: 0.1, z: self.z + 0.5}, function(moneyId) {
					// Build the item and spawn it
					self.itemFactory.buildFromId(moneyId, function(item) {
						self.location.addItem(item);
						self.location.pushBroadcast(item.spawn());
						// Update player money
						self.money -= netMessage.amount;
						self.saveMoney(function() {
							self.sendInfo();
							// Tell everybody to stop animating this player
							self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
						});
					});
				});
			} else {
				if (netMessage.recipientKind == Constants.Entities.SPY) {
					// Give the money to the other guy
					var recipient = self.location.getEntityById(netMessage.recipientId);
					self.money -= netMessage.amount;
					recipient.money += netMessage.amount;
					self.saveMoney(function() {
						self.sendInfo();
						// Tell everybody to stop animating this player
						self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
					});
					recipient.saveMoney(function() {
						recipient.sendInfo();
					});
				} else {
					// Insert the money in the thing.
					// Create the item instance in DB
					self.server.createMoneyItem(netMessage.amount, null, {x: self.x + 0.5, y: 0.1, z: self.z + 0.5}, function(moneyId) {
						// Build the item and spawn it
						self.itemFactory.buildFromId(moneyId, function(item) {
							// Rebuild container behaviour
							self.insertItem(item, netMessage.recipientId);
							// Update player money
							self.money -= netMessage.amount;
							self.saveMoney(function() {
								self.sendInfo();
								// Tell everybody to stop animating this player
								self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
							});
						});
					});
				}
			}
		}
		
		// SELL
		else if (netMessage.message == "sell") {
			console.log("Sell message received");
			
			// Tell other players to animate this player
			self.broadcast(new NetworkMessages.Animation(self.id, "interacting"));
			
			if (netMessage.buyer == 0 && netMessage.buyers.length == 0) {
				// Send potential buyers info
				self.getBuyersInfo(netMessage.item, function(buyers) {
					netMessage.buyers = buyers;
					netMessage.send(self.socket);
				});
			} else {
				// Sell the item
				if (netMessage.buyer == 0) {
					// Sell to a company
					var buyer = netMessage.buyers[0];
					self.server.db.query("DELETE FROM object_instance WHERE id = ?", [netMessage.item], function(err, rows, fields) {
						if (err) {
							console.log("Error selling item");
							throw err;
						} else {
							self.removeFromInventory(netMessage.id);
							self.updateInventory(function() {
								self.sendInventory();
								self.money += parseInt(buyer.offer);
								self.saveMoney(function() {
									self.sendInfo();
									// Tell everybody to stop animating this player
									self.location.pushBroadcast(new NetworkMessages.Animation(self.id, "interacting"));
								});
							});
						}
					});
				} else {
					// Sell to a player
					var otherParty;
					if (netMessage.seller == self.id) {
						otherParty = self.location.getEntityById(netMessage.buyer);
					} else {
						otherParty = self.location.getEntityById(netMessage.seller);
					}
					console.log("Sale negotiation: item " + netMessage.item + " from " + netMessage.seller + " to " + netMessage.buyer + " for $ " + netMessage.price);
					// Tell everybody to animate the other party
					self.location.pushBroadcast(new NetworkMessages.Animation(otherParty.id, "interacting"));
					// Check the running transactions
					var sellId = netMessage.item + "-" + netMessage.seller + "-" + netMessage.buyer;
					if (self.location.transactions[sellId] == undefined) {
						// Start the negotiation
						self.location.transactions[sellId] = {item: netMessage.item, seller: netMessage.seller, price: netMessage.price};
						// Send the offer to the other party
						netMessage.send(otherParty.socket);
					} else {
						// The negotiation is already open, so this was a reply offer
						if (netMessage.price != self.location.transactions[sellId].price) {
							// Someone didn't like the previous offer
							self.location.transactions[sellId].price = netMessage.price;
							netMessage.send(otherParty.socket);
						} else {
							// The previous offer was accepted, sell the item
							var seller = self.location.getEntityById(netMessage.seller);
							var buyer = self.location.getEntityById(netMessage.buyer);
							var itemKind = seller.getItemKind(netMessage.item);
							if (itemKind == Constants.Inventory.OUTFIT) {
								delete seller.outfits[netMessage.item];
								seller.inventory[Constants.Inventory.OUTFIT][netMessage.item].state = 0;
								seller.broadcast(new NetworkMessages.Textures(seller.id, Constants.Entities.SPY, {outfit: seller.outfits}), false);
							}
							buyer.addToInventory(seller.removeFromInventory(netMessage.item));
							seller.saveInventory(function() {
								seller.sendInventory();
							});
							buyer.saveInventory(function() {
								buyer.sendInventory();
							});
							seller.money += netMessage.price;
							seller.saveMoney(function() {
								seller.sendInfo();
								// Tell everybody to stop animating the seller
								seller.location.pushBroadcast(new NetworkMessages.Animation(seller.id, "interacting"));
							});
							buyer.money -= netMessage.price;
							buyer.saveMoney(function() {
								buyer.sendInfo();
								// Tell everybody to stop animating the buyer
								buyer.location.pushBroadcast(new NetworkMessages.Animation(buyer.id, "interacting"));
							});
							delete self.location.transactions[sellId];
							// A Sell message with price 0 to signal the end of the transaction
							netMessage.price = 0;
							netMessage.send(seller.socket);
							netMessage.send(buyer.socket);
						}
					}
				}
			}
		
		}
		
		// BUY
		else if (netMessage.message == "buy") {
			console.log("Buy message received");
			var objectId = netMessage.id;
			self.server.createItem(objectId, null, null, self.id, function(itemIds, onBundle, components) {
				var variables = [];
				var completed = 0;
				for (var i=0; i<itemIds.length; i++) {
					(function(index) {
						self.itemFactory.buildFromId(itemIds[index], function(item) {
							self.addToInventory(item);
							variables[index] = item;
							self.money -= item.price;
							completed++;
							if (completed == itemIds.length) {
								var done = function() {
									self.saveMoney(function() {
										self.sendInfo();
									});
									self.sendInventory();
								};
								if (onBundle != undefined) {
									onBundle.apply(onBundle, variables);
									completed = 0;
									for (var i in variables) {
										self.updateItemProperties(variables[i].id, variables[i].properties, function(item) {
											self.itemFactory.buildBehaviour(item, function() {
												completed++;
												if (completed == variables.length) {
													done();
												}
											});
										});
									}
								} else {
									done();
								}
							}
						});
					})(i);
				}
			});
			
		}
		
		else {
			console.log("Unknown message");
		}
	},
	
	/*
		Function: onLogin
		Called after a successful login
	*/
	onLogin: function() {
		var self = this;

		// Update user state and position, then proceed
		self.requestState(function(state) {
			self.level = state.level;
			self.xp = state.xp;
			self.money = state.money;
			self.outfits = state.outfits;
			self.home = self.server.getLocationById(state.home_id);
			self.requestPosition(function(location, pos, retLocation, retPos) {
			
				if (location) {
					self.location = location;
					self.setPosition(pos.x, pos.z);
					self.savePosition();
					if (retLocation) {
						self.returnTo = {location: retLocation, pos: retPos};
					}
					self.onInformationUpdated();
				} else {
					self.startCitySelection();
				}
			
			});
		});
	},
	
	/*
		Function: requestState
		Retrieves basic player information (level, xp, money, home and outfits) and passes it to a callback function.
		
		Parameters:
			stateCallback	-	The callback function.
	*/
	requestState: function(stateCallback) {
		var self = this;
		
		self.server.db.query("SELECT level, xp, money, home_id FROM spy WHERE id = ?", [self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving state");
				throw err;
			} else {
				var state = {level: rows[0].level, xp: rows[0].xp, money: rows[0].money, home_id: rows[0].home_id};
				state.outfits = {};
				// Default outfit
				state.outfits[1] = {index: 0, animations: {walking: "baseWalking", interacting: "baseInteracting"}};
				self.server.db.query("SELECT i.id, o.name, o.textures " +
									"FROM object o INNER JOIN object_instance i ON i.object_id = o.id " +
									"WHERE i.spy_id = ? AND o.outfit = 1 AND i.state = 1", [self.id], function(err, rows, fields) {
					if (err) {
						console.log("Error retrieving outfits");
						throw err;
					} else {
						for (var t in rows) {
							var textures = JSON.parse(rows[t].textures);
							state.outfits[rows[t].id] = textures.outfit;
						}
						stateCallback(state);
					}
				});
			}
		});
	},
	
	/*
		Function: requestPosition
		Retrieves player position and passes it to a callback function.
		
		Parameters:
			posCallback	-	The callback function.
	*/
	requestPosition: function(posCallback) {
		var self = this;
		
		self.server.db.query("SELECT location_id, posX, posZ, ret_location_id, ret_posX, ret_posZ FROM spy WHERE id = ?", [self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving position");
				throw err;
			} else {
				
				// Find and assign the correct location from its id
				var location = _.detect(self.server.locations, function(location) {
					return location.id == rows[0].location_id;
				});
				var coords = {x: rows[0].posX, z: rows[0].posZ};
				// If no previous position is recorded, assign a random one
				if (location && rows[0].posX == null) {
					coords = location.map.getRandomStartingPosition();
				}
				
				// Find and assign the correct return location from its id
				var retLocation = _.detect(self.server.locations, function(location) {
					return location.id == rows[0].ret_location_id;
				});
				var retCoords = {x: rows[0].ret_posX, y: 0.3, z: rows[0].ret_posZ};
				
				posCallback(location, coords, retLocation, retCoords);
			}
		});
	},
	
	/*
		Function: startCitySelection
		Gathers data about available cities and sends them to the player.
	*/
	startCitySelection: function() {

		var citiesData = this.server.getCities();
		var cities = new NetworkMessages.Cities(citiesData);
		cities.send(this.socket);
		
	},
	
	/*
		Function: onInformationUpdated
		Called after first state update; instructs the client to create the player object and updates inventory.
	*/
	onInformationUpdated: function() {
		var self = this;
		
		// Stats, connections, queues, etc.
		self.location.addPlayer(self);
		// Game stuff
		self.location.onPlayerEnter(self);
		
		// Tell the client to create the player
		var create = new NetworkMessages.Spawn(self);
		create.send(self.socket);
		
		self.hasEnteredGame = true;
		
		self.updateInventory(function() {
			self.sendInventory();
		});
		
		self.sendInfo();
		
		self.inventoryInterval = setInterval(function() {
			self.updateInventory(function() {
				self.sendInventory();
			});
		}, 10*60*1000);
		
	},
	
	/*
		Function: savePosition
		Stores current position in DB.
	*/
	savePosition: function() {
		var self = this;
		self.server.db.query("UPDATE spy SET location_id = ?, posX = ?, posZ = ? WHERE id = ?", [self.location.id, self.x, self.z, self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error saving position");
				throw err;
			} else {
				console.log("Position saved");
			}
		});
	},
	
	/*
		Function: incrementXp
		Increments the player's XP count (and level, if needed).
		
		Parameters:
			addXp	-	The XP points to be added.
	*/
	incrementXp: function(addXp) {
		this.xp += addXp;
	},
	
	/*
		Function: checkWin
		Checks if a collected item is the current location's goal, and manages the situation.
		
		Parameters:
			itemId	-	The collected item's id.
	*/
	checkWin: function(itemId) {
		var self = this;
		
		// Check if the item is the location target object
		if (self.location.map.ending && self.location.map.ending.goal == itemId) {
			console.log(self.name + " found it!");
			
			self.location.markForDeletion();
			// Compute the reward for each player in the location (the one who found the object gets a 20% bonus)
			for (var p in self.location.players) {
				self.manageWin(self.location.players[p], itemId, 0.2);
			}
		}
	},
	
	/*
		Function: manageWin
		Computes the reward and sends the appropriate messages to the player.
		REMINDER: The "self" player is the one who found the item.
		
		Parameters:
			player		-	The player.
			itemId		-	The winning item id.
			bonus		-	The bonus fraction for the player who found the item.
	*/
	manageWin: function(player, itemId, bonus) {
		var self = this;
		if (bonus == undefined)
			bonus = 0.2;
		
		// Everybody else in the location gets a copy too
		if (player.id != self.id) {
			self.server.db.query("INSERT INTO object_instance (object_id, location_id, posX, posY, posZ, rotation, container_id, spy_id, state, properties) " +
								"SELECT object_id, NULL, posX, posY, posZ, rotation, NULL, ?, state, properties " +
								"FROM object_instance " +
								"WHERE id = ?", [player.id, itemId], function(err, rows, fields) {
				if (err) {
					console.log("Error copying item");
					throw err;
				} else {
					// Delete any token for this location the player may have
					var tokenIds = [];
					for (var t in player.tokens) {
						if (player.tokens[t].properties.location == self.location.id)
							tokenIds.push(t);
					}
					var tokenQuery;
					var tokenParams;
					if (tokenIds.length > 0) {
						tokenQuery = "DELETE FROM object_instance WHERE id IN (?)";
						tokenParams = [tokenIds.join()];
					} else {
						tokenQuery = "SELECT 0 FROM DUAL";
						tokenParams = [];
					}
					self.server.db.query(tokenQuery, tokenParams, function(err, rows, fields) {
						if (err) {
							console.log("Error deleting tokens");
							throw err;
						} else {
							player.updateInventory(function() {
								player.sendInventory();
							});
						}
					});
				}
			});
		}
		
		var xpReward;
		if (player.id == self.id) {
			xpReward = Math.round(self.location.map.ending.reward.xp * (1 + bonus) / (Object.keys(self.location.players).length + bonus));
		} else {
			xpReward = Math.round(self.location.map.ending.reward.xp / (Object.keys(self.location.players).length + bonus));
		}
		console.log("Reward for " + player.name + ": " + xpReward);
		player.incrementXp(xpReward);
		self.server.db.query("UPDATE spy s SET s.xp = ?, s.level = ? WHERE id = ?", [player.xp, player.level, player.id], function(err, rows, fields) {
			if (err) {
				console.log("Error updating xp");
				throw err;
			} else {
				console.log("XP updated, sending win message to " + player.name);
				var win = new NetworkMessages.Win({xp: xpReward, message: self.location.map.ending.reward.message});
				win.send(player.socket);
			}
		});
	},
	
	/*
		Function: getBuyers
		Computes information about potential buyers for the document and passes it to a callback function.
		
		Parameters:
			document	-	The document to sell (id or full object).
			callback	-	A callback function
	*/
	getBuyersInfo: function(document, callback) {
		var self = this;
		
		var query;
		var params;
		if (typeof document == "object") {
			query = "SELECT id, name, agency_id, company_id, level " +
					"FROM doc_cluster " +
					"WHERE id = (SELECT doc_cluster_id " +
								"FROM doc_cluster_element " +
								"WHERE id = ?)";
			params = [document.doc_cluster_element_id];
		} else {
			query = "SELECT id, name, agency_id, company_id, level " +
					"FROM doc_cluster " +
					"WHERE id = (SELECT doc_cluster_id " +
								"FROM doc_cluster_element " +
								"WHERE id = (SELECT doc_cluster_element_id " +
											"FROM object_instance " +
											"WHERE id = ?))";
			params = [document];
		}
		
		var buyers = [];
		self.server.db.query(query, params, function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving document cluster");
				throw err;
			} else {
				var cluster = rows[0];
				self.server.db.query("SELECT id, name, level, 5 AS market_sector_id, 'agency' AS kind FROM agency " +
							"WHERE id = ? " +
							"UNION " +
							"SELECT id, name, level, market_sector_id, 'company' AS kind FROM company " +
							"WHERE id = ? ", [cluster.agency_id, cluster.company_id], function(err, rows, fields) {
					if (err) {
						console.log("Error retrieving companies");
						throw err;
					} else {
						var subject = rows[0];
						var level = cluster.level + subject.level;
						self.server.db.query("SELECT id, name, level, 5, 'agency' AS kind FROM agency " +
											"WHERE ABS(level - ?) < 20 " +
											"UNION " +
											"SELECT id, name, level, market_sector_id, 'company' AS kind FROM company " +
											"WHERE market_sector_id = ? AND ABS(level - ?) < 30", [subject.level, subject.market_sector_id, subject.level], function(err, rows, fields) {
							if (err) {
								console.log("Error retrieving companies");
								throw err;
							} else {
								for (var r in rows) {
									var entity = rows[r];
									var offer = level * 10;
									if (entity.id == cluster.agency_id || entity.id == cluster.company_id)
										offer *= 5;
									buyers.push({id: entity.id, kind: entity.kind, name: entity.name, offer: offer});
								}
								callback(buyers);
							}
						});
					}
				});
			}
		});
	},
	
	/*
		Function: onHint
		Manages the creation of a hint.
		
		Parameters:
			hint	-	The hint descriptor object.
	*/
	onHint: function(hint) {
		var self = this;
		
		self.server.createHint(hint, self, function(hintId) {
			self.updateInventory(Constants.Inventory.HINT, function() {
				self.sendInventory();
			});
		});
	},
	
	/*
		Function: onHintSolved
		Manages the solution of a hint.
		
		Parameters:
			hint	-	The hint object.
	*/
	onHintSolved: function(hint) {
		var self = this;
		
		self.server.createToken(hint.properties.location, self, function(tokenId) {
			// Delete the hint
			delete self.inventory[Constants.Inventory.HINT][hint.id];
			self.server.db.query("DELETE FROM object_instance WHERE id = ?", [hint.id], function(err, rows, fields) {
				if (err) {
					console.log("Error deleting hint");
					throw err;
				} else {
					self.updateInventory(Constants.Inventory.TOKEN, function() {
						self.sendInventory();
					});
				}
			});
		});
	},
	
	/*
		Function: onToken
		Manages the usage of a token.
		
		Parameters:
			token	-	The token object.
	*/
	onToken: function(token) {
		var self = this;
		
		var location = self.server.getLocationById(token.properties.location);
		
		var onLocationReady = function(location) {
			self.returnTo = {location: self.location, pos: {x: self.x, y: 0.3, z: self.z}};
			var teleport = new NetworkMessages.Teleport(self.id, location.id, {x: 1, y: 0.3, z: 1});
			teleport.send(self.socket);
		};

		if (location.copy_of) {
			// The token references a specific instance, so just send the player there
			onLocationReady(location);
		} else {
			// The token references a secret location that hasn't been instantiated yet
			// Find the location and instantiate (duplicate) it
			self.server.db.query("INSERT INTO location (name, lat, lng, weather, indoor, max_players, descriptor, copy_of) " +
								"SELECT name, lat, lng, weather, indoor, max_players, descriptor, id " +
								"FROM location WHERE id = ?", [location.id], function(err, resultLocation) {
				if (err) {
					console.log("Error instantiating location");
					throw err;
				} else {
					// Update the token properties
					token.properties.location = resultLocation.insertId;
					self.server.db.query("UPDATE object_instance SET properties = ? WHERE id = ?", [JSON.stringify(token.properties), token.id]);
					// Find and duplicate all the objects inside
					self.server.db.query("INSERT INTO object_instance (object_id, location_id, posX, posY, posZ, rotation, container_id, spy_id, " +
																		"state, properties, doc_cluster_element_id, copy_of) " +
									"SELECT i1.object_id, ?, i1.posX, i1.posY, i1.posZ,i1. rotation, container_id, i1.spy_id, "+
											"i1.state, i1.properties, i1.doc_cluster_element_id, i1.id " +
									"FROM object_instance i1 "+
									"WHERE location_id = ?", [resultLocation.insertId, location.id], function(err, resultObjects) {
						if (err) {
							console.log("Error instantiating location");
							throw err;
						} else {
							// Fix containers
							self.server.db.query("UPDATE object_instance i1 INNER JOIN object_instance i2 ON i1.container_id = i2.copy_of " +
												"SET i1.container_id = i2.id " +
												"WHERE i1.location_id = ?;", [resultLocation.insertId], function(err, resultContainers) {
								if (err) {
									console.log("Error instantiating location");
									throw err;
								} else {
									// Add the new location instance to the server and move the player there
									var location_id = resultLocation.insertId;
									self.server.addLocation(location_id, onLocationReady);
								}
							});
						}
					});
				}
			});
		}
	},
	
	/*
		Function: computeRobCount
		Parameters
			victim	-	The poor guy.
			
		Returns:
			An object indicating if the wallet can be stolen and the number of inventory items that the player can steal.
	*/
	computeRobCount: function(victim) {
		var self = this;
		
		var robbery = {};
		
		var coefficient = (self.level - victim.level) / Math.max(self.level, victim.level);
		
		robbery.wallet = victim.money && victim.money != null && Math.random() < coefficient;
		
		var maxRobbed = (robbery.wallet ? 4 : 5);		// Maximum amount of stolen items ever in the history of the world.
		var maxIncrement = 1/(maxRobbed+1) / Math.floor((maxRobbed+1)/2);
		var increment = coefficient * maxIncrement;
		var probabilities = [];
		for (var i=0; i<maxRobbed+1; i++) {
			probabilities[i] = 1/(maxRobbed+1) + (i-Math.floor((maxRobbed+1)/2)) * increment;
		}
		var cumulative = [];
		cumulative[0] = probabilities[0];
		for (var i=1; i<probabilities.length; i++) {
			cumulative[i] = probabilities[i] + cumulative[i-1];
		}
		var rand = Math.random();
		for (var i=0; i<cumulative.length; i++) {
			if (rand < cumulative[i])
				break;
		}
		robbery.items = i;
		return robbery;
	},
	
	/*
		Function: insertItem
		Tries to insert an item in a container
		
		Parameters:
			itemId		-	Item (id or full object).
			containerId	-	Container (id or full object).
			callback	-	A callback function.
	*/
	insertItem: function(item, container, callback) {
		var self = this;
		var itemId = item;
		var containerId = container;
		if (typeof item == "object") {
			itemId = item.id;
		}
		if (typeof container == "object") {
			containerId = container.id;
		}
		
		// Check the amount of free space in the container
		self.server.db.query("SELECT SUM(o.scaleX*o.scaleY*o.scaleZ) fillVolume " +
									"FROM object_instance i INNER JOIN object o ON o.id = i.object_id " +
									"WHERE i.container_id = ?", [containerId], function(err, rows, fields) {
			if (err) {
				console.log("Error computing free space in container");
				throw err;
			} else {
				// Find the inserted item
				if (typeof item != "object")
					item = self.getItemById(itemId);
				
				var fillVolume = rows[0].fillVolume != null ? rows[0].fillVolume : 0;
				var itemVolume = item.scale.x * item.scale.y * item.scale.z;
				if (fillVolume + itemVolume > item.properties.capacity) {
					// The item won't fit
					console.log("Container full");
					var notify = new NetworkMessages.Notification("There is not enough space in this object");
					notify.send(self.socket);
				} else {
					if (typeof container != "object") {
						var kind = self.getItemKind(containerId);
						// Add the item to the container's contents
						if (kind != null)
							container = self.inventory[kind][containerId];
						else
							container = self.location.items[containerId];
					}				
					self.server.db.query("UPDATE object_instance i " +
											"SET i.container_id = ?, " +
											"    i.location_id = NULL, " +
											"    i.spy_id = NULL " +
											"WHERE i.id = ?", [containerId, container.location_id, itemId], function(err, rows, fields) {
						if (err) {
							console.log("Error updating item data");
							throw err;
						} else {
							container.contents.push(itemId);
							// Update container's behaviour
							self.itemFactory.buildFromId(container.id, function(container) {
								var behaviourMsg = new NetworkMessages.Behaviour(container.id, 0, container.behaviour);
								self.location.pushBroadcast(behaviourMsg);
							});
							kind = self.getItemKind(itemId);
							if (kind != null)
								delete self.inventory[kind][itemId];
							if (callback)
								callback();
						}
					});
				}
			}
		});
	},
	
	/*
		Function: sendInfo
		Sends player info to the client.
	*/
	sendInfo: function() {
		var homeId;
		if (this.home == null)
			homeId = 0;
		else
			homeId = this.home.id;
		var info = new NetworkMessages.PlayerInfo(this.id, this.level, this.xp, this.money, homeId);
		info.send(this.socket);
	},
	
	/*
		Function: getInventoryIds
		Parameters
			kind	-	The inventory kind (<Constants.Inventory>) (optional).
		
		Returns
			An array containing the ids of all the inventory items.
	*/
	getInventoryIds: function(kind) {
		var invIds = [];
		if (kind && kind > 0) {
			for (var i in this.inventory[kind]) {
				invIds.push(this.inventory[kind][i].id);
			}
		} else {
			for (var k in this.inventory) {
				for (var i in this.inventory[k]) {
					invIds.push(this.inventory[k][i].id);
				}
			}
		}
		return invIds;
	},
	
	/*
		Function: getInventorySize
		Parameters:
			kind	-	Inventory kind (<Constants.Inventory>) (optional).
			
		Returns
			The total inventory current size.
	*/
	getInventorySize: function(kind) {
		var invIds = this.getInventoryIds(kind);
		return invIds.length;
	},
	
	/*
		Function: getItemById
		Parameters:
			id	-	The item id.
		
		Returns
			The item.
	*/
	getItemById: function(id) {
		for (var kind in this.inventory) {
			if (this.inventory[kind][id] != undefined)
				return this.inventory[kind][id];
		}
		return null;
	},
	
	/*
		Function: getItemKind
		Parameters:
			id	-	The item id.
		
		Returns
			The item inventory kind (<Constants.Inventory>).
	*/
	getItemKind: function(id) {
		for (var kind in this.inventory) {
			if (this.inventory[kind][id] != undefined)
				return kind;
		}
		return null;
	},
	
	/*
		Function: sendInventory
		Sends a list of all inventory items as an Inventory message to the player.
	*/
	sendInventory: function() {
		var self = this;
		
		var invMsg = new NetworkMessages.Inventory(self.getInventoryIds());
		console.log("Sending inventory message");
		invMsg.send(self.socket);
	},
	
	/*
		Function: addToInventory
		Parameters:
			item	-	The item.
	*/
	addToInventory: function(item) {
		if (item.flags.outfit)
			this.inventory[Constants.Inventory.OUTFIT][item.id] = item;
		else if (item.flags.document)
			this.inventory[Constants.Inventory.DOCUMENT][item.id] = item;
		else if (item.flags.hint)
			this.inventory[Constants.Inventory.HINT][item.id] = item;
		else if (item.flags.token)
			this.inventory[Constants.Inventory.TOKEN][item.id] = item;
		else
			this.inventory[Constants.Inventory.OBJECT][item.id] = item;
	},
	
	/*
		Function: removeFromInventory
		Parameters:
			id	-	The item id.
		
		Returns:
			The removed item.
	*/
	removeFromInventory: function(id) {
		for (var kind in this.inventory) {
			if (this.inventory[kind][id] != undefined) {
				var item = this.inventory[kind][id];
				delete this.inventory[kind][id];
				return item;
			}
		}
		return null;
	},
	
	/*
		Function: updateInventory
		Fills inventory from DB.
		
		Parameters:
			kind		-	The inventory kind (<Constants.Inventory>) (optional).
			callback	-	A callback function.
	*/
	updateInventory: function(kind, callback) {
		// Manage optional parameter
		if (typeof kind == "function") {
			callback = kind;
			kind = -1;
		}
	
		var self = this;
		
		var whereCondition = "";
		
		if (kind >= 0) {
			self.inventory[kind] = {};
			if (kind == Constants.Inventory.OBJECT)
				whereCondition = " AND o.outfit + o.document + o.hint + o.token = 0";
			else if (kind == Constants.Inventory.OUTFIT)
				whereCondition = " AND o.outfit = 1";
			else if (kind == Constants.Inventory.DOCUMENT)
				whereCondition = " AND o.document = 1";
			else if (kind == Constants.Inventory.HINT)
				whereCondition = " AND o.hint = 1";
			else if (kind == Constants.Inventory.TOKEN)
				whereCondition = " AND o.token = 1";
		} else {
			for (var k in self.inventory) {
				self.inventory[k] = {};
			}
		}
		
		self.server.db.query("SELECT i.id, i.posX, i.posY, i.posZ, o.id object_id, o.pickable, o.container, o.outfit, o.document, o.hint, o.token, " +
										"o.name, o.scaleX, o.scaleY, o.scaleZ, o.price, o.textures, " +
										"o.descriptor, o.properties baseProperties, o.parts, i.state, i.properties " +
								"FROM object o INNER JOIN object_instance i ON o.id = i.object_id " +
								"WHERE i.spy_id = ?" + whereCondition, [self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving inventory");
				throw err;
			} else {
				if (rows.length == 0) {
					console.log("Inventory updated");
					callback();
				} else {
					for (var i in rows) {
						self.itemFactory.buildFromRow(rows[i], function(item) {
							self.addToInventory(item);
							console.log("Added item " + item.id + " to inventory");
							if (self.getInventorySize(kind) == rows.length) {
								console.log("Inventory updated");
								callback();
							}
						});
					}
				}
			}
		});
	},
	
	/*
		Function: saveInventory
		Stores current inventory in DB.
		
		Parameters:
			kind		-	Inventory kind (<Constants.Inventory>) (optional).
			callback	-	A callback function.
	*/
	saveInventory: function(kind, callback) {
		// Manage optional parameter
		if (typeof kind == "function") {
			callback = kind;
			kind = -1;
		}
		var self = this;
		
		var whereCondition = "";
		
		if (kind == Constants.Inventory.OBJECT)
			whereCondition = " AND o.outfit + o.document + o.hint + o.token = 0 ";
		else if (kind == Constants.Inventory.OUTFIT)
			whereCondition = " AND o.outfit = 1 ";
		else if (kind == Constants.Inventory.DOCUMENT)
			whereCondition = " AND o.document = 1 ";
		else if (kind == Constants.Inventory.HINT)
			whereCondition = " AND o.hint = 1 ";
		else if (kind == Constants.Inventory.TOKEN)
			whereCondition = " AND o.token = 1 ";
		
		// Get list of inventory item ids
		var idsString = self.getInventoryIds(kind).join();
		
		if (idsString.length > 0) {
		
			// Remove items that are not in the list
			self.server.db.query("UPDATE object_instance i INNER JOIN object o ON o.id = i.object_id " +
									"SET i.spy_id = NULL " +
									"WHERE i.spy_id = ? " + 
									whereCondition +
									"AND i.id NOT IN (" + idsString + ")", [self.id], function(err, rows, fields) {
				if (err) {
					console.log("Error saving inventory");
					throw err;
				} else {
					// Add objects that are in the list
					self.server.db.query("UPDATE object_instance i INNER JOIN object o ON o.id = i.object_id " +
											"SET i.spy_id = ?, i.location_id = NULL, i.posX = NULL, i.posY = NULL, i.posZ = NULL " +
											"WHERE i.id IN (" + idsString + ")", [self.id], function(err, rows, fields) {
						if (err) {
							console.log("Error saving inventory");
							throw err;
						} else {
							console.log("Inventory saved");
							callback();
						}
					});
				}
			});
			
		} else {
			// The inventory is empty, so remove everything
			self.server.db.query("UPDATE object_instance i INNER JOIN object o ON o.id = i.object_id " +
									"SET i.spy_id = NULL " +
									"WHERE i.spy_id = ? " + whereCondition, [self.id], function(err, rows, fields) {
				if (err) {
					console.log("Error saving inventory");
					throw err;
				} else {
					console.log("Inventory saved");
					callback();
				}
			});
		}
	},
	
	/*
		Function: saveMoney
		Stores current money in DB.
		
		Parameters:
			callback	-	A callback function.
	*/
	saveMoney: function(callback) {
		var self = this;
		
		self.server.db.query("UPDATE spy s " +
								"SET s.money = ? " +
								"WHERE s.id = ? ", [self.money, self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error saving money");
				throw err;
			} else {
				console.log("Money saved");
				callback();
			}
		});
	},
	
	/*
		Function: updateItemState
		Updates the state of an item (also, rebuilds its behaviour and notifies the players).
		
		Parameters:
			id			-	<Item> id.
			state		-	New state of the item.
			callback	-	A callback function.
	*/
	updateItemState: function(id, state, callback) {
		var self = this;
		
		self.server.db.query("UPDATE object_instance i SET state = ? WHERE i.id = ?", [state, id], function(err, result) {
			if (err) {
				console.log("Error updating intance state in DB");
				throw err;
			} else {
				console.log("Instance state modified");
				var kind = self.getItemKind(id);
				if (kind)
					self.inventory[kind][id].state = state;
				else
					self.location.items[id].state = state;
				
				self.itemFactory.buildFromId(id, function(data) {
					if (kind)
						self.inventory[kind][id].behaviour = data.behaviour;
					else
						self.location.items[id].behaviour = data.behaviour;
					var behaviourMsg = new NetworkMessages.Behaviour(data.id, 0, data.behaviour);
					self.location.pushBroadcast(behaviourMsg);
					if (callback)
						callback(data);
				});
			}
		});
	},
	
	/*
		Function: updateItemProperties
		Updates the properties of an item.
		
		Parameters:
			id			-	<Item> id.
			properties	-	New properties of the item.
			callback	-	A callback function.
	*/
	updateItemProperties: function(id, properties, callback) {
		var self = this;
		
		self.server.db.query("UPDATE object_instance i SET properties = ? WHERE i.id = ?", [JSON.stringify(properties), id], function(err, result) {
			if (err) {
				console.log("Error updating instance properties in DB");
				throw err;
			} else {
				var item = null;
				var kind = self.getItemKind(id);
				if (kind) {
					item = self.inventory[kind][id];
				} else {
					item = self.location.items[id];
				}
				item.properties = properties;
				console.log("Instance properties modified");
				if (callback)
					callback(item);
			}
			
		});
	},
	
	/*
		Function: destroy
	*/
	destroy: function() {
		// Unused, but maybe one day, who knows.
	},
	
	/*
		Function: despawn
		Returns:
			A <NetworkMessages.Despawn> for this player.
	*/
	despawn: function() {
		return new NetworkMessages.Despawn(this);
	},
	
	/*
		Function: setPosition
		Parameters:
			x	-	X coordinate.
			z	-	Z coordinate.
	*/
	setPosition: function(x, z) {
        this.x = x;
        this.z = z;
    },
	
	/*
		Function: send
		Sends a message over the socket.
		
		Parameters:
			message	-	The <NetworkMessage>.
	*/
	send: function(message) {
		this.socket.write(message);
	},
	
	/*
		Function: broadcast
		Enqueues message for the adjacent groups.
		
		Parameters:
			message		-	The <NetworkMessage> to be sent.
			ignoreSelf	-	You usually know about it already, so.
	*/
	broadcast: function(message, ignoreSelf) {
		if (ignoreSelf === undefined)
			ignoreSelf = true;
		this.location.pushToAdjacentGroups(this.group, message, ignoreSelf ? this.id : null);
	},
	
	/*
		Function: broadcastToZone
		Enqueues message for the player's group.
		
		Parameters:
			message		-	The <NetworkMessage> to be sent.
			ignoreSelf	-	Oh, hey, I better let myself know about this, said no one ever.
	*/
	broadcastToZone: function(message, ignoreSelf) {
		if (ignoreSelf === undefined)
			ignoreSelf = true;
		this.location.pushToGroup(this.group, message, ignoreSelf ? this.id : null);
	},
    
	/*
		Function: onLeave
		Called when the player leaves the location.
	*/
    onLeave: function(sendCitySelection) {
		console.log(this.name + " has left the location.");
		if (this.location) {
			this.location.removePlayer(this);
			delete this.location;
			if (sendCitySelection)
				this.startCitySelection();
		}
	},
	
	/*
		Function: onExit
		Called when the player exits the game.
	*/
    onExit: function() {
		this.onLeave();
		console.log(this.name + " has left the game.");
	},
	
	/*
		Function: onMove
		Called each time a Move message is received.
		
		Parameters:
			x	-	X coordinate of the target.
			z	-	Z coordinate of the target.
	*/
	onMove: function(x, z) {
		var self = this;
		
		// Check if player has changed his group
		var hasChangedGroups = self.location.handleEntityGroupMembership(self);
		if (hasChangedGroups) {
			// Save player position to DB
			self.savePosition();
			// Tell everybody in the previous groups to despawn the player
			self.location.pushToPreviousGroups(self, new NetworkMessages.Despawn(self));
			// Tell everybody in the new groups to spawn the player
			self.location.pushToAdjacentGroups(self.group, new NetworkMessages.Spawn(self), self.id);
			// Send the relevant entity list to the player
			self.location.pushRelevantEntityListTo(self);
		}
	},
	
	/*
		Function: resetTimeout
		Sets disconnectTimeout.
	*/
	resetTimeout: function() {
		clearTimeout(this.disconnectTimeout);
		this.disconnectTimeout = setTimeout(this.timeout.bind(this), 1000 * 60 * 15); // 15 min.
	},
	
	/*
		Function: timeout
		Called when the timer expires, ends the connection.
	*/
	timeout: function() {
		this.socket.end("Player was idle for too long");
	}
	
});