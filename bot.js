/*
	Script: bot
*/

var cls = require("./lib/class"),
	NetworkMessages = require("./message"),
	fs = require("fs");

/*
	Class: Bot
	Holds bot information
	
	Variables:
		id						-	The bot id.
		name					-	Bots are people too.
		level					-	I'm pretty sure it's the level.
		location				-	The current location.
		x						-	X position in the current location.
		z						-	Z position in the current location.
		kind					-	The kind (as in <Constants.Entities>).
		outfits					-	Bots like fashion.
		movements				-	The moving cycle for this bot.
		currentMovementOrder	-	The current step in the moving cycle.
*/

module.exports = Bot = cls.Class.extend({

	/*
		Constructor: init
		Parameters:
			id			-	Nope.
			name		-	Nope nope.
			level		-	Nope nope nope nope.
			position	-	Oh, this is interesting.
			location	-	Yeah.
	*/
	init: function(id, name, level, position, location) {
		var self = this;
		
		this.id = id;
		this.name = name;
		this.level = level;
		this.location = location;
		
        this.kind = Constants.Entities.SPY;
        this.x = position.x;
        this.z = position.z;
		
		this.outfits = {};
		// Default outfit
		this.outfits[1] = {index: 0, animations: {walking: "baseWalking", interacting: "baseInteracting"}};
		this.movements = [];
		this.currentMovementOrder = -1;
		
		// Retrieve bot outfits
		self.location.server.db.query("SELECT i.id, o.name, o.textures " +
									"FROM object o INNER JOIN object_instance i ON i.object_id = o.id " +
									"WHERE i.spy_id = ? AND o.outfit = 1 AND i.state = 1", [self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving outfits");
				throw err;
			} else {
				for (var t in rows) {
					var textures = JSON.parse(rows[t].textures);
					self.outfits[rows[t].id] = textures.outfit;
				}
			}
		});
		
		// Retrieve bot movements
		self.location.server.db.query("SELECT m.order, m.wait_time, m.targetX, m.targetZ " +
										"FROM bot_movement m " +
										"WHERE m.spy_id = ?", [self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error retrieving bot movements");
				throw err;
			} else {
				for (var m in rows) {
					self.movements.push({order: rows[m].order, wait_time: rows[m].wait_time, target: {x: rows[m].targetX, z: rows[m].targetZ}});
				}
				if (self.movements.length > 0) {
					// Set the last target in the cycle as the bot's starting position
					self.setPosition(rows[rows.length - 1].targetX, rows[rows.length - 1].targetZ);
				}
				self.savePosition();
				self.currentMovementOrder = 0;
				if (self.movements.length > 0) {
					self.startMovement();
				}
			}
		});
	
	},
	
	/*
		Function: updateState
		Updates the bot's state (with respect to the player), rebuilds behaviour and passes them to a callback function.
		
		Parameters:
			state		-	The new entity state.
			player		-	The player with respect to whom the state has to be updated.
			callback	-	A callback function.
	*/
	updateState: function(state, player, callback) {
		var self = this;

		var queryString = "INSERT INTO bot_spy_state (bot_id, spy_id) " +
							"VALUES (?, ?) " +
							"ON DUPLICATE KEY UPDATE state = ?";
		var queryArgs = [self.id, player.id, state];
		if (state == null || state == undefined) {
			var queryString = "INSERT INTO bot_spy_state (bot_id, spy_id) " +
							"VALUES (?, ?) " +
							"ON DUPLICATE KEY UPDATE bot_id = bot_id";
			var queryArgs = [self.id, player.id];
		}
		
		self.location.server.db.query(queryString, queryArgs, function(err, rows, fields) {
			if (err) {
				console.log("Error updating state");
				throw err;
			} else {
				// Retrieve bot behaviour
				self.location.server.db.query("SELECT s.descriptor, bs.state, s.properties " +
												"FROM spy s INNER JOIN bot_spy_state bs ON bs.bot_id = s.id " +
												"WHERE bs.bot_id = ? AND bs.spy_id = ?", [self.id, player.id], function(err, rows, fields) {
					if (err) {
						console.log("Error retrieving bot movements");
						throw err;
					} else {
						var behaviour = {};
						self.descriptor = rows[0].descriptor;
						self.properties = rows[0].properties ? rows[0].properties : {};
						// Parse the descriptor
						fs.readFile(__dirname + "/bots/" + self.descriptor + ".json", function(err, file) {
							var json = JSON.parse(file.toString());
							// Reminder: rows[0].state is the id of the bot's current state (0, 1, or whatever)
							var stateId = rows[0].state;
							var state = json.states[stateId];
							behaviour.state = state;
							behaviour.name = state.name;
							behaviour.methods = {};
							
							// Bot properties
							var properties = self.properties;
							// If it's not been parsed already, parse it
							if (typeof self.properties == "string") {
								if (self.properties.length > 0)
									properties = JSON.parse(self.properties);
								else
									properties = {};
							}
							
							// Retrieve all possible dialogues for this bot
							// Reminder: dialogues have preconditions, to be matched with state and properties of the bot
							self.location.server.db.query("SELECT id, precondition, dialogue " +
															"FROM bot_dialogues " +
															"WHERE bot_id = ?", [self.id], function(err, rows, fields) {
								if (err) {
									console.log("Error retrieving behaviour from DB");
									throw err;
								} else {
									if (rows.length > 0) {
										// Dialogues available, add the Talk method
										behaviour.methods[Constants.Methods.TALK] = {name: "Talk", isSubject: true, isObject: false, auto: true, icon: "talk"};
										behaviour.methods[Constants.Methods.TALK].dialogues = {};
										for (var pre in rows) {
											var checkPrecondition = new Function("state", rows[pre].precondition);
											var applicable = checkPrecondition(stateId);
											if (applicable) {
												// Add the dialogue to the method
												behaviour.methods[Constants.Methods.TALK].dialogues[rows[pre].id] = rows[pre].dialogue;
											}
										}
									}
									// Execute callback
									if (callback) {
										callback(state, behaviour);
									}
								}
							});
						});
					}
				});
			}
		});
	},
	
	/*
		Function: stopMessage
		Returns:
			A <NetworkMessages.Dialogue> to end the conversation.
	*/
	stopMessage: function() {
		return new NetworkMessages.Dialogue(0, this.id, 1, "");
	},
	
	/*
		Function: behaviourMessage
		Parameters:
			behaviour	-	The desired behaviour.
		
		Returns:
			A <NetworkMessages.Behaviour>.
	*/
	behaviourMessage: function(behaviour) {
		return new NetworkMessages.Behaviour(0, this.id,behaviour);
	},
	
	/*
		Function: catalogMessage
		Fetches the catalog of the current location and passes a <NetworkMessages.Catalog> to a callback function.
		
		Parameters:
			callback	-	A callback function.
	*/
	getCatalogMessage: function(callback) {
		var catalogData = this.location.getCatalog(function(catalog) {
			callback(new NetworkMessages.Catalog(catalog));
		});
	},
	
	/*
		Function: destroy
	*/
	destroy: function() {
		// Unused, but maybe one day, who knows.
	},
	
	/*
		Function: despawn
		Returns:
			A <NetworkMessages.Despawn> for this player.
	*/
	despawn: function() {
		return new NetworkMessages.Despawn(this);
	},
	
	/*
		Function: setPosition
		Parameters:
			x	-	X coordinate.
			z	-	Z coordinate.
	*/
	setPosition: function(x, z) {
        this.x = x;
        this.z = z;
    },
	
	/*
		Function: onMove
		Called each time a Move message is received.
		
		Parameters:
			x	-	X coordinate of the target.
			z	-	Z coordinate of the target.
	*/
	onMove: function(x, z) {
		var self = this;
		
		// Check if the bot has changed his group
		var hasChangedGroups = self.location.handleEntityGroupMembership(self);
		if (hasChangedGroups) {
			// Save bot position to DB
			self.savePosition();
			// Tell everybody in the previous groups to despawn the bot
			self.location.pushToPreviousGroups(self, new NetworkMessages.Despawn(self));
			// Tell everybody in the new groups to spawn the bot
			self.location.pushToAdjacentGroups(self.group, new NetworkMessages.Spawn(self), self.id);
		}
	},
	
	/*
		Function: savePosition
		Stores current position in DB.
	*/
	savePosition: function() {
		var self = this;
		self.location.server.db.query("UPDATE spy SET location_id = ?, posX = ?, posZ = ? WHERE id = ?", [self.location.id, self.x, self.z, self.id], function(err, rows, fields) {
			if (err) {
				console.log("Error saving position");
				throw err;
			} else {
				//console.log("Position saved");
			}
		});
	},
	
	/*
		Function: startMovement
		Sets up the periodic moving cycle management.
	*/
	startMovement: function() {
		var self = this;
		
		self.movementInterval = setInterval(function() {
			var currentMovement = self.movements[self.currentMovementOrder];
			if (currentMovement.wait_time) {
				// The bot has to wait
				self.pauseMovement();
				self.movementTimeout = setTimeout(function() {
					self.currentMovementOrder = (self.currentMovementOrder + 1) % self.movements.length;
					self.startMovement();
				}, currentMovement.wait_time);
			} else {
				// The bot has to move
				var distance = Math.sqrt(Math.pow(self.x - currentMovement.target.x, 2) + Math.pow(self.z - currentMovement.target.z, 2));
				if (distance > 0.2) {
					// Compute new position along the line towards the target
					var deltaX = currentMovement.target.x - self.x;
					var deltaZ = currentMovement.target.z - self.z;
					var signX = (deltaX == 0 ? 0 : deltaX / Math.abs(deltaX));
					var signZ = (deltaZ == 0 ? 0 : deltaZ / Math.abs(deltaZ));
					var m = deltaZ / deltaX;
					var d = Math.min(0.5, distance);
					var dx = signX * d / Math.sqrt(1 + m*m);
					var dz = signZ * d / Math.sqrt(1 + 1/m/m);
					self.x += dx;
					self.z += dz;
					var move = new NetworkMessages.Move(self.id, {x: self.x, y: 0, z: self.z});
					self.broadcast(move);
					self.onMove(self.x, self.z);
				} else {
					// We miss you Ian Curtis
					var newOrder = (self.currentMovementOrder + 1) % self.movements.length;
					self.currentMovementOrder = newOrder;
				}
			}
        }, 10000 / self.location.ups);
	},
	
	pauseMovement: function() {
		clearTimeout(this.movementTimeout);
		clearInterval(this.movementInterval);
	},
	
	/*
		Function: broadcast
		Enqueues message for the adjacent groups.
		
		Parameters:
			message		-	The <NetworkMessage> to be sent.
			ignoreId	-	Do not notify this guy.
	*/
	broadcast: function(message, ignoreId) {
		if (ignoreId === undefined)
			ignoreId = null;
		this.location.pushToAdjacentGroups(this.group, message, ignoreId);
	}
	
});