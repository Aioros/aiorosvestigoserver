/*
	Script: constants
*/

/*
	Class: Constants
	Just a lot of game constants and enums.
*/

Constants = {
	/*
		Constants: Generic constants
		
		NET_HEADER_SIZE			-	Size of the network message header, in bytes.
		CITY_ZONE_WIDTH			-	Width of a city zone.
		CITY_ZONE_HEIGHT		-	Height of a city zone.
		WALLS_DEPTH				-	Depth of walls.
		WALLS_HEIGHT			-	Height of walls.
	*/

	NET_HEADER_SIZE: 4,
	
	CITY_ZONE_WIDTH: 12,
	CITY_ZONE_LENGTH: 12,
	
	WALLS_DEPTH: 0.2,
	WALLS_HEIGHT: 1.3,
	DOORS_WIDTH: 0.6,
	DOORS_HEIGHT: 0.8,
	
	/*
		Enum: Inventory
		Identify the inventory categories.
	*/
	Inventory: {
		OBJECT: 0,
		OUTFIT: 1,
		DOCUMENT: 2,
		HINT: 3,
		TOKEN: 4
	},
	
	/*
		Enum: Messages
		Identify the network message type.
	*/
	Messages: {
		HELLO: {value: 0, messageClass: "Hello"},
		WELCOME: {value: 1, messageClass: "Welcome"},
		LOGIN: {value: 2, messageClass: "Login"},
		CITIES: {value: 3, messageClass: "Cities"},
		CITY: {value: 4, messageClass: "City"},
		LOCATIONINFO: {value: 5, messageClass: "LocationInfo"},
		PLAYERINFO: {value: 6, messageClass: "PlayerInfo"},
		NOTIFICATION: {value: 7, messageClass: "Notification"},
		CREATE: {value: 8, messageClass: "Create"},
		DESTROY: {value: 9, messageClass: "Destroy"},
		MOVE: {value: 10, messageClass: "Move"},
		TEXTURE: {value: 11, messageClass: "Texture"},
		LIST: {value: 12, messageClass: "List"},
		CHAT: {value: 13, messageClass: "Chat"},
		INVENTORY: {value: 14, messageClass: "Inventory"},
		COLLECT: {value: 15, messageClass: "Collect"},
		DROP: {value: 16, messageClass: "Drop"},
		INTERACTION: {value: 17, messageClass: "Interaction"},
		DIALOGUE: {value: 18, messageClass: "Dialogue"},
		BEHAVIOUR: {value: 19, messageClass: "Behaviour"},
		TEXTURES: {value: 20, messageClass: "Textures"},
		INSERT: {value: 21, messageClass: "Insert"},
		LOOT: {value: 22, messageClass: "Loot"},
		EQUIP: {value: 23, messageClass: "Equip"},
		UNEQUIP: {value: 24, messageClass: "Unequip"},
		TELEPORT: {value: 25, messageClass: "Teleport"},
		EXAMINE: {value: 26, messageClass: "Examine"},
		HINT: {value: 27, messageClass: "Hint"},
		TOKEN: {value: 28, messageClass: "Token"},
		WIN: {value: 29, messageClass: "Win"},
		ROB: {value: 30, messageClass: "Rob"},
		GIVE: {value: 31, messageClass: "Give"},
		MONEYTRANSFER: {value: 32, messageClass: "MoneyTransfer"},
		SELL: {value: 33, messageClass: "Sell"},
		ANIMATION: {value: 34, messageClass: "Animation"},
		CATALOG: {value: 35, messageClass: "Catalog"},
		BUY: {value: 36, messageClass: "Buy"}
	},
		
	/*
		Enum: CreateFlags
		Flags used in any object creation.
		
		ACTIVE			- Set to false when creating a temporary invisible object (i.e. for inventory).
		OUTFIT			- Set to true if the object is an outfit.
		DOCUMENT		- Set to true if the object is a document.
		HINT			- Set to true if the object is a hint.
		TOKEN			- Set to true if the object is a mission token.
		BOT				- Set to true if the object is a bot.
	*/
	CreateFlags: {
		ACTIVE: 0x1,
		OUTFIT: 0x2,
		DOCUMENT: 0x4,
		HINT: 0x8,
		TOKEN: 0x10,
		BOT: 0x20
	},
	
	/*
		Enum: Methods
		Special game interactions between objects.
	*/
	Methods: {
		PICK: 0,
		INSERT: 1,
		LOOT: 2,
		EQUIP: 3,
		UNEQUIP: 4,
		TALK: 5,
		GIVE: 6,
		ROB: 7,
		GO: 8,
		EXAMINE: 9,
		TOKEN: 10,
		SELL: 11
	},
	
	/*
		Enum: Entities
		Identify the entity kind.
	*/
	Entities: {
		SPY: 0,
		FLOOR: 1,
		CITY_OBJECT: 2,
		OBJECT: 3,
		LAMP: 4,
		WALL: 5,
		DOOR: 6,
		PORTKEY: 7
	}
};
    
if(!(typeof exports === 'undefined')) {
    module.exports = Constants;
}